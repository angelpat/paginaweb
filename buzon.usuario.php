<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="cleditor/jquery.cleditor.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="js/menu.js"></script>          
		<script src="cleditor/jquery.cleditor.min.js"></script>
        <script src="js/buzon-usuario.js"></script>                         
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");				
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>      
    	<div id="modal-mensaje" title="Buz&oacute;n de Contacto">		
			<br><br><br>	
			<p style="font-size: 14px; color: #053B64;">Comentario guardado satisfactoriamente</p>
		</div>
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>			
			<div id="content">				
				<section class="buzon-content">
					<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto; width: 600px;">													
						<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">BUZON DEL USUARIO</p><br/>	
					</div>								
					<br/><br/>	
					<div class="blue-top">
						<img src="img/content/crn-tl-blue.gif" alt="" class="crn-tl-blue" />
						<img src="img/content/crn-tr-blue.gif" alt="" class="crn-tr-blue" />
					</div>
					<div class="blue-content ">
									
						<div id="boxcontrol" class="boxcontrol">	
							
							<form  id="frm-buzon">
														
								<table>
									<thead>
									<tr>
									<td> 
										<p id="tip" class="validateTips ui-corner-all" style='padding: 0 .7em; font-size: 100%; width:725px;'>
									<br/>
									</td>
									</tr>
									</thead>
								</table>						
																
								<table class="tabla-buzon">
									<tr>
										<td class="rb-buzon">Opini&oacute;n:</td>
										<td class="rb-buzon"><input type="radio" id="tipo-felicitacion" name="rb-tipo" value="Felicitacion"  class="rb-tipo-activo" /> <label for="tipo-felicitacion">Felicitacion</label></td>
										<td class="rb-buzon"><input type="radio" id="tipo-queja" name="rb-tipo" value="Queja" class="rb-tipo-activo" /> <label for="tipo-queja">Queja</label></td>
										<td class="rb-buzon"><input type="radio" id="tipo-sugerencia" name="rb-tipo" value="Sugerencia"  class="rb-tipo-activo" /> <label for="tipo-sugerencia">Sugerencia</label></td>										
										<td class="rb-buzon"></td>
										<td class="rb-buzon"></td>
									</tr>																	
									<tr>
										<td class="rb-buzon">Derechohabiencia:</td>
										<td class="rb-buzon"><input type="radio" id="derecho-ssa" name="rb-derecho" value="SSA" class="rb-derecho-activo" /> <label for="derecho-ssa">SSA</label></td>
										<td class="rb-buzon"><input type="radio" id="derecho-issste" name="rb-derecho" value="ISSSTE" class="rb-derecho-activo" /> <label for="derecho-issste">ISSSTE</label></td>
										<td class="rb-buzon"><input type="radio" id="derecho-imss" name="rb-derecho" value="IMSS" class="rb-derecho-activo" /> <label for="derecho-imss">IMSS</label></td>
										<td class="rb-buzon"><input type="radio" id="derecho-otro" name="rb-derecho" value="otro" class="rb-derecho-activo rb-derecho-inactivo" /> <label for="derecho-otro">OTROS</label> </td>
										<td class="rb-buzon"><input type="text"  id="txt-derecho-otro" value="" placeholder="Especifique" size="14" maxlength="50"/></td>										
									</tr>
									<tr>
										<td colspan="6">
											Nombre de quien recibio la atenci&oacute;n:
										</td>
									</tr>										
									<tr>
										<td colspan="6">
											<input type="text" id="txt-nombre-personal" name="txt-nombre-personal" size="118"  />											
										</td>									
									</tr>
					            </table> 
								<table class="tabla-buzon">
									<tr>
										<td class="servicio">
											Fecha: <input type="text" id="fecha" name="fecha" class="date" maxlength="10" placeholder="31/01/2012" />
										</td>
										<td class="servicio">
											Hora: 	  											
											<select id="hora" style="width: 80px;">
												<option value="0">12:00 </option>
												<option value="1">01:00 </option>													
												<option value="2">02:00 </option>													
												<option value="3">03:00 </option>													
												<option value="4">04:00 </option>													
												<option value="5">05:00 </option>
												<option value="6">06:00 </option>
												<option value="7">07:00 </option>
												<option value="8">08:00 </option>													
												<option value="9">09:00 </option>													
												<option value="10">10:00 </option>													
												<option value="11">11:00</option>																																																																														
											</select>											
											<select id="meridiano">
												<option value="AM">AM</option>
												<option value="PM">PM</option>
											</select>											
										</td>
										<td class="servicio">
											Turno: 	
												<select id="turno" style="width: 130px;" >
													<option value="matutino">Matutino</option>
													<option value="vespertino">Vespertino</option>
													<option value="nocturno">Nocturno</option>
												</select> 										
										</td>									
									</tr>
								</table>
								<br/>								
								Servicio del cual recibio la atenci&oacute;n:									
								<table class="tabla-buzon-servicio">
									<thead>						
										<tr>	
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tr>
										<td><input type="radio" id="servicio-urgencias" name="rb-servicio" class="rb-ser-activo" value="Urgencias" /> <label for="servicio-urgencias">Urgencias</label></td>
										<td><input type="radio" id="servicio-admision" name="rb-servicio" class="rb-ser-activo" value="Admision" /> <label for="servicio-admision">Admisi&oacute;n</label></td>
										<td><input type="radio" id="servicio-ts" name="rb-servicio" class="rb-ser-activo" value="Trabajo Social" /> <label for="servicio-ts">Trabajo Social</label></td>
									</tr>
									<tr>
										<td><input type="radio" id="servicio-enfermeria" name="rb-servicio" class="rb-ser-activo" value="Enfermeria" /> <label for="servicio-enfermeria">Enfermeria</label></td>
										<td><input type="radio" id="servicio-pediatria" name="rb-servicio" class="rb-ser-activo" value="Pediatria" /> <label for="servicio-pediatria">Pediatr&iacute;a</label></td>
										<td><input type="radio" id="servicio-rayos-x" name="rb-servicio" class="rb-ser-activo" value="Rayos X" /> <label for="servicio-rayos-x">Rayos X</label></td>
									</tr>
									<tr>
										<td><input type="radio" id="servicio-archivo" name="rb-servicio" class="rb-ser-activo" value="Archivo Clinico" /> <label for="servicio-archivo">Archivo Cl&iacute;nico</label></td>
										<td><input type="radio" id="servicio-recepcion" name="rb-servicio" class="rb-ser-activo" value="Recepcion" /> <label for="servicio-recepcion">Recepci&oacute;n</label></td>
										<td><input type="radio" id="servicio-consulta-ext" name="rb-servicio" class="rb-ser-activo" value="Consulta Externa" /> <label for="servicio-consulta-ext">Consulta Externa</label></td>
									</tr>
									<tr>
										<td><input type="radio" id="servicio-laboratorio" name="rb-servicio" class="rb-ser-activo" value="Laboratorio" /> <label for="servicio-laboratorio">Laboratorio</label></td>
										<td><input type="radio" id="servicio-medicos" name="rb-servicio" class="rb-ser-activo" value="Medicos" /> <label for="servicio-medicos">M&eacute;dicos</label></td>
										<td><input type="radio" id="servicio-caja" name="rb-servicio" class="rb-ser-activo" value="Caja" /> <label for="servicio-caja">Caja</label></td>
									</tr>															
									<tr>
										<td><input type="radio" id="servicio-intendencia" name="rb-servicio" class="rb-ser-activo" value="Intendencia" /> <label for="servicio-intendencia">Intendencia</label></td>
										<td><input type="radio" id="servicio-sp" name="rb-servicio" class="rb-ser-activo" value="Seguro popular" /> <label for="servicio-sp">Seguro Popular</label></td>
										<td><input type="radio" id="servicio-ginecologia" name="rb-servicio" class="rb-ser-activo" value="Ginecologia" /> <label for="servicio-ginecologia">Ginecolog&iacute;a</label></td>
									</tr>	
									<tr>
										<td colspan="3"><input type="radio" id="servicio-otro" name="rb-servicio" value="otro" class="rb-ser-activo rb-ser-inactivo" /> 
											<label for="servicio-otro" style="padding-right: 10px;">Otro Servicio</label> 																						
										</td>							
									</tr>										
								</table>																				
								<input type="text" placeholder="Especifique" maxlength="255" id="txt-servicio-otro" value="" size="118" /><br/>
								<br/>
								Narrativos de los Hechos:<br/><br/>
								<textarea name="narrativa" id="narrativa" class="text ui-widget-content ui-corner-all cleditor"></textarea>
								<br/>
								<br/>
								<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:720px;" > 									
									El an&aacute;lisis de las quejas es totalmente <strong>(CONFIDENCIAL)</strong>, si deas proporcione sus datos generales, asi nos 
									permitira informarle a usted de la resoluci&oacute;n de su queja.														
								</div>																
								<table class="tabla-buzon">
									<thead>
										<tr>
											<th class="datos"></th>
											<th class="txt-datos"></th>
										</tr>
									</thead>
									<tr>
										<td>Nombre:</td>
										<td><input type="text" id="nombre" name="nombre" placeholder="Nombre del quejoso" size="105"> </td>
									</tr>
									<tr>
										<td>Domicilio:</td>
										<td><input type="text" id="domicilio" name="domicilio" placeholder="Calle No. Colonia" size="105"></td>
									</tr>
									<tr>
										<td>Tel&eacute;fono:</td>
										<td><input type="text" id="telefono" name="telefono" placeholder="612 17 5 05 00" size="105"></td>
									</tr>
									<tr>
										<td>Correo-e:</td>
										<td><input type="text" id="correo-e" name="correo-e" placeholder="nombre@correo.com" size="105"></td>
									</tr>									
									<tr>
										<td colspan="2" > 
										<!-- 	<input type="button" id="btn-zoo" name="btn-enviar" value="Enviar" class="btn" style="display: block; margin: auto;" /> -->
											<a id="btn-enviar" style="display: block; margin: auto; width: 100px;">Enviar</a> 
										</td>
									</tr>							
								</table>		
							</form>
						</div>
					</div>
					<div class="blue-btm">
						<img src="img/content/crn-bl-blue.gif" alt="" class="crn-bl-blue" />
						<img src="img/content/crn-br-blue.gif" alt="" class="crn-br-blue" />
					</div>				
				</section>			
			</div>					
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>