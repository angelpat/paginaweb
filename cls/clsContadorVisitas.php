<?php
/**
 * Clase para manipular cirugia
 */
class Contador
{
    /**
     * Conectar a base de datos
     */
  	public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }  
    
	
    /***********************************************************************************************
    * 	GetUserIP: Obtiene la Ip del usuario
    *	@return IP del usuario
    *   http://thepcspy.com/read/getting_the_real_ip_of_your_users/
    */
    
	function GetUserIP() {
	
	    if (isset($_SERVER)) {
	
	        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	            return $_SERVER["HTTP_X_FORWARDED_FOR"];
	        
	        if (isset($_SERVER["HTTP_CLIENT_IP"]))
	            return $_SERVER["HTTP_CLIENT_IP"];
	
	        return $_SERVER["REMOTE_ADDR"];
	    }
	
	    if (getenv('HTTP_X_FORWARDED_FOR'))
	        return getenv('HTTP_X_FORWARDED_FOR');
	
	    if (getenv('HTTP_CLIENT_IP'))
	        return getenv('HTTP_CLIENT_IP');
	
	    return getenv('REMOTE_ADDR');
	}

	
	/***********************************************************************************************
	* 	insertContadorVisitas: ejecuta el SP webSP_insertContadorVisitas
	*	@return Total de visitas
	*/
	
	public function insertContadorVisitas($url){
	
		$visitas =  '';  

		$ip = Contador::GetUserIP();		
		$nombreEquipo = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				
		$conn = connecToDB();
		 
		$tsql = "EXEC webSP_insertContadorVisitas '". $ip . "', '". $nombreEquipo  . "', '" . $url ."'";
		
		$stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
		{
			echo "Error in query preparation/execution.\n";
			die( print_r( sqlsrv_errors(), true));
		}
	
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$visitas = $row['visitas'];
		}
	
		/* Free statement and connection resources. */
		sqlsrv_free_stmt( $stmt);
		sqlsrv_close( $conn);
		 
		return $visitas;
		 
	}
		


	public function insertDatosBitacora($reason,$urlhost){
	
		//$visitas =  '';  

		$ip = Contador::GetUserIP();		
		$nombreEquipo = gethostbyaddr($_SERVER['REMOTE_ADDR']);
				
		$conn = connecToDB();
		 
		$tsql = "EXEC webSP_insertContadorVisitas '". $ip . "', '". $nombreEquipo  . "', '" . $url ."'";
		
		$stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
		{
			echo "Error in query preparation/execution.\n";
			die( print_r( sqlsrv_errors(), true));
		}
	
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$visitas = $row['visitas'];
		}
	
		/* Free statement and connection resources. */
		sqlsrv_free_stmt( $stmt);
		sqlsrv_close( $conn);
		 
		return $visitas;
		 
	}

	
	
    
}
