<?php
/**
 * Clase para manipular el envio de correo a Redes
 */
class CorreoRedes
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        

    /***********************************************************************************************
    * 	selectDescripcionConmutador: ejecuta el SP webSP_peticionDesbloqueo
    *
    *	@param  $datos 
    */
    
    public function peticionDesbloqueo($datos){

    	$conn = connecToDB();
        	
    	
    	//$tsql = "exec webSP_peticionDesbloqueo '". $datos['reason'] ."', '". $datos['urlhost'] ."', '". $datos['nombre'] ."', '". $datos['servicio'] ."','". $datos['correo'] ."','". $datos['motivo'] ."'";
    	$tsql = "exec webSP_peticionDesbloqueo '". $datos['reason'] ."', '". $datos['urlhost'] ."', ' " . htmlspecialchars( $datos['nombre'] ). "   ', '". $datos['servicio'] ."','". $datos['correo'] ."','". $datos['motivo'] ."'";
    	
        $stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
    	{
			echo "Error in query preparation for <strong> webSP_peticionDesbloqueo</strong> /execution.<br><br>\n";
    		die( print_r( sqlsrv_errors(), true));
        }    
       
        /* Free statement and connection resources. */
        sqlsrv_free_stmt( $stmt);
        sqlsrv_close( $conn);
        
       // return $datos;        	
	}
    
}
