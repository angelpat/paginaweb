<?php
/**
 * Clase para manipular el envio de correo a Redes
 */
class CorreoRedes
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        

    /***********************************************************************************************
    * 	selectDescripcionConmutador: ejecuta el SP webSP_peticionDesbloqueo
    *
    *	@param  $datos 
    */

	function GetUserIP() {
	
	    if (isset($_SERVER)) {
	
	        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	            return $_SERVER["HTTP_X_FORWARDED_FOR"];
	        
	        if (isset($_SERVER["HTTP_CLIENT_IP"]))
	            return $_SERVER["HTTP_CLIENT_IP"];
	
	        return $_SERVER["REMOTE_ADDR"];
	    }
	
	    if (getenv('HTTP_X_FORWARDED_FOR'))
	        return getenv('HTTP_X_FORWARDED_FOR');
	
	    if (getenv('HTTP_CLIENT_IP'))
	        return getenv('HTTP_CLIENT_IP');
	
	    return getenv('REMOTE_ADDR');
	}

    
    public function peticionDesbloqueo($datos){

    	$conn = connecToDB();
        	
    	$ip = CorreoRedes::GetUserIP();		
	$nombreEquipo = gethostbyaddr($_SERVER['REMOTE_ADDR']);

    	//$tsql = "exec webSP_peticionDesbloqueo '". $datos['reason'] ."', '". $datos['urlhost'] ."', '". $datos['nombre'] ."', '". $datos['servicio'] ."','". $datos['correo'] ."','". $datos['motivo'] ."'";
    	$tsql = "exec webSP_peticionDesbloqueo '". $datos['reason'] ."', '". $datos['urlhost'] ."', ' " . htmlspecialchars( $datos['nombre'] ). "   ', '". $datos['servicio'] ."','". $datos['correo'] ."','". $datos['motivo'] ."','". $ip. "','" . $nombreEquipo . "'";
    	
        $stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
    	{
			echo "Error in query preparation for <strong> webSP_peticionDesbloqueo</strong> /execution.<br><br>\n";
    		die( print_r( sqlsrv_errors(), true));
        }    
       
        /* Free statement and connection resources. */
        sqlsrv_free_stmt( $stmt);
        sqlsrv_close( $conn);
        
       // return $datos;        	
	}
    
}
