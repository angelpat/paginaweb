<?php
/**
 * Clase para manipular cirugia
 */
class Cirugia
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        
	
    
    public function test(){

    	$conn = connecToDB();
		
		/* Set up and execute the query. */
	//	$tsql = "EXEC webSP_selectSolicitudCirugia 2000, '06/06/2011'";
	
    	$tsql = "select * from solicitudCirugias";
    	 
		$stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
		{
		    echo "Error in query preparation/execution.\n";
		    die( print_r( sqlsrv_errors(), true));
		}
		
		/* Retrieve each row as an associative array and display the results.*/
		echo "<ul>";
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			echo "<li>".$row['estado']."</li>";
		}
		echo "</ul>";
		
		/* Free statement and connection resources. */
		sqlsrv_free_stmt( $stmt);
		sqlsrv_close( $conn);
    }     

    /***********************************************************************************************
    * 	selectSolicitudCirugia: ejecuta el SP webSP_selectSolicitudCirugia
    *
    *	@param  $folio	No folio
    *	@param	$date	Fecha Naciemiento
    *
    *	@return Datos de la cirugia
    */

    public function selectSolicitudCirugia($folio, $date){

    	$datos = array();    	
    	$conn = connecToDB();
    	
    	$tsql = "EXEC webSP_selectSolicitudCirugia '". $folio . "', '". $date . "'";
    	
    	$stmt = sqlsrv_query( $conn, $tsql);
    	if( $stmt === false)
    	{
    		echo "Error in query preparation/execution.\n";
    		die( print_r( sqlsrv_errors(), true));
    	}
    	    	
    	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))    	
    	{
			$datos = $row;
    	}
    	    	
    	/* Free statement and connection resources. */
    	sqlsrv_free_stmt( $stmt);
    	sqlsrv_close( $conn);
    	
    	return $datos;
    	
    }
    

 
    
}
