<?php
/**
 * Clase para manipular las noticias
 */
class Noticia
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        

    /***********************************************************************************************
    * 	selectDescripcionConmutador: ejecuta el SP webSP-selectNoticias
    *	@return Tabla de reusltados de la busqueda
    */
    
    public function selectNoticias($tipoNoticia){
    
    	$datos = array();
    	$conn = connecToDB();
    	$tsql = "EXEC webSP_selectNoticias '" . $tipoNoticia . "'";
        $stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
    	{
    		echo "Error in query preparation/execution.\n";
    		die( print_r( sqlsrv_errors(), true));
        }    
      		
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
        {
        	    $datos[] = array("idNoticia" 		=> $row['idNoticia'] ,
                                 "titulo" 	 		=> $row['titulo'],
        	    				 "descripcion"		=> $row['descripcion'],
				        	     "lugarFecha" 		=> $row['lugarFecha'],
				        	     "plantilla" 		=> $row['plantilla'],
				        	     "tipoLink" 		=> $row['tipoLink'],
				        	     "imgDescripcion" 	=> $row['imgDescripcion'],
				        	     "thumbRuta"	 	=> $row['thumbRuta'],
				        	     "thumbAltura" 		=> $row['thumbAltura'],
        	    				 "thumbAncho" 		=> $row['thumbAncho']);        		
        }
    
        /* Free statement and connection resources. */
        sqlsrv_free_stmt( $stmt);
        sqlsrv_close( $conn);
        
        return $datos;        	
	}
       

	/***********************************************************************************************
	* 	selectDescripcionConmutador: ejecuta el SP webSP-selectNoticia
	*	@return Tabla de reusltados de la busqueda
	*/
	
	public function selectNoticia($idNoticia){
	
		$datos = array();
		$conn = connecToDB();
		$tsql = "EXEC webSP_selectNoticia " . $idNoticia ;
		$stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
		{
			echo "Error in query preparation/execution.\n";
			die( print_r( sqlsrv_errors(), true));
		}
	
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
		{
			$datos = array("idNoticia" 		=> $row['idNoticia'] ,
                                 "titulo" 	 		=> $row['titulo'],
        	    				 "descripcion"		=> $row['descripcion'],
				        	     "lugarFecha" 		=> $row['lugarFecha'],
				        	     "plantilla" 		=> $row['plantilla'],
				        	     "tipoLink" 		=> $row['tipoLink'],
				        	     "imgDescripcion" 	=> $row['imgDescripcion'],
				        	     "imgRuta"	 	    => $row['imgRuta'],
				        	     "imgAltura" 		=> $row['imgAltura'],
        	    				 "imgAncho" 		=> $row['imgAncho']);        		
		}
	
		/* Free statement and connection resources. */
		sqlsrv_free_stmt( $stmt);
		sqlsrv_close( $conn);
	
		return $datos;
	}	

     
    
}
