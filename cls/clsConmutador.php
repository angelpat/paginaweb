<?php
/**
 * Clase para manipular el conmutador
 */
class Conmutador
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        

    /***********************************************************************************************
    * 	selectDescripcionConmutador: ejecuta el SP webSP-selectDescripcionConmutador
    *
    *	@param  $buscar	Texto a buscar
    *	@return Tabla de reusltados de la busqueda
    */
    
    public function selectDescripcionConmutador($buscar, $tipo){
    
    	$datos = array();
    	$conn = connecToDB();
        //$tsql = "SELECT Clave, Extension, Lugar FROM ExtensionTel WHERE Lugar LIKE '%".$buscar."%' OR Extension LIKE '%".$buscar."%' ORDER BY Lugar";
    	$tsql = "EXEC webSP_selectDescripcionConmutador '". $buscar . "','" . $tipo ."'";
        $stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
    	{
    		echo "Error in query preparation/execution.\n";
    		die( print_r( sqlsrv_errors(), true));
        }    
      		
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
        {
        	    $datos[] = array("area" => $row['area'] ,
                                 "lugar" => $row['lugar'],
                                 "extension" => $row['extension'],
        	    				 "responsable" => $row['responsable']);        		
        }
    
        /* Free statement and connection resources. */
        sqlsrv_free_stmt( $stmt);
        sqlsrv_close( $conn);
        
        return $datos;        	
	}
       
     
    
}
