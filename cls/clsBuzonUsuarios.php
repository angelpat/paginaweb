<?php
/**
 * Clase para manipular el buzon del usuario
 */
class BuzonUsuarios
{
    /**
     * Conectar a base de datos
     */
    public function  __construct() {
		//include_once "connect.php";
		require_once "connect.php";   
    }    
        

    /***********************************************************************************************
    * 	selectDescripcionConmutador: ejecuta el SP webSP_insertBuzonUsuarios
    *
    *	@param  $datos 
    */
    
    public function insertBuzonUsuarios($datos){

    	$conn = connecToDB();
    	
    	$tsql = "exec webSP_insertBuzonUsuarios '". $datos['tipo'] ."','"
    											  . $datos['derechoabiencia'] ."','"
    	                                          . $datos['nombrePersonal'] ."','"
    	                                          . $datos['fecha'] . "','" 
    	                                          . $datos['turno'] . "','"
    											  . $datos['servicioAtencion'] . "','"
    	                                          . $datos['narrativa'] . "','"
    	                                          . $datos['nombre'] . "','"
    	                                          . $datos['domicilio'] . "','"
    	                                          . $datos['telefono'] . "','"
												  . $datos['mail'] . "'";
    	
        $stmt = sqlsrv_query( $conn, $tsql);
		if( $stmt === false)
    	{
			echo "Error in query preparation for <strong> webSP_insertBuzonUsuarios</strong> /execution.<br><br>\n";
    		die( print_r( sqlsrv_errors(), true));
        }    
       
        /* Free statement and connection resources. */
        sqlsrv_free_stmt( $stmt);
        sqlsrv_close( $conn);
        
       // return $datos;        	
	}
    
}
