<?php
class Cabecera
{

	public function  __construct() {

	}

	public function cabecera($nivel){		
		if($nivel == "0"){
		?>
			<header class="hgjms">
				<img src="img/content/logo-bcs.png" class="link-bcs" alt="Secretaria Salud">
				<a href="http://saludbcs.gob.mx/"><img src="img/content/logo-secretaria-salud.png" class="link-secretaria-salud" alt="Secretaria Salud"></a>				
			</header>
		<?php 							
		}elseif ($nivel =="1"){
		?>
			<header class="hgjms">
				<img src="../img/content/logo-bcs.png" class="link-bcs" alt="Secretaria Salud">
				<a href="http://saludbcs.gob.mx/"><img src="../img/content/logo-secretaria-salud.png" class="link-secretaria-salud" alt="Secretaria Salud"></a>				
			</header>
		<?php 							
		}else {
		?>
			<header class="hgjms">
				<img src="../../img/content/logo-bcs.png" class="link-bcs" alt="Secretaria Salud">
				<a href="http://saludbcs.gob.mx/"><img src="../../img/content/logo-secretaria-salud.png" class="link-secretaria-salud" alt="Secretaria Salud"></a>				
			</header>
		<?php 						
		}	
	}	 

	public function menu($path){ ?>
		<ul id="menu">		
			<li><a href="#" class="drop menu-home">Inicio</a>		
				<div class="dropdown_3columns">
					<div class="col_3">
						<h2>Bienvenida</h2>
					</div>
					<div class="col_1">
						<img src="<?php echo $path;?>css/menu/director.png" width="125" alt="" /> <br>
						<p>Dr. Roberto Rodr&iacute;guez Pulido</p>
						<p>Director del Hospital</p>
					</div>
					<div class="col_2">
						<p>El Benem&eacute;rito Hospital General con Especialidades "Juan
							Mar&iacute;a de Salvatierra" es una instituci&oacute;n que funciona
							como Hospital de concentraci&oacute;n Estatal y ofrece
							atenci&oacute;n M&eacute;dica de Segundo Nivel, con especialidades
							agregadas de mayor nivel como Cirug&iacute;a Pedi&aacute;trica,
							Nefrolog&iacute;a, Cirug&iacute;a Vascular, Odontopediatr&iacute;a,
							Gineco Obstetricia, entre otras; al laborar con lineamientos de
							humanismo y calidad.</p>
						<p>Al ser el primer Hospital que se instal&oacute; en Baja
							California Sur, cuenta con un historial de servicio y prestigio que
							enorgullece a los que actualmente tenemos el honor de laborar en
							el; y nos esforzamos por continuar enalteciendo su nombre.</p>
						<p>
							<strong style="text-align: center;">Bienvenidos<br>a esta
								instituci&oacute;n de salud
							</strong>
						</p>
					</div>
				</div>
			</li>
		
			<li><a href="#" class="drop menu-about-us">&iquest;Qui&eacute;nes
					somos?</a>
				<div class="dropdown_2columns">
					<div class="col_2">
						<h2>&iquest;Qui&eacute;nes somos?</h2>
					</div>
					<div class="col_2">
						<ul class="simple">
							<li><a href="#" class="menu-about-us"> <span
									class="ui-icon ui-icon-check"
									style="float: left; margin-right: .3em;"></span> Misi&oacute;n
							</a>
							</li>
							<li><a href="#" class="menu-about-us"> <span
									class="ui-icon ui-icon-check"
									style="float: left; margin-right: .3em;"></span> Visi&oacute;n
							</a>
							</li>
							<li><a href="#" class="menu-about-us"> <span
									class="ui-icon ui-icon-check"
									style="float: left; margin-right: .3em;"></span> Valores
							</a>
							</li>
						</ul>
					</div>
				</div>
			</li>
			<li><a href="#" class="drop menu-services">Servicios</a>
				<div class="dropdown_4columns">
					<div class="col_2">
						<h2>Servicios</h2>
					</div>
					<div class="col_2">
						<h2>Departamentos</h2>
					</div>
					<div class="col_2">
						<a href="#" class="menu-archivo">Archivo Cl&iacute;nico</a><br> 
						<a href="#" class="menu-cirugia">Cirug&iacute;a</a><br> 
						<a href="#" class="menu-consulta">Consulta Externa</a><br> 
						<a href="#" class="menu-imagenologia">Imagenologia (Rayos X)</a><br> 
						<a href="#" class="menu-laboratorio">Laboratorio</a><br> 
						<a href="#" class="menu-unidad-quirurgica">Unidad Quir&uacute;rgica</a><br> 
						<a href="#" class="menu-urgencias">Urgencias</a><br> 
						<a href="#" class="menu-services"><strong>Ver mas...</strong> </a>
						<h2>Galer&iacute;as de imagenes</h2>
						<img
							src="<?php echo $path;?>img/servicios/pediatria/ucin/galeria/img-10.jpg"
							style="width: 60px;" class="img_left imgshadow" alt="" /> <br> <br>
						<p>
							UCIN<a href="#" class="menu-ucin-galeria">Ir a
								galer&iacute;a...</a>
						</p>
						<img src="<?php echo $path;?>img/servicios/unidad.quirurgica/galeria/1468-thumb.JPG"
							style="width: 60px;" class="img_left imgshadow" alt="" /> <br> <br>
						<p>
							Unidad Quir&uacute;rgica<a href="#"
								class="menu-unidad-quirurgica-galeria">Ir a galer&iacute;a...</a>
						</p>
					</div>
					<div class="col_2">
						<a href="#" class="menu-calidad">Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</a><br> 
						<a href="#" class="menu-ensenanza">Ense&ntilde;anza en Investigaci&oacute;n</a><br> 
						<a href="#" class="menu-informatica">Ingenieria en Sistemas de Informaci&oacute;n</a><br> 
						<a href="#"	class="menu-nutricion">Nutrici&oacute;n, Diet&eacute;tica y Cocina</a><br>
						<a href="#"	class="menu-ts">Trabajo Social</a><br>
						<a href="#" class="menu-services"><strong>Ver mas...</strong> </a>
						<h2>Solicitud de Cirug&iacute;a</h2>
						<img class="img_left imgshadow"
							src="<?php echo $path;?>css/menu/solicitud-cirugia-thumb.png" alt="Organigrama"
							style="width: 50px;" /><br> <br>
						<p>Revise el estado de su Solicitud de Cirug&iacute;a</p>
						<a href="#" class="menu-cirugia-proceso">Solicitud de Cirug&iacute;a</a>
					</div>
				</div>
			</li>
		
			<li><a href="#" class="drop menu-organization">Organigrama</a> <!-- Begin 5 columns Item -->
				<div class="dropdown_3columns">
					<!-- Begin 5 columns container -->
					<div class="col_3">
						<h2>Organigrama</h2>
					</div>
					<div class="col_1">
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Abril de 2012
						</p>
						<a target="_blank"
							href="<?php echo $path;?>resources/content/Organigrama-2012-04-18.pdf"
							class="organigrama"><img src="<?php echo $path;?>img/icon/pdf.png"
							alt="organigrama.pdf" title="organigrama.pdf" width="16"
							height="16"> Descargar</a>
					</div>
					<div class="col_1">
						<img class="img_left imgshadow" src="<?php echo $path;?>css/menu/organigrama.png"
							alt="Organigrama" style="width: 250px;" />
					</div>
				</div>
			</li>
			<li class="menu_right"><a href="#" class="drop">Correo</a>
				<div class="dropdown_2columns align_right">
					<div class="col_2">
						<h2>Correo</h2>
					</div>
					<div class="col_1" style="width: 70px;">
						<img src="<?php echo $path?>css/menu/email.png" width="66" alt="" class="imgshadow" />
					</div>
					<div class="col_1">
						<p>Iniciar Sesi&oacute;n</p>
							

						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>
							<a href="https://www.hgejms.gob.mx/">Correo Salvatierra</a>
						</p>					
					</div>
				</div>
			</li>

			<li class="menu_right menu-commutator"><a href="#" class="drop">Connmutador</a>
				<div class="dropdown_2columns align_right">
					<div class="col_2">
						<h2>Conmutador</h2>
					</div>
					<div class="col_1" style="width: 70px;">
						<img src="<?php echo $path?>css/menu/telefono.jpg" width="66" alt="" class="imgshadow" />
					</div>
					<div class="col_1">
						<p>Busqueda por:</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Extensiones
						</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>M&eacute;dicos
						</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Administrativos
						</p>
					</div>
				</div>
			</li>
		
			<li class="menu_right menu-location"><a href="#" class="drop">Localizaci&oacute;n</a>
				<div class="dropdown_3columns align_right">
					<div class="col_3">
						<h2>Localizaci&oacute;n</h2>
					</div>
					<div class="col_3">
						<table>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td><p>Direcci&oacute;n:</p></td>
								<td><p>	Blvd. Paseo de los Deportistas #5115 <br>e/ Misioneros
										Combonianos y Luis Barajas
									</p></td>
							</tr>
							<tr>
								<td><p>Colonia:</p></td>
								<td><p>8 de Octubre 2da Secci&oacute;n</p></td>
							</tr>
							<tr>
								<td><p>CP:</p></td>
								<td><p>23085</p></td>
							</tr>
							<tr>
								<td><p>Conmutador:</p></td>
								<td><p>612-17-5-05-00</p></td>
							</tr>
							<tr>
								<td><p>Lada sin costo:</p></td>
								<td><p>01-800-837-6640</p></td>
							</tr>
							<tr>
								<td><p>Correo:</p></td>
								<td><p>hgejms@gmail.com</p></td>
							</tr>
						</table>
					</div>
		
				</div>
			</li>
			<li class="menu_right menu-mailbox"><a href="#" class="drop">Buz&oacute;n</a>
				<div class="dropdown_2columns align_right">
					<div class="col_2">
						<h2>Buz&oacute;n de Usuario</h2>
					</div>
					<div class="col_1" style="width: 70px;">
						<img src="<?php echo $path;?>css/menu/buzon-usuario.png" width="66" alt="" />
					</div>
					<div class="col_1">
						<p>H&aacute;ganos llegar sus:</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Felicitaciones
						</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Sugerencias
						</p>
						<p>
							<span class="ui-icon ui-icon-check"
								style="float: left; margin-right: .3em;"></span>Quejas
						</p>
					</div>
				</div>
			</li>
		</ul>				
	<?php 
	}		
	
	public function pie(){?>
		<footer class="hgjms">
			<section class="footer-izq">
				Blvd. Paseo de los Deportistas 5115&nbsp;<br /> e/ Misioneros
				Combonianos y Luis Barajas&nbsp;<br /> Col. 8 de Octubre, 2da
				Secci&oacute;n C.P. 23085&nbsp;
			</section>
			<section class="footer-der">
				&nbsp;Tel. 612 17 5 05 00<br /> &nbsp;Lada sin costo: 01 800 837 6640<br />
				&nbsp;hgejms@gmail.com
			</section>
			<section class="footer-cen">La Paz, Baja California Sur, M&eacute;xico
			</section>
		</footer>
	<?php 
	}
	
	public function tipSalud($path){ ?>
		<div class="blue-top">
			<img src="<?php echo $path;?>img/content/crn-tl-blue.gif" alt="" class="crn-tl-blue" /> <img
				src="<?php echo $path;?>img/content/crn-tr-blue.gif" alt="" class="crn-tr-blue" />
		</div>
		<div class="blue-content">
			<div id="boxcontrol" class="boxcontrol">
				<table id="tabla-tips">
					<thead>
						<tr>
							<td colspan="4">TIPS DE SALUD</td>
						</tr>
					</thead>
					<tr>
						<td><a href="#" class="tip-salud-embarazada"><img
								src="<?php echo $path;?>img/tip-salud/embarazada.png" alt="�Estas Embarazada?" /> </a>
						</td>
						<td><a href="#" class="tip-salud-embarazada">Embarazada</a></td>
						<td><a href="#" class="tip-salud-diabetico"><img
								src="<?php echo $path;?>img/tip-salud/diabetico.png" alt="�Eres Diabetico?" /> </a></td>
						<td><a href="#" class="tip-salud-diabetico">Diabetico</a></td>
					</tr>
					<tr>
						<td><a href="#" class="tip-salud-hipertenso"><img
								src="<?php echo $path;?>img/tip-salud/hipertenso.png" alt="�Estas Hipertenso?" /> </a>
						</td>
						<td><a href="#" class="tip-salud-hipertenso">Hipertenso</a></td>
						<td><a href="#" class="tip-salud-tosedor"><img
								src="<?php echo $path;?>img/tip-salud/tosedor.png" alt="�Eres Tosedor Cronico?" /> </a>
						</td>
						<td><a href="#" class="tip-salud-tosedor">Tosedor Cr&oacute;nico</a>
						</td>
					</tr>
					<tr>
						<td><a href="#" class="tip-salud-sida"><img
								src="<?php echo $path;?>img/tip-salud/sida.png" alt="Quien puede tener SIDA?" /> </a>
						</td>
						<td><a href="#" class="tip-salud-sida">SIDA</a></td>
						<td><a href="#" class="tip-salud-violencia"><img
								src="<?php echo $path;?>img/tip-salud/violencia.png"
								alt="�Sufres de Violencia Intrafamiliar?" /> </a></td>
						<td><a href="#" class="tip-salud-violencia">Violencia Intrafamiliar</a>
						</td>
					</tr>
					<tr>
						<td><a href="#" class="tip-salud-diarrea"><img
								src="<?php echo $path;?>img/tip-salud/diarrea.png" alt="�Tu hijo tiene Diarrea?" /> </a>
						</td>
						<td><a href="#" class="tip-salud-diarrea">Diarrea</a></td>
						<td><a href="#" class="tip-salud-neumonia"><img
								src="<?php echo $path;?>img/tip-salud/neumonia.png" alt="Neumonia" /> </a></td>
						<td><a href="#" class="tip-salud-neumonia">Neumonia</a></td>
					</tr>
					<tr>
						<td><a href="#" class="tip-salud-vacunas"><img
								src="<?php echo $path;?>img/tip-salud/vacunas.png" alt="Vacunas" /> </a></td>
						<td><a href="#" class="tip-salud-vacunas">Vacunas</a></td>
						<td><a href="#" class="tip-salud-dengue"><img
								src="<?php echo $path;?>img/tip-salud/dengue.png" alt="Dengue" /> </a></td>
						<td><a href="#" class="tip-salud-dengue">Dengue</a></td>
					</tr>
					<tr>
						<td><a href="#" class="tip-salud-planificacion"><img
								src="<?php echo $path;?>img/tip-salud/planificacion.png" alt="Planificacion Familiar" />
						</a></td>
						<td><a href="#" class="tip-salud-planificacion">Planificaci&oacute;n
								Familiar</a></td>
						<td><a href="#" class="tip-salud-recien-nacido"><img
								src="<?php echo $path;?>img/tip-salud/recien-nacido.png"
								alt="Hipotiroidismo (Recien Nacido)" /> </a></td>
						<td><a href="#" class="tip-salud-recien-nacido">Hipotiroidismo
								(Reci&eacute;n Nacido)</a></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="blue-btm">
			<img src="<?php echo $path;?>img/content/crn-bl-blue.gif" alt="" class="crn-bl-blue" /> <img
				src="<?php echo $path;?>img/content/crn-br-blue.gif" alt="" class="crn-br-blue" />
		</div>
	<?php 
	}
	
}
