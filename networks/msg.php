<?php 
include_once 'cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />		
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>   
		<script>
			$(function(){			
			});		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>          
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");				
			?>				
			<div id="content">	
				<br><br><br>
				<!--Aqui va un titulo con formato dise�o 1-->
				<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto; width: 800px;">													
					<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">DEPTO. DE INGENIER&IacuteA EN SISTEMAS DE INFORMACI&OacuteN </p><br/>	
				</div>
				<br/>							
								
				<!--Aqui va un titulo con formato dise�o 2	 					
				<p class="organigrama-hgjms">BENEM&Eacute;RITO HOSPITAL GENERAL CON ESPECIALIDADES "JUAN MARIA DE SALVATIERRA"</p>
				<br/> -->
						
				<!--Aqui va una imagen									
				<img id="organigrama" src="img/msg/construccion.jpg" style="width:950px;" alt="mensaje"/>				
				<br/>	 -->
					<br><br><br>
				<!--Aqui va un mensaje de error-->	
				<div class="ui-state-error ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:930px;" > 
					<br><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Mensaje de error!</strong></p>
					<div>
						<br>
						<b>P&#225gina negada por Pol&iacute;ticas de Seguridad Inform&aacute;tica</b>
						<br> <!--
						<b> Raz&oacute;n: </b> %(reason)% <br>
						
						<b> Portal: </b> %(url-host)% <br>
						--><br>
					</div>
				</div>												
				<br/>		
					<br><br><br>					
				<!--Aqui va un mensaje de alerta-->	
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:930px;" > 
					<br><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Informacion!</strong></p>
					<div> <BR> Esta acci&oacute;n se ha guardado en bit&#225cora												
					</div><br>
				</div>	
												
			</div>
			<?php 
				$objCabecera->pie();
			?>	
		</div>               	 
    </body>
</html>