<?php

$type = $_GET['type'];

/**
 * Type 1: Muestra Esfigmomanómetro 
 * Type 2: Termometro 
 * Type 3: Medidor de Flujo Máximo Pulmonar 
 * Type 4: Medidor de Pulso
 * Type 3: Carga de captura de datos  
 */

switch ($type) {
    case 1:
?>
	
	<div style="float: left; margin-top:20px;  margin-left:320px;" class="titulo-triage">
		Esfigmoman&oacute;metro
	</div>
	
	<div class="imagen-Material">
		<img alt="" src="../../img/servicios/urgencias/presion150.jpg">
	</div>
	<p class="descripcion-Material">
		Un esfigmoman&oacute;metro es un instrumento m&eacute;dico empleado para la medici&oacute;n indirecta de la presi&oacute;n arterial, 
		que la suele proporcionar en unidades f&iacute;sicas de presi&oacute;n, por regla general en mil&iacute;metros de mercurio (mmHg o torr).
		En el ciclo de bombeo el coraz&oacute;n y el sistema circulatorio pasa por un m&aacute;ximo de presi&oacute;n que coincide con el bombeo de sangre
	    (s&iacute;stole o contracci&oacute;n), tras este punto de m&aacute;xima presi&oacute;n el coraz&oacute;n se relaja y se llena de sangre procedente de las venas, 
		alcanzando un m&iacute;nimo de presi&oacute;n (di&iacute;stole o relajaci&oacute;n). Completando de esta forma un ciclo card&iacute;aco. El esfigmoman&oacute;metro se emplea 
		como instrumento de medida de estos valores extremos de presi&oacute;n debidos al flujo sangu&iacute;neo, es decir de la presi&oacute;n sist&oacute;lica 
		(de contracci&oacute;n del coraz&oacute;n, o de bombeo) 
	</p>
<?php
    break;
 
    case 2:
?>  	
	<div style="float: left; margin-top:20px;  margin-left:320px;" class="titulo-triage">
		Termometro
	</div>
	
	<div class="imagen-Material">
		<img alt="" src="../../img/servicios/urgencias/termo150.jpg">
	</div>
	<p class="descripcion-Material">
		Es un instrumento de medici&oacute;n de temperatura que usa el principio de la dilataci&oacute;n, por lo que se prefiere el uso de materiales con un 
		coeficiente de dilataci&oacute;n alto de modo que, al aumentar la temperatura, la dilataci&oacute;n del material sea f&aacute;cilmente visible.. Este aparato 
		es com&uacute;nmente empleado para tomar la temperatura, de una persona. Asimismo, el term&oacute;metro, se utiliza de igual manera, para medir la
		temperatura, en los animales, por parte de los veterinarios. En la actualidad, es la manera m&aacute;s pr&aacute;ctica, para saber o conocer, qu&eacute; temperatura
		corporal posee una persona.
	</p>
<?php
    break;
    
    case 3:	
?>
    	<div style="float: left; margin-top:20px;  margin-left:320px;" class="titulo-triage">
    		Medidor de Flujo M&aacute;ximo Pulmonar
    	</div>
    	
    	<div class="imagen-Material">
    		<img alt="" src="../../img/servicios/urgencias/respiracion150.jpg">
    	</div>
    	<p class="descripcion-Material">
	    	El flujo espiratorio m&aacute;ximo es la velocidad m&aacute;s r&aacute;pida en la que los pulmones expulsan aire hacia fuera despu&eacute;s de respirar hondo. 
	    	Esta medici&oacute;n es &uacute;til para detectar cambios en las v&iacute;as respiratorias que podr&iacute;an indicar un empeoramiento de los s&iacute;ntomas 
	    	o una mejora de la respiraci&oacute;n en personas con afecciones respiratorias como asma o enfisema.
    	</p>
 <?php
    break;
    
    case 4:
?>
		<div style="float: left; margin-top:20px;  margin-left:320px;" class="titulo-triage">
			Medidor de Pulso
		</div>		 
		<div class="imagen-Material">
			<img alt="" src="../../img/servicios/urgencias/pulso150.jpg">
		</div>
		<p class="descripcion-Material">
			Permite escuchar y medir de forma sencilla la frecuencia del pulso card&iacute;aco. Con su utilizaci&oacute;n podemos controlar si nuestras pulsaciones son normales o si sufrimos alg&uacute;n tipo de alteraci&oacute;n.
		</p>
	<?php
    break;  
    
	case 5:

	break;
}