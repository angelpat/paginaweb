<?php 
include_once 'cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />		
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="js/msg-denied-page.js"></script>   
		<script>
			$(function(){	
				$('.btn').button();		
			});		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>          
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");				
			?>				
			<div id="content">	
				<br><br><br>
				<!--Aqui va un titulo con formato dise�o 1-->
				<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">DEPTO. DE INGENIER&Iacute;A EN SISTEMAS DE INFORMACI&Oacute;N </p><br/>	
				</div>
				<br/>							
								
				<!--Aqui va un titulo con formato dise�o 2	 					
				<p class="organigrama-hgjms">BENEM&Eacute;RITO HOSPITAL GENERAL CON ESPECIALIDADES "JUAN MARIA DE SALVATIERRA"</p>
				<br/> -->
						
				<!--Aqui va una imagen									
				<img id="organigrama" src="img/msg/construccion.jpg" style="width:950px;" alt="mensaje"/>				
				<br/>	 -->
				<br><br>								
				
				<!--Aqui va un mensaje de error-->	
				<div class="ui-state-error ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:930px;" id="msg-error" > 
					<br><p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
					<strong>Mensaje de error!</strong></p>
					<div>
						<br>
						<b>P&aacute;gina negada por Pol&iacute;ticas de Seguridad Inform&aacute;tica</b><br><br> 

					<!-- 	<b> Raz&oacute;n: <?php echo str_replace("'", " ", $_GET["reason"]); ?></b>
						
						
						<input type="hidden" id="reason" value="<?php echo str_replace("'", " ", $_GET["reason"]); ?>"> 
						<br><br>						
						<b> Portal: </b> <?php echo $_GET["urlhost"]; ?>
						<input type="hidden" id="urlhost" value="<?php echo $_GET["urlhost"]; ?>"> 													
						<br> 

						<br>  -->
					</div>
				</div>												
				<br>		
					<br><br>
															
				<!--Aqui va un mensaje de alerta-->	
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:930px;" id="msg-alerta" > 
					<br><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Informacion!</strong></p>
					<div> 
                        <br>Esta acci&oacute;n se ha guardado en bit&aacute;cora  	<br> 	
						<br> <b>Contacto: <a href="mailto:redes@hgejms.gob.mx">redes@hgejms.gob.mx</a> </b> <br> 
					</div><br>
				</div>	
 				<!-- <table  border="0" cellpadding="0" cellspacing="0" width=100%>
      					<tr><td align="center" > 
       					         <iframe  src="/contador/counter.php" scrolling="no" border="0" frameborder="0" width="100" height="50" ></iframe>
 					</td></tr>
			        </table>  -->
			    <br><br>
			        		
	<!--
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:930px;" id="msg-correo"> 
					<br><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Informacion!</strong></p>
					<div> 
                        <br>La peticion se envio correctamente a <b><a href="mailto:redes@hgejms.gob.mx">redes@hgejms.gob.mx</a> </b> <br> 
					</div><br>
				</div>				    
			    
			    <table class="ui-widget ui-widget-content" id="tabla" width=100%>
			    	<thead>
			    		<tr class="ui-widget-header ">
			    			<th colspan="2" style="padding: 10px;">Solicitar desbloqueo de p&aacute;gina</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		<tr>
			    			<td colspan="2" style="padding: 10px 10px 0 10px;">
			    				<p id="tip" class="validateTips ui-corner-all" style='padding:0.7em; font-size:100%;'>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td style="padding: 10px;">
			    				<b class="requerido">*</b> <label for="nombre">Nombre del usuario</label>
			    			</td>
			    			<td style="padding: 10px;">			    				
								<input type="text" name="nombre" id="nombre" value="" placeholder="Nombre" class="text ui-widget-content ui-corner-all" size="100;" style="height: 20px;"/>			    						    			
			    			</td>
			    		</tr>		
			    		<tr>
			    			<td style="padding: 10px;">
			    				<b class="requerido">*</b> <label for="servicio">Servicio</label>
			    			</td>
			    			<td style="padding: 10px;">			    				
								<input type="text" name="servicio" id="servicio" value="" placeholder="Servicio" class="text ui-widget-content ui-corner-all" size="100;" style="height: 20px;"/>			    						    			
			    			</td>
			    		</tr>				    			    		
			    		<tr>
			    			<td style="padding: 10px;">
			    				<b class="requerido">*</b> <label for="correo">Introduzca un correo valido</label>
			    			</td>
			    			<td style="padding: 10px;">			    				
								<input type="text" name="correo" id="correo" value="" placeholder="correo@dominio.com" class="text ui-widget-content ui-corner-all" size="100;" style="height: 20px;"/>			    						    			
			    			</td>
			    		</tr>	
			    		<tr>
			    			<td style="padding: 10px;"><label for="motivo">Motivo o justificaci&oacute;n</label></td>
			    			<td style="padding: 10px;">			    				
								<input type="text" name="motivo" id="motivo" value="" placeholder="Motivo" class="text ui-widget-content ui-corner-all" size="100;" style="height: 20px;"/>			    						    			
			    			</td>
			    		</tr>				    				    		
			    		<tr>
			    			<td></td>
			    			<td><a id="btn-enviar" class="btn" style="width: 100px; margin: 0 0 10px 10px;" >Enviar</a></td>
			    		</tr>
			   		 </tbody>
			    </table>
			        
			</div>   -->
			<?php 
				$objCabecera->pie();
			?>	
		</div>               	 
    </body>
</html>