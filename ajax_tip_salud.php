<?php

$type = $_GET['type'];

/**
 * Type 1: Tips - Embarazada
 * Type 2: Tips - Diabetico
 * Type 3: Tips - Hipertenso
 * Type 4: Tips - Tosedor Cronico
 * Type 5: Tips - SIDA
 * Type 6: Tips - Violencia Intrafamiliar
 * Type 7: Tips - Diarrea
 * Type 8: Tips - Neumonia
 * Type 9: Tips - Vacunas
 * Type 10: Tips - Dengue
 * Type 11: Tips - Planificacion familiar
 * Type 12: Tips - 
 */
switch ($type) {
    case 1:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Estas embarazada?</p>
	<br /><br />
	<p> 
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Acude a tu Centro de Salud mas cercano para control de embarazo.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Debes de Aplicarte la Vacuna Td.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Tomar Acido Folico.<br /><br />
	<!-- 	<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>En el Hospital Salvatierra se dan pl&aacute;ticas los Jueves a las 8:30am para embarazadas, detecci&oacute;n oportuna de VDRL, VIH. -->
	</p>
<?php
    break;
 
    case 2:
?>   
	<br />
	<p class="titulo-tip-salud">&iquest;Eres Diabetico?</p> 
	<br /><br />	
	<p>
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Acude regularmente a control m&eacute;dico para la vigilancia del tratamiento.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Haz ejercicio 3 veces a la semana por 20 minutos.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Dieta baja en grasas y azucares.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Cuida tus pies, utilizando calzado comodo, evita medias, tacon alto, si notas alguna lesion, acude de inmediato con tu M&eacute;dico Familiar.
	</p>
<?php
    break;
    case 3:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Eres Hipertenso?</p>
	<br /><br />
	<p>
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Acude a control con tu M&eacute;dico Familiar regularmente.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Ejercicio 3 veces a la semana por 20 minutos, si el m&eacute;dico lo autoriza.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Dieta baja en grasas y en sal, comer abundantes verduras.<br /><br />
		<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>Si presentas dolor de cabeza intenso acude con tu m&eacute;dico familiar.<br /><br />
	</p>
<?php    	
	break;    
	case 4:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Eres Tosedor Cr&oacute;nico?</p>
	<br /><br />
	<p class="texto sangria">Presentas tos de larga evoluci&oacute;n?</p><br/><br/>
	<p class="texto sangria">		
		CUIDADO!! puede ser TUBERCULOSIS PULMONAR,
		acude con tu m&eacute;dico familiar y con una prueba sencilla se puede determinar, 
		la Tuberculosis es curable, si se lleva bien control en tratamiento.	
	</p>
<?php    	
	break;
	case 5:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Quien puede tener SIDA?</p>
	<br /><br />
	<p class="texto">
		Todos estamos expuestos, si tenemos relaciones sexuales sin protecci&oacute;n.<br />
		El SIDA no es exclusivo de homosexuales (hombres que tienen sexo con hombres).<br /> 
		Hoy en d&iacute;a, hay pacientes mujeres, ni&ntilde;os, hombres heterosexuales, bisexuales, con problemas de adicci&oacute;n a las drogras, etc.<br />		
		<!-- 	Deseas hacerte la prueba? Acude a MEDICINA PREVENTIVA EN LA TORRE 2DO PISO a solicitarla RECUERDA!! SOLO CON TU PAREJA. -->
	</p>
<?php    	
	break;	
	case 6:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Sufres de Violencia Intrafamiliar?</p>
	<br /><br />
	<p class="texto sangria">	
		Acude a nuestro M&oacute;dulo de atenci&oacute;n en el Hospital Salvatierra donde se dara apoyo y orientaci&oacute;n, 
		contamos con trabajadoras, Psicolog&iacute;a y atenci&oacute;n m&eacute;dica, recuerda, <strong>NO ESTAS SOLA(O).</strong>
	</p>
<?php    	
	break;
	case 7:
?>
	<br />
	<p class="titulo-tip-salud">&iquest;Tu hijo tiene Diarrea?</p>
	<br /><br />
	<p class="texto sangria">	
		Inicia con SUERO VIDA ORAL, y ofrecele una Onza despues de cada evacuaci&oacute;n, 
		si presenta fiebre, llanto sin lagrimas, esta poco reactivo, acude inmediatamente con tu m&eacute;dico familiar
	</p>
<?php    	
	break;	
	case 8:
?>
	<br />
	<p class="titulo-tip-salud">Neumonia</p>
	<br /><br />
	<p class="texto sangria">	
		Si tu hijo(a) esta resfriado, pero presenta datos de difucultad respiratoria, 
		fiebre; acude inmediatamente al m&eacute;dico, puede ser neumonia.
	</p>
<?php    	
	break;
	case 9:
?>
	<br />
	<p class="titulo-tip-salud">Vacunas</p>
	<br /><br />
	<p class="texto sangria">	
		El Hospital Salvatierra, cuenta con el esquema b&aacute;sico de Vacunaci&oacute;n 
		de LUNES a VIERNES de 8:00 a 12:30 Hrs. 
	</p>
<?php    	
	break;
	case 10:
?>
	<br />
	<p class="titulo-tip-salud">Dengue</p>
	<br /><br />
	<p class="texto sangria">	
	Tienes fiebre, dolor muscular de articulaciones, retro ocular, acude con tu m&eacute;dico puede ser Dengue. 
	No hay vacuna para el dengue, pero lo podemos prevenir, manteniendo el patio limpio, si almacenas agua en tambos, 
	mantenerlos tapados o le puedes aplicar ABATE en los depositos descubiertos. 
	De nosotros depende la reproducci&oacute;n del mosquito.
	</p>
<?php    	
	break;	
	case 11:
?>
	<br />
	<p class="titulo-tip-salud">Planificaci&oacute;n Familiar</p>
	<br /><br />
	<p class="texto sangria">	
		Deseas llevar control de fertilidad?... acude al servicio de PLANIFICACI&Oacute;N FAMILIAR, se 
		cuenta con m&eacute;todos temporales (DIU, Hormonas orales, Hormonas inyectables, preservativos)
		M&eacute;todos definitivos (Salpingoclasia y Vasectom&iacute;a sin bisturi).
	</p>
<?php    	
	break;
	case 12:
?>
	<br />
	<p class="titulo-tip-salud">Hipotiroidismo (Reci&eacute;n Nacido)
	<br /><br />
	<p class="texto sangria">	
		Con una prueba sencilla se detecta Hipotiroidismo en el reci&eacute;n nacido y hasta antes de los 30 d&iacute;as de nacido, 
		se realiza Tamiz, ya que se debe de iniciar tratamiento antes de los 2 meses de nacido, si se reporta positivo.
	</p>
<?php    	
	break;				
    default:
    break;
?>			
<?php      
}