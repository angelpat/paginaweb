<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>   
		<script src="js/menu.js"></script>       		
		<script src="js/localizacion.js"></script>    		
 		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<section id="modal-localizacion"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>     	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>	
			<div id="content">					
				<!-- 
				<p class="localizacion-titulo">LOCALIZACI&Oacute;N</p> --><br/>
				<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto; width: 470px;">													
					<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">LOCALIZACI&Oacute;N</p><br/>	
				</div>
				<br>
				<p class="localizacion-hgjms">BENEM&Eacute;RITO HOSPITAL GENERAL CON ESPECIALIDADES "JUAN MARIA DE SALVATIERRA"</p><br/>	
				<section class="localizacion-izq">				
					<iframe width="590" height="355" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.mx/?ie=UTF8&amp;t=h&amp;vpsrc=0&amp;ll=24.113071,-110.31621&amp;spn=0.006855,0.01266&amp;z=16&amp;output=embed"></iframe>
					<br />
				</section>
				<section class="localizacion-der">				
					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png'); margin: auto; ">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">Direcci&oacute;n</p><br/>	
					</div>
					<table id="table-localizacion">
						<thead>
							<tr>								
								<td colspan="2"></td>
							</tr>	
						</thead>
							<tr>
								<td>Direcci&oacute;n:</td>
								<td>Blvd. Paseo de los Deportistas #5115 e/ Misioneros Combonianos y Luis Barajas</td>
							</tr>
							<tr>
								<td>Colonia:</td>
								<td>8 de Octubre 2da Secci&oacute;n</td>
							</tr>
							<tr>
								<td>CP:</td>
								<td>23085</td>
							</tr>		
							<tr>	
								<td>Conmutador:</td>
								<td>612-17-5-05-00</td>
							</tr>
							<tr>	
								<td>Lada sin costo</td>
								<td>01-800-837-6640</td>
							</tr>							
							<tr>	
								<td>Correo:</td>
								<td>hgejms@gmail.com</td>
							</tr>							
					</table><br/>
					<a id="btn-zoom" class="btn" style="float: left; top: 40px">Zoom</a>
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width:240px; float: right;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						<div >* Para obtener una copia el archivo haga clic en 							
							<strong><a target="_blank" href="img/content/croquis.png" class="localizacion"> descargar</a></strong>
						</div>
					</div>
	
				</section>		
			</div>
			<?php 
				$objCabecera->pie();
			?>			
		</div>               	 
    </body>
</html>