<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<link rel="stylesheet" href="cleditor/jquery.cleditor.css" />		
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="js/menu.js"></script>   		      
		<script src="js/buzon-contacto.js"></script>    
		<script src="cleditor/jquery.cleditor.min.js"></script>    
		<script >	
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>          
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>				
			<div id="content">		
				<div id="modal-mensaje">
					<br>El mensaje ha sido enviado
				</div>							
				<section class="buzon-content">
				
					<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto; width: 470px;">													
						<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">CONTACTO</p><br/>	
					</div>		
					<div class="ui-state-highlight ui-corner-all"
						style="float: left; margin-left: 10px; padding: 0.7em; font-size: 95%; width: 760px; margin-top: 10px;">
						<p>
							<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> 
							<strong>
								Nota!
							</strong>
						</p>
						<div>
							Los campos marcados con un <b><font class="requerido">*</font></b>
							son necesarios para el env&iacute;o de su mensaje
						</div>					
					</div>					
					<br>	
					<div class="blue-top" style="margin-top: 50px;">
						<img src="img/content/crn-tl-blue.gif" alt="" class="crn-tl-blue" />
						<img src="img/content/crn-tr-blue.gif" alt="" class="crn-tr-blue" />
					</div>
					<div class="blue-content">
	
						<div id="boxcontrol" class="boxcontrol">
	
							<form action="" id="frm-buzon">	
								<table>
									<thead>
										<tr>
											<td>
												<p id="tip" class="validateTips ui-corner-all" style='padding:0.7em; font-size:100%; width:725px;'><br/>											
											</td>
										</tr>
									</thead>
								</table>
								<table class="tabla-buzon">
									<thead>
										<tr>
											<th width="100"></th>
											<th width="650"></th>
										</tr>
									</thead>
									<tr>
										<td>
											<label for="nombre"><b>Nombre:&nbsp;&nbsp;&nbsp;</b></label><b><font class="requerido">*</font> </b>
										</td>
										<td>
											<input type="text" id="nombre" name="nombre" placeholder="Nombre(s)" size="105">
										</td>
									</tr>
									<tr>
										<td>
											<label for="apellido"><b>Apellidos:&nbsp;</b></label><b><font class="requerido">*</font></b> 
										</td>
										<td>
											<input type="text" id="apellido" name="apellido" placeholder="Apellido Materno y Paterno" size="105">
										</td>
									</tr>
									<tr>
										<td>
											<label for="domicilio"><b>Domicilio:&nbsp;</b></label><b><font class="requerido">*</font> </b>
										</td>
										<td>
											<input type="text" id="domicilio" name="domicilio" placeholder="Calle No. Colonia" size="105">
										</td>
									</tr>
									<tr>
										<td>
											<label for="telefono"><b>Tel&eacute;fono:</b></label>
										</td>
										<td>
											<input type="tel" id="telefono" name="telefono" placeholder="612 17 5 05 00" size="105">
										</td>
									</tr>
									<tr>
										<td>
											<label for="correo-e"><b>Correo-e:&nbsp;</b></label><b><font class="requerido">*</font></b>
										</td>
										<td>
											<input type="text" id="correo-e" name="correo-e" placeholder="nombre@correo.com" size="105">
										</td>
									</tr>
								</table>
								<br /> <br />
								<label for="narrativa"><b>Comentarios:&nbsp;</b></label><b><font class="requerido">*</font></b>
								<br /> <br />
								<textarea name="narrativa" id="narrativa"class="text ui-widget-content ui-corner-all cleditor"></textarea>
								<br /> <br />
							</form>
							<a id="btn-enviar"
								style="display: block; margin: auto; width: 100px;">Enviar</a>
						</div>	
					</div>
					<div class="blue-btm">
						<img src="img/content/crn-bl-blue.gif" alt="" class="crn-bl-blue" />
						<img src="img/content/crn-br-blue.gif" alt="" class="crn-br-blue" />
					</div>
	
				</section>						
			</div>
			<?php 
				$objCabecera->pie();
			?>
		</div>               	 
    </body>
</html>