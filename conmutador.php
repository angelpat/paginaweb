<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="js/menu.js"></script>           
		<script>
			$(function(){
					
				$( "#modal-contacto" ).dialog({
					autoOpen: false,
					show: "blind",
					hide: "explode"
				});
					
				$("#menu-contact").click(function(){
					$( "#modal-contacto" ).dialog( "open" );
					return false;		
				});	

				$(document).ready( function(){	
					$("#rb-ext").attr("checked", true);
					var data ='type=1&buscar=&tipo=';
                    $( "#directorio" ).load('ajax_conmutador.php',data,'');	
				});		
				
				$('#buscar').live('keyup', function(){
					var tipo = $(":checked").val();
					var data ='type=1&buscar=' + $(this).val() +
					                '&tipo=' + tipo;
                    $( "#directorio" ).load('ajax_conmutador.php',data,'');
                });

				$( "#radio" ).buttonset();  
				
				$( "#rb-ext" ).button({ icons: {primary:'ui-icon-contact',secondary:null} })
				$( "#rb-med" ).button({ icons: {primary:'ui-icon-contact',secondary:null} })
				$( "#rb-adm" ).button({ icons: {primary:'ui-icon-contact',secondary:null} })				

				$( "#rb-ext, #rb-med, #rb-adm").click(function(){
					var data ='type=1&buscar=' + $('#buscar').val() +
					                '&tipo=' + $(this).val();			
					$( "#directorio" ).load('ajax_conmutador.php',data,'');		
				});	
						
			});		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>      
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>      
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>				
			<section id="content" style="padding-top: 0px">	
				<section class="conmutador-izq">
					<div class="barra-01" style="background-image: url('img/content/barra-01-l.png');">													
						<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">CONMUTADOR</p><br/>	
					</div>						
				</section>				
				<section class="conmutador-der">
					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png');">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">BUSCAR <input type="text" name="buscar" id="buscar" placeholder="Buscar" class="ui-corner-all"></p>
						<!-- <span class="ui-icon ui-icon-search" style="float: left; margin-right: .3em; margin-left: 10px;" ></span> -->				 		
					</div>							
				</section>								
			 	<section  style="padding-top: 10px; float: left;">
			 	
					<div id="radio">
						<input type="radio" id="rb-ext" name="rb-tipo" class="tipo" value=""><label for="rb-ext">Extensiones</label>
						<input type="radio" id="rb-med" name="rb-tipo" class="tipo" value="med"><label for="rb-med">Medicos</label>
						<input type="radio" id="rb-adm" name="rb-tipo" class="tipo" value="adm"><label for="rb-adm">Administrativo</label>
					</div>
	
					<div id="log"></div>
					
					<div id="directorio" style="padding-top: 10px; float: left;">
					
					</div>
			 	</section>													
			</section>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>