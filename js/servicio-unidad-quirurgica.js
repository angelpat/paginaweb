
function consultaExterna_2(cadena)
{
	$(function()
	{
		$("#desc-croquis").empty();
		$("#desc-croquis").append(cadena);
	});
}

function opcion(pos)
{
	var plantaBaja = new Array();

	plantaBaja[0] = "<b>Acceso lateral</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1681.jpg' alt='Acceso lateral'/>";
	plantaBaja[1] = "<b>Acceso posterior exterior</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1687.jpg' alt='Acceso lateral'/>";
	//plantaBaja[2] = "<b>Acceso posterior exterior</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1687.jpg' alt='Acceso lateral'/>";
	plantaBaja[3] = "<b>Pasillo interno posterior</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1682.jpg' alt='Pasillo interno posterior'/>";
	plantaBaja[4] = "<b>Pasillo interno posterior</b><br> Cirug&iacute;a 3 <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1653.jpg' alt=''/>";
	plantaBaja[5] = "<b>Pasillo interno posterior</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1682.jpg' alt=''/>";
	plantaBaja[6] = "<b>Pasillo interno posterior</b> <br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1656.jpg' alt=''/>";
	
	
	plantaBaja[7] = "<b>Pasillo interno posterior</b><br> Cirug&iacute;a corta estancia 2<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1509.jpg' alt=''/>";
	plantaBaja[8] = "<b>Pasillo interno posterior</b><br> Cirug&iacute;a corta estancia 1<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1510.jpg' alt=''/>";
	plantaBaja[9] = "<b>Pasillo interno posterior</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1476.jpg' alt=''/>";
	
	plantaBaja[10] = "<b>Acceso exterior anterior</b><br>Entrega de limpio<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1581.jpg' alt=''/>";
	plantaBaja[11] = "<b>Acceso exterior anterior</b><br>Esterilizaci&oacute;n con Vapor Autogenerado<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1577.jpg' alt=''/>";
	plantaBaja[12] = "<b>Acceso exterior anterior</b><br>CEyE<br>Recepci&oacute;n de Sucio<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1578.jpg' alt=''/>";
	//plantaBaja[13]="<b>Acceso exterior anterior</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1476.jpg' alt=''/>";
	plantaBaja[14] = "<b>Acceso exterior anterior</b><br>Camilleros<br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1575.jpg' alt=''/>";
	
	plantaBaja[15] = "<b>Preparaci&iacute;n y Recuperaci&oacute;n</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1670.jpg' alt=''/>";
	plantaBaja[16] = "<b>Jefe de Cirug&iacute;a y Estancia Corta</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1661.jpg' alt=''/>";
	plantaBaja[17] = "<b>Salida</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1528.jpg' alt=''/>";
	plantaBaja[18] = "<b>Lobbie Cirugia de corta estancia y endoscopian</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1534.jpg' alt=''/>";
	plantaBaja[19] = "<b>Cirug&iacute;a Endoscopica (Salida del elevador)</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1541.jpg' alt=''/>";
	plantaBaja[20] = "<b>Acceso posterior exterior</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1540.jpg' alt=''/>";
	
	plantaBaja[21] = "<b>Sala 3 de Quir&oacute;fano</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1490.jpg' alt=''/>";
	plantaBaja[22] = "<b>Sala 2 de Quir&oacute;fano</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1469.jpg' alt=''/>";
	plantaBaja[23] = "<b>Sala 1 de Quir&oacute;fano</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1468.jpg' alt=''/>";
	plantaBaja[24] = "<b>Sala 2 de Quir&oacute;fano (Corta estancia)</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1484.jpg' alt=''/>";
	plantaBaja[25] = "<b>Sala 1 de Quir&oacute;fano (corta estancia)</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1480.jpg' alt=''/>";
	
	plantaBaja[26] = "<b>Acceso posterior exterior</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1690.jpg' alt=''/>";
	plantaBaja[27] = "<b>Acceso posterior exterior</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1538.jpg' alt=''/>";

	plantaBaja[28] = "<b>Lobbie Cirugia de corta estancia y endoscopia</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1532.jpg' alt=''/>";
	plantaBaja[29] = "<b>Consultorio</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1543.jpg' alt=''/>";
	plantaBaja[30] = "<b>Jefe de la Unidad Quirurgica y Cirugia de Corta Estancia</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1529.jpg' alt=''/>";
	
	plantaBaja[31] = "<b>Sala de recuperaci&oacute;n</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1514.jpg' alt=''/>";
	plantaBaja[32] = "<b>Sala de recuperaci&oacute;n</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1515.jpg' alt=''/>";
	
	plantaBaja[33] = "<b>Sala de endoscopia 1</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1524.jpg' alt=''/>";
	plantaBaja[34] = "<b>Sala de endoscopia 2</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1524.jpg' alt=''/>";
	plantaBaja[35] = "<b>Endoscopias</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1666.jpg' alt=''/>";
	plantaBaja[36] = "<b>Endoscopias</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1667.jpg' alt=''/>";
	plantaBaja[37] = "<b>Pasillo Endoscopias</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1526.jpg' alt=''/>";
	plantaBaja[38] = "<b>Endoscopias</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1535.jpg' alt=''/>";
	plantaBaja[39] = "<b>Central de enfermeria</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1525.jpg' alt=''/>";
	plantaBaja[40] = "<b>Jefe de Servicio</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1527.jpg' alt=''/>";	
		
	plantaBaja[41] = "<b>CENDI</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1600.jpg' alt=''/>";
	plantaBaja[42] = "<b>Ba&ntilde;o y Vestidores de Mujeres</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1592.jpg' alt=''/>";
	plantaBaja[43] = "<b>Ba&ntilde;o y Vestidores de Hombres</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1589.jpg' alt=''/>";
	plantaBaja[44] = "<b>NECESARIO ENTRAR CON TRAJE QUIR&Uacute;RGICO, GORRO, BOTAS, CUBREBOCAS</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1594.jpg' alt=''/>";
	plantaBaja[45] = "<b>CEyE y Quirofano</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1676.jpg' alt=''/>";
	plantaBaja[46] = "<b>Entrega de Limpios CEyE</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1675.jpg' alt=''/>";
	
	plantaBaja[47] = "<b>CENDI</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1569.jpg' alt=''/>";
	plantaBaja[48] = "<b>CENDI</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1553.jpg' alt=''/>";
	plantaBaja[49] = "<b>CENDI</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1558.jpg' alt=''/>";
	plantaBaja[50] = "<b>CENDI</b><br><br><img src='../../img/servicios/unidad.quirurgica/croquis/1564.jpg' alt=''/>";
	
	consultaExterna_2(plantaBaja[pos]);		
	
}


function datosPersonales(nombre,escuela,especialidad,cedula,tiempo,logro,ruta)
{
	$(function()
	{
		
		
		
	/*	$( "#modal-cv").load('../../ajax_servicio_unidad_quirurgica.php?type=8&nombre='+nombre+'&escuela='+escuela
		+'&especialidad='+especialidad+'&cedula='+cedula+'&tiempo='+tiempo+'&logro='+logro+'&ruta='+ruta);
		$( "#modal-cv" ).dialog( "open" );
		
	 */
		
	});
}




$(function(){
	
	var	nombre = "Dr. Miguel Angel<br> Azua Elias",
		escuela = "Universidad Aut&oacute;noma de Tamaulipas",
		especialidad = "Especialidad en Anestesiolog&iacute;a",
		cedula = "No. AESSA-31600",
		tiempo = 1993,
		logro = "Consejo Mexicano de Anestesiolog&iacute;a, AC. Certificaci&oacute;n 1993, Primera Re-Certificaci&oacute;n 1998 y  Segunda Re-Certificaci&oacute;n 2006.<br><br>" +
				"Profesor Adjunto del Postgrado en la Especialidad de Anestesiolog&iacute;a Hospital General de Tampico 'Dr.  Carlos Canseco'  1997-2001.<br><br>" +
				"Jefe del Servicio de Anestesiolog&iacute;a del Hospital General de Tampico 'Dr. Carlos Canseco'   2002 - 2004.<br><br>" +
				"Medico Anestesi&oacute;logo Adscrito al Servicio de Anestesiolog&iacute;a Turno Matutino Benem&eacute;rito Hospital con Especialidades 'Juan Mar&iacute;a de Salvatierra'  2008 - 2011<br><br>" +
				"Jefe del Servicio de Quir&oacute;fanos Generales del BHCE 'Juan Mar&iacute;a de Salvatierra' Desde 1ro. de julio 2011 a la fecha."
,

		ruta = "../../img/personal/foto/dr-miguel-angel-azua-elias.png";
	
	$( ".cv" ).click(function(){		
		
		$( "#modal-cv").load(
			'../../ajax_servicio_unidad_quirurgica.php',
			{'type' : '8', 
			 'nombre': nombre,
			 'escuela': escuela,
			 'especialidad': especialidad,
			 'cedula': cedula,
			 'tiempo': tiempo,
			 'ruta': ruta,
			 'logro': logro}
		);
		
		$( "#modal-cv" ).dialog( "open" );		
		
	});
	
	$( "#modal-cv" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 620,
		width: 650,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		}
	});			
			
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});

	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});			
	
	$(document).ready(function(){
	/*	$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		var posAleatoria=Math.floor(Math.random()* limite);	
		opcion(posAleatoria);*/
		tooltip();	
		$('#features').jshowoff();
		opcion(0);		
			
	});		
		
	
	$(".btn").button();	
	
	$( "#btn-croquis" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-croquis" ).load('../../ajax_servicio_unidad_quirurgica.php?type=1');
		$( "#modal-croquis" ).dialog( "open" );		
	});		

	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-organigrama" ).load('../../ajax_servicio_unidad_quirurgica.php?type=2');
		$( "#modal-organigrama" ).dialog( "open" );
	});		

	$( "#modal-croquis" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 600,
		width: 900,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		

	$( "#modal-organigrama" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 530,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
		
	$( ".bullet" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-radio-on" );
		$(this).find("span").addClass( "ui-icon-bullet" );		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-bullet" );
		$(this).find("span").addClass( "ui-icon-radio-on" );
	});	
	
	$( "#modal-img" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 540,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});	
	
	$( "#pro-img-01" ).click(function() {
		$( "#modal-img" ).load('../../ajax_servicio_unidad_quirurgica.php?type=3');
		$( "#modal-img" ).dialog( "open" );
	});
	
	$( "#pro-img-02" ).click(function() {
		$( "#modal-img" ).load('../../ajax_servicio_unidad_quirurgica.php?type=4');
		$( "#modal-img" ).dialog( "open" );
	});
	
	$( "#pro-img-03" ).click(function() {
		$( "#modal-img" ).load('../../ajax_servicio_unidad_quirurgica.php?type=5');
		$( "#modal-img" ).dialog( "open" );
	});	
	
	$( "#pro-img-04" ).click(function() {
		$( "#modal-img" ).load('../../ajax_servicio_unidad_quirurgica.php?type=6');
		$( "#modal-img" ).dialog( "open" );
	});	
	
	$( "#pro-img-05" ).click(function() {
		$( "#modal-img" ).load('../../ajax_servicio_unidad_quirurgica.php?type=7');
		$( "#modal-img" ).dialog( "open" );
	});		
	
});
