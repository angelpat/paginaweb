$(function(){
	

	// Fechas	
	$(".date").datepicker();          
		
	$(".date" ).datepicker( "option", {dateFormat: "dd/mm/yy"});
		
	var monthNamesShort = $( ".date" ).datepicker( "option", "monthNamesShort" );
	$( ".date" ).datepicker( "option", "monthNamesShort", ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'] );

	var monthNames = $( ".date-picker" ).datepicker( "option", "monthNames" );
	$( ".date" ).datepicker( "option", "monthNames", ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] );
	
	var dayNames = $( ".date-picker" ).datepicker( "option", "dayNames" );
	$( "date" ).datepicker( "option", "dayNames", ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'] );
	
	var dayNamesMin = $( ".fecha, .date-picker" ).datepicker( "option", "dayNamesMin" );
	$( ".date" ).datepicker( "option", "dayNamesMin", ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'] );            	

	var nextText = $( ".fecha, .date-picker" ).datepicker( "option", "nextText" );
	$( ".date" ).datepicker( "option", "nextText", 'Siguiente' );

	var prevText = $( ".fecha, .date-picker" ).datepicker( "option", "prevText" );
	$( ".date" ).datepicker( "option", "prevText", 'Anterior' );     		

	// Validaciones
		
	function updateTips( t ) {
		tips
			.text( t )
			//.addClass( "ui-state-highlight" );
			.addClass( "ui-state-error" );
	//	setTimeout(function() {
	//		tips.removeClass( "ui-state-highlight", 1500 );
	//	}, 500 );		
	}

	function updateMessage( t ) {
		tips
			.text( t )
			.addClass( "ui-state-highlight" );		
	}

	function isRequired( o, n ){
		if ( ! o.val() ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}
	
	function checkRegexp( o, regexp, n ) {		
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}	
	
	function checkLength( o, n, min, max ) {	
	
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			updateTips( "La longitud de " + n + " debe estar entre " +
				min + " y " + max + " caracteres." );
			return false;
		} else {
			return true;
		}
	}		
		
	function validaFecha(o){  
		//El Formato es dd-mm-aaaa  		
		
		var fecha = o.val();
			    
	    // Cadena A�o  
	    var Ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
	    // Cadena Mes  
	    var Mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
	    // Cadena D�a  
	    var Dia= new String(fecha.substring(0,fecha.indexOf("/")))  
	  
	    // Valido el a�o  
	    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El anio esta incorrecto" ); 
	        return false  
	    }  
	    // Valido el Mes  
	    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El mes esta incorrecto" ); 
	        return false  
	    }  
	    // Valido el Dia  
	    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El dia esta incorrecto" );
	        return false  
	    }  
	    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
	        if (Mes==2 && Dia > 28 || Dia>30) {  
		    	o.addClass( "ui-state-error" );
				updateTips( "El dia esta incorrecto" );
	            return false  
	        }  
	    }  
	  	return true    
	}  
	  		
	function concatenaFecha(fecha, hora, meridiano){
		
		var horaCompleta, fechaCompleta;
		
		if(meridiano == "AM"){
			horaCompleta = hora.toString() + ':00:00.000';			
		}		
		else{
			horaCompleta = parseInt(hora) + parseInt('12');
			horaCompleta = horaCompleta.toString() + ':00:00.000';
		}
				
		fechaCompleta = fecha + ' ' + horaCompleta; 
		
		return fechaCompleta;
	}
	

	 $(".cleditor").cleditor({
	        width:        735, // width not including margins, borders or padding
	        height:       350, // height not including margins, borders or padding
	        controls:     // controls to add to the toolbar
	                      "bold italic underline strikethrough subscript superscript | font size " +
	                      "style | removeformat | bullets numbering  " +
	                      " | alignleft center alignright justify | undo redo | " +
	                      "rule link unlink | cut copy paste pastetext | print source",
	        colors:       // colors in the color popup
	                      "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
	                      "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
	                      "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
	                      "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
	                      "666 900 C60 C93 990 090 399 33F 60C 939 " +
	                      "333 600 930 963 660 060 366 009 339 636 " +
	                      "000 300 630 633 330 030 033 006 309 303",    
	        fonts:        // font names in the font popup
	                      "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
	                      "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
	        sizes:        // sizes in the font size popup
	                      "1,2,3,4,5,6,7",
	        styles:       // styles in the style popup
	                      [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
	                      ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
	                      ["Header 6","<h6>"]],
	        useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
	        docType:      // Document type contained within the editor
	                      '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
	        docCSSFile:   // CSS file used to style the document contained within the editor
	                      "", 
	        bodyStyle:    // style to assign to document body contained within the editor
	                      "margin:4px; font:10pt Arial,Verdana; cursor:text"
		});
	
	
});
