$(function(){	

	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});	
	
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		$('#features').jshowoff();
		tooltip();
	});			
		
	$(".btn").button();	

	$( "#modal-laboratorio" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 570,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});			
	
	$( "#modal-ingreso" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 570,
		width: 550,
		modal: true,
		buttons: {
			"Paso 1": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=2", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 2": function() {				
				$(this).load( "../../ajax_servicio_laboratorio.php?type=3", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 3": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=4", "", function(){
	        		$(this).slideDown( "fast" );
				});								
			},
			"Paso 4": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=5", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 5": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=6", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 6": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=7", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});	
	
	
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-laboratorio" ).load('../../ajax_servicio_laboratorio.php?type=1');
		$( "#modal-laboratorio" ).dialog( "open" );
	});	
				
	$( ".paso-1" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=2');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	
	$( ".paso-2" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=3');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	
	$( ".paso-3" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=4');
		$( "#modal-ingreso" ).dialog( "open" );
	});	

	$( ".paso-4" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=5');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	
	$( ".paso-5" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=6');
		$( "#modal-ingreso" ).dialog( "open" );
	});	

	$( ".paso-6" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_laboratorio.php?type=7');
		$( "#modal-ingreso" ).dialog( "open" );
	});		
	
	$( ".paso-desc" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-pin-w" );
		$(this).find("span").addClass( "ui-icon-pin-s" );
		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-pin-s" );
		$(this).find("span").addClass( "ui-icon-pin-w" );
	});
	
	$( ".bullet" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-radio-on" );
		$(this).find("span").addClass( "ui-icon-bullet" );		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-bullet" );
		$(this).find("span").addClass( "ui-icon-radio-on" );
	});	
	
	
	
	$( "#modal-donacion" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 600,
		width: 590,
		modal: true,
		buttons: {
			"Paso 1": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=8", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 2": function() {				
				$(this).load( "../../ajax_servicio_laboratorio.php?type=9", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 3": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=10", "", function(){
	        		$(this).slideDown( "fast" );
				});								
			},
			"Paso 4": function() {
	        	$(this).load( "../../ajax_servicio_laboratorio.php?type=11", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( ".donacion-1" ).click(function() {
		$( "#modal-donacion" ).load('../../ajax_servicio_laboratorio.php?type=8');
		$( "#modal-donacion" ).dialog( "open" );
	});		
	
	$( ".donacion-2" ).click(function() {
		$( "#modal-donacion" ).load('../../ajax_servicio_laboratorio.php?type=8');
		$( "#modal-donacion" ).dialog( "open" );
	});		
		
	
});
