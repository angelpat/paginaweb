function consultaExterna_1(cadena)
{
	$(function()
	{
		$("#textoExterno-1").empty();
		$("#textoExterno-1").append(cadena);
	});
}

function consultaExterna_2(cadena)
{
	$(function()
	{
		$("#textoExterno-2").empty();
		$("#textoExterno-2").append(cadena);
	});
}

function opcion(caso,pos)
{
	switch(caso)
	{
		case 1://Consultorios de Consulta Interna 2
		{
			var plantaBaja=new Array();
			
			plantaBaja[0]="<b>Consultorio n&uacute;mero 1</b> <br><br> Trabajo social  							<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-01-TRABAJO-SOCIAL.jpg' alt='consultorio_1'/> 		<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM - 14:00 PM";
			plantaBaja[1]="<b>Consultorio n&uacute;mero 2</b> <br><br> Somatometr&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-02-SOMATOMETRIA.jpg' alt='consultorio_2'/> 		<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM - 20:00 PM";
			plantaBaja[2]="<b>Consultorio n&uacute;mero 3</b> <br><br> Medicina preventiva y Jur&iacute;dico	<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-03-MEDICINA-PREVENTIVA.jpg' alt='consultorio_3'/> 	<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM";
			plantaBaja[3]="<b>Consultorio n&uacute;mero 4</b> <br><br> Pediatr&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-04-PEDIATRIA.jpg' alt='consultorio_4'/> 			<br><br><b>Horarios:</b><br>	Lunes, Miercoles y Viernes de 10:00 AM";
			plantaBaja[4]="<b>Consultorio n&uacute;mero 5</b> <br><br> Oftalmolog&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 011.jpg' alt='consultorio_5'/> 			<br><br><b>Horarios:</b><br>	Lunes, Miercoles y Viernes de 10:00 AM";
			plantaBaja[5]="<b>Consultorio n&uacute;mero 6</b> <br><br> Psicolog&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-06-PSICOLOGIA.jpg' alt='consultorio_6'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 9:00 AM";
			plantaBaja[6]="<b>Consultorio n&uacute;mero 7</b> <br><br> Neurocirug&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 003.jpg' alt='consultorio_7'/> 			<br><br><b>Horarios:</b><br>	Lunes, Martes, Jueves y Viernes de 8:00 AM";
			plantaBaja[7]="<b>Consultorio n&uacute;mero 8</b> <br><br> Tamiz neonatal			 				<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 007.jpg' alt='consultorio_8'/>";
			plantaBaja[8]="<b>Consultorio n&uacute;mero 9</b> <br><br> Otorrinolaringolog&iacute;a 				<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-09-OTORRINO.jpg' alt='consultorio_9'/> 			<br><br><b>Horarios:</b><br>	Lunes, Martes y Jueves de 10:00 AM";
			plantaBaja[9]="<b>Consultorio n&uacute;mero 10</b> <br><br> Cardiolog&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-10-CARDIOLOGIA.jpg' alt='consultorio_10'/> 		<br><br><b>Horarios:</b><br>	Martes y Jueves de 10:00 AM";
			plantaBaja[10]="<b>Consultorio n&uacute;mero 11</b> <br><br> Angiolog&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 011.jpg' alt='consultorio_11'/> 			<br><br><b>Horarios:</b><br>	Lunes y Viernes de 9:00 AM";
			plantaBaja[11]="<b>Consultorio n&uacute;mero 12</b> <br><br> Electrocardiograma 					<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 001.jpg' alt='consultorio_12'/> 			<br><br><b>Horarios:</b><br>	Miercoles de 10:00 AM";
			plantaBaja[12]="<b>Consultorio n&uacute;mero 13</b> <br><br> Neumolog&iacute;a / Gen&eacute;tica 	<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 007.jpg' alt='consultorio_13'/> 			<br><br><b>Horarios:</b><br>	Martes de 8:00 AM y Miercoles de 10:00 AM";
			plantaBaja[13]="<b>Consultorio n&uacute;mero 14</b> <br><br> Labio y paladar hendido 				<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 003.jpg' alt='consultorio_14'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM a 14:30 PM";
			plantaBaja[14]="<b>Consultorio n&uacute;mero 15</b> <br><br> Traumatolog&iacute;a 					<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-15-TRAUMATOLOGIA.jpg' alt='consultorio_15'/> 		<br><br><b>Horarios:</b><br>	Martes y Jueves de 8:00 AM <br>Lunes y Miercoles de 11:00 AM";
			plantaBaja[15]="<b>Consultorio n&uacute;mero 16</b> <br><br> Cirug&iacute;aqa reconstructiva 		<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 007.jpg' alt='consultorio_16'/> 			<br><br><b>Horarios:</b><br>	Martes y Jueves de 8:00 AM";
			plantaBaja[16]="<b>Consultorio n&uacute;mero 17</b> <br><br> Cirug&iacute;a general 				<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-17-CIRUGIA-GENERAL.jpg' alt='consultorio_17'/> 	<br><br><b>Horarios:</b><br>	Lunes y Martes de 10:00 AM";
			plantaBaja[17]="<b>Consultorio n&uacute;mero 18</b> <br><br> Cirug&iacute;a general 				<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-18-CIRUGIA-GENERAL.jpg' alt='consultorio_18'/> 	<br><br><b>Horarios:</b><br>	Lunes y Jueves de 10:00 AM";
			plantaBaja[18]="<b>Consultorio n&uacute;mero 19</b> <br><br> Oncolog&iacute;a m&eacute;dica quir&uacute;rgica / Estimulaci&oacute;n temprana <br><br> No diponible <br><br><img src='../../img/servicios/consulta.externa/consultorios/consultorios 003.jpg' alt='consultorio_19'/>";
			
			consultaExterna_1(plantaBaja[pos]);
		}
		break;
		
		case 2://Consultorios de Consulta Interna 1
		{
			var plantaBaja=new Array();
			
			plantaBaja[0]="<b>Consultorio n&uacute;mero 20</b> <br><br> Jefatura de enfermer&iacute;a			<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 001.jpg' alt='consultorio_20'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM - 14:00 PM";
			plantaBaja[1]="<b>Consultorio n&uacute;mero 21</b> <br><br> Somatometr&iacute;a 					<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-21-SOMATOMETRIA.jpg' alt='consultorio_21'/> 		<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM - 20:00 PM";
			plantaBaja[2]="<b>Consultorio n&uacute;mero 22</b> <br><br> Ginecolog&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 004.jpg' alt='consultorio_22'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 13:00 PM";
			plantaBaja[3]="<b>Consultorio n&uacute;mero 23</b> <br><br> Ginecolog&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 007.jpg' alt='consultorio_29'/>";
			plantaBaja[4]="<b>Consultorio n&uacute;mero 24</b> <br><br> Ginecolog&iacute;a 						<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-24-GINECOLOGIA.jpg' alt='consultorio_24'/> 		<br><br><b>Horarios:</b><br>	Lunes, Martes, Miercoles, Jueves y Viernes de 8:00 AM <br> Lunes, Miercoles y Viernes de 11:00 AM" ;
			plantaBaja[5]="<b>Consultorio n&uacute;mero 25</b> <br><br> Odontopediatr&iacute;a 					<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-25-ODONTOLOGIA.jpg' alt='consultorio_25'/> 		<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM ";
			plantaBaja[6]="<b>Consultorio n&uacute;mero 26</b> <br><br> Planificaci&oacute;n familiar 			<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-26-PLANIFICACION-FAM.jpg' alt='consultorio_26'/> 	<br><br><b>Horarios:</b><br>	Lunes, Martes, Jueves y Viernes de 8:00 AM <br> Miercoles de 9:00 AM";
			plantaBaja[7]="<b>Consultorio n&uacute;mero 27</b> <br><br> Vacuna 									<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-27-VACUNAS.jpg' alt='consultorio_27'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM a 13:30 PM";
			plantaBaja[8]="<b>Consultorio n&uacute;mero 28</b> <br><br> Medicina preventiva 					<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-28-SALUD-REPRODUCTIVA.jpg' alt='consultorio_28'/> 	<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM";
			plantaBaja[9]="<b>Consultorio n&uacute;mero 29</b> <br><br> Tamiz neonatal							<br><br><br>	<img src='../../img/servicios/consulta.externa/consultorios/CONS-29-TAMIZ-NEONATAL.jpg' alt='consultorio_29'/> 		<br><br><b>Horarios:</b><br>	Lunes a Viernes de 8:00 AM a 13:30 PM";
			plantaBaja[10]="<b>Consultorio n&uacute;mero 30</b> <br><br> Dermatolog&iacute;a 					<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-30-DERMATOLOGIA .jpg' alt='consultorio_30'/> 		<br><br><b>Horarios:</b><br>	Lunes, Martes, Miercoles y Viernes de 8:00 AM";
			plantaBaja[11]="<b>Consultorio n&uacute;mero 31</b> <br><br> Endocrinolog&iacute;a 					<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-31-NUTRICION.jpg' alt='consultorio_31'/> 			<br><br><b>Horarios:</b><br>	Lunes, Martes y Jueves de 9:00 AM <br> Lunes y Jueves de 11:00 AM";
			plantaBaja[12]="<b>Consultorio n&uacute;mero 32</b> <br><br> Medicina interna 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 003.jpg' alt='consultorio_32'/> 			<br><br><b>Horarios:</b><br>	Martes a Viernes de 12:00 PM";
			plantaBaja[13]="<b>Consultorio n&uacute;mero 33</b> <br><br> Medicina interna 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-33-MEDICINA-INTERNA.jpg' alt='consultorio_33'/> 	<br><br><b>Horarios:</b><br>	Lunes 10:00 AM o Martes y Miercoles de 10:00 AM <br>Jueves y Viernes 10:00 AM";
			plantaBaja[14]="<b>Consultorio n&uacute;mero 34</b> <br><br> Medicina interna 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/consultorios 007.jpg' alt='consultorio_34'/> 			<br><br><b>Horarios:</b><br>	Martes a Jueves de 11:00 PM";
			plantaBaja[15]="<b>Consultorio n&uacute;mero 35</b> <br><br> Nefrolog&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-35-NEFROLOGIA-MED-INTEGRADA.jpg' alt='consultorio_35'/> 			<br><br><b>Horarios:</b><br>	Lunes a Viernes de 10:00 AM";
			plantaBaja[16]="<b>Consultorio n&uacute;mero 36</b> <br><br> Dermatolog&iacute;a 					<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-36-DERMATOLOGIA.jpg' alt='consultorio_36'/> 			<br><br><b>Horarios:</b><br>	Lunes, Martes, Miercoles y Viernes de 8:00 AM <br> Lunes a Jueves de 10:00 AM";
			plantaBaja[17]="<b>Consultorio n&uacute;mero 37</b> <br><br> Urolog&iacute;a 						<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/CONS-37-UROLOGIA.jpg' alt='consultorio_37'/> 			<br><br><b>Horarios:</b><br>	Lunes y Jueves de 10:00 PM";
			plantaBaja[18]="<b>Jefatura de Consulta Externa</b> <br><br> Dr. Jos&eacute; Grajales Montiel				<br><br><br> 	<img src='../../img/servicios/consulta.externa/consultorios/JEFATURA-CONS-EXT.jpg' alt='jefatura_ce'/>";
			
			consultaExterna_2(plantaBaja[pos]);
		}
		break;
	}
}

$(function(){
		
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$( "#menu-contact" ).click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});			
	
	$(document).ready(function(){
		$( '#contador' ).load('../../ajax_contador.php', 'url=' + location.href, '');
		$('#features').jshowoff();
		for (var indice=1; indice < 3; ++indice)
		{
			if(indice == 1)
			{
				var limite=18
			}
			else
			{
				var limite=17
			}
				
			var posAleatoria=Math.floor(Math.random()* limite);	
			opcion(indice,posAleatoria);
		}		
		
	});	
	
	$(".btn").button();	

	$( "#modal-organigrama" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 570,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-organigrama" ).load('../../ajax_servicio_consulta_externa.php?type=1');
		$( "#modal-organigrama" ).dialog( "open" );
	});	
		
	
	$( ".bullet" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-radio-on" );
		$(this).find("span").addClass( "ui-icon-bullet" );		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-bullet" );
		$(this).find("span").addClass( "ui-icon-radio-on" );
	});	
	
	
});
