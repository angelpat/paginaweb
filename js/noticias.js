$(function(){

	var urlParams = {};
	(function () {
	    var e,
	        a = /\+/g,  // Regex for replacing addition symbol with a space
	        r = /([^&=]+)=?([^&]*)/g,
	        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
	        q = window.location.search.substring(1);

	    while (e = r.exec(q))
	       urlParams[d(e[1])] = d(e[2]);
	})();
		
	$(document).ready(function() {
		data = 'type=2&idNoticia=' + urlParams["idNoticia"];
		$('#content').load('ajax_noticia.php?', data, '');				
	});
		
});
