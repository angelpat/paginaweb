$(function(){
		
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
	
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		$('#features').jshowoff();
		tooltip();
	});				
	
	$( "#modal-ingreso" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 570,
		width: 550,
		modal: true,
		buttons: {
			"Paso 1": function() {
	        	$(this).load( "../../ajax_servicio_imagenologia.php?type=1", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 2": function() {				
				$(this).load( "../../ajax_servicio_imagenologia.php?type=2", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Paso 3": function() {
	        	$(this).load( "../../ajax_servicio_imagenologia.php?type=3", "", function(){
	        		$(this).slideDown( "fast" );
				});								
			},
			"Paso 4": function() {
	        	$(this).load( "../../ajax_servicio_imagenologia.php?type=4", "", function(){
	        		$(this).slideDown( "fast" );
				});	
			},
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	
	$( ".paso-1-ingreso" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_imagenologia.php?type=1');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	
	$( ".paso-2-ingreso" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_imagenologia.php?type=2');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	
	$( ".paso-3-ingreso" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_imagenologia.php?type=3');
		$( "#modal-ingreso" ).dialog( "open" );
	});	

	$( ".paso-4-ingreso" ).click(function() {
		$( "#modal-ingreso" ).load('../../ajax_servicio_imagenologia.php?type=4');
		$( "#modal-ingreso" ).dialog( "open" );
	});	
	

			
	$( "#accordion" ).accordion();
	
	
	$( ".paso-desc" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-pin-w" );
		$(this).find("span").addClass( "ui-icon-pin-s" );
		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-pin-s" );
		$(this).find("span").addClass( "ui-icon-pin-w" );
	});
	
	
	
});
