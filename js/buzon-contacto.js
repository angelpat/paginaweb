$(function()
{
	
	var nombre = $( "#nombre" ),
		apellido = $( "#apellido" ),
		domicilio = $( "#domicilio" ),
		telefono = $( "#telefono" ),
		correo = $( "#correo-e" ),
		narrativa = $( "#narrativa" ),
		allField = $([]).add(nombre).add(apellido).add(domicilio).add(telefono).add(correo).add(narrativa);
		tips = $( "#tip" );
	
	function checarLongitud( o, n, min, max ) {			
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			actualizarTips( "La longitud de " + n + " debe de ser entre " + min + " y " + max );
			return false;
		} 
		else{
			return true;
		}
	}
	
	function checarExpRegular( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			actualizarTips( n );
			return false;
		} else {
			return true;
		}
	}
	
	function campoRequerido(o,n,isRequerido){
		if(o.val() == "" && isRequerido){
			o.addClass( "ui-state-error" );
			actualizarTips( "El campo " + n + " es requerido" );					
			return false;
		}
		else{
			return true;
		}
	}
	
	function actualizarTips( t ) {
		tips
		.text( t )
		.addClass( "ui-state-error" );
	}
	
	$( "#btn-enviar" )
	.button({
		icons: {
			primary: "ui-icon-comment"
		}
	})
	.click(function() {
		allField.removeClass( "ui-state-error" );
		tips.removeClass( "ui-state-error" );
		
		var esVerdadero = true;
		
		esVerdadero = esVerdadero && checarLongitud( nombre, "nombre", 3, 50 );
		esVerdadero = esVerdadero && checarLongitud( apellido, "apellido", 3, 50 );
		esVerdadero = esVerdadero && checarLongitud( domicilio, "domicilio", 5, 100 );
		esVerdadero = esVerdadero && checarLongitud( correo, "correo-e", 7, 75 );
		
		esVerdadero = esVerdadero && campoRequerido( nombre, "nombre", true);	
		esVerdadero = esVerdadero && campoRequerido( apellido, "apellido", true);
		esVerdadero = esVerdadero && campoRequerido( domicilio, "domicilio", true);
		esVerdadero = esVerdadero && campoRequerido( narrativa, "comentario", true);
		
		if(telefono.val() != ""){
			esVerdadero = esVerdadero && checarLongitud( telefono, "telefono", 5, 25 );	
			esVerdadero = esVerdadero && checarExpRegular( telefono, /^[a-zA-Z 0-9]+$/, "El numero de telefono solo acepta numeros y espacios en blanco" );
		}
		
		esVerdadero = esVerdadero && campoRequerido( correo,"correo-e",true);				
		
		esVerdadero = esVerdadero && checarExpRegular( nombre, /^[a-zA-Z �����A������,:;-]+$/i, "El campo nombre solo permite letras" );
		esVerdadero = esVerdadero && checarExpRegular( apellido, /^[a-zA-Z �����A������,:;-]+$/i, "El campo apellido solo permite letras" );
		esVerdadero = esVerdadero && checarExpRegular( domicilio, /^[a-zA-Z 0-9 �����A������,'/']+$/i, "Debe especificar el domicilio" );
		esVerdadero = esVerdadero && checarExpRegular( correo, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Ejemplo: nombre@correo.com" );
		
		if ( esVerdadero ) {
			$.ajax({
				type: "POST",
				url: "ajax_contacto.php",
				data: { nombre: $( "#nombre" ).val(), 
						apellido: $( "#apellido" ).val(),
					  	domicilio: $( "#domicilio" ).val(),
					  	telefono: $( "#telefono" ).val(),
					  	correo: $( "#correo-e" ).val(),
					  	narrativa: $( "#narrativa" ).val()
					  }
			}).done(function( msg ) {
				$( "#modal-mensaje" ).dialog( "open" );
				actualizarTips( "" );
				tips.removeClass( "ui-state-error" );	
				allField.val( "" ).removeClass( "ui-state-error" );			
				$( ".cleditor" ).cleditor({width:735, height:350, updateTextArea:function (){}})[0].clear().execCommand("inserthtml", data, null, null);
			});	
		}				
	});
	
	function limpiaFormulario(){
		$( "#nombre" ).val("");
		$( "#apellido" ).val("");
		$( "#domicilio" ).val("");
		$( "#telefono" ).val("");
		$( "#correo-e" ).val("");
		$( ".cleditor" ).cleditor({width:735, height:350, updateTextArea:function (){}})[0].clear().execCommand("inserthtml", data, null, null);
	}
	
	$( "#modal-mensaje" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});

	$( "#menu-contact" ).click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
	
	$( ".cleditor" ).cleditor({
		width:        735, // width not including margins, borders or padding
		height:       350, // height not including margins, borders or padding
		controls:     // controls to add to the toolbar
		              "bold italic underline strikethrough subscript superscript | font size " +
		              "style | removeformat | bullets numbering  " +
		              " | alignleft center alignright justify | undo redo | " +
		              "rule link unlink | cut copy paste pastetext | print source",
		colors:       // colors in the color popup
		              "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
		              "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
		              "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
		              "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
		              "666 900 C60 C93 990 090 399 33F 60C 939 " +
		              "333 600 930 963 660 060 366 009 339 636 " +
		              "000 300 630 633 330 030 033 006 309 303",    
		fonts:        // font names in the font popup
		              "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
		              "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
		sizes:        // sizes in the font size popup
		              "1,2,3,4,5,6,7",
		styles:       // styles in the style popup
		              [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
		              ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
		              ["Header 6","<h6>"]],
		useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
		docType:      // Document type contained within the editor
		              '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
		docCSSFile:   // CSS file used to style the document contained within the editor
		              "", 
		bodyStyle:    // style to assign to document body contained within the editor
		              "margin:4px; font:10pt Arial,Verdana; cursor:text"
	 });
		 
});