$(function(){
	
///////////////////////////////////////////Index.php//////////////////////////////////////////////////////
	//var tipo = "/hgejms/"; 
	var tipo = "/";
		
	$( ".menu-home" ).click(function() {		
		var url = "http://" + location.host + tipo + "index.php";	
		location.href= url;		
	});
		
	$( ".menu-about-us" ).click(function() {		
		var url = "http://" + location.host + tipo + "quienes.somos.php";		
		location.href=url;					
	});

	$( ".menu-services" ).click(function() {
		var url = "http://" + location.host + tipo + "servicios/servicios.php";		
		location.href=url;				
	});

	$( ".menu-organization" ).click(function() {			
		var url = "http://" + location.host + tipo + "organigrama.php";		
		location.href=url;				
	});
	$( ".menu-mailbox").click(function(){
		var url="http://" + location.host + tipo + "buzon.usuario.php";
		location.href=url;
	});
	
	$( ".menu-location" ).click(function() {		
		var url = "http://" + location.host + tipo + "localizacion.php";		
		location.href=url;				
	});
	
	$( ".menu-commutator" ).click(function() {		
		var url = "http://" + location.host + tipo + "conmutador.php";		
		location.href=url;				
	});	
	
	/*
	$("#menu-contact").click(function(){
		var url="http://" + location.host + tipo + "contacto.php";
		location.href=url;
	});
	*/
	
	//////////////////////////////////////Servicios/Servicios.php//////////////////////////////////
		
	$( ".menu-archivo" ).click(function() {	
		var url = "http://" + location.host + tipo + "servicios/archivo.clinico/archivo.clinico.php";
		location.href=url;				
	});
	
	$( ".menu-laboratorio" ).click(function() {	
		var url = "http://" + location.host + tipo + "servicios/laboratorio/laboratorio.php";
		location.href=url;				
	});
	
	$( ".menu-consulta" ).click(function() {	
		var url = "http://" + location.host + tipo + "servicios/consulta.externa/consulta.externa.php";
		location.href=url;				
	});	
	
	$( ".menu-consulta-galeria" ).click(function() {	
		var url = "http://" + location.host + tipo + "servicios/consulta.externa/galeria.php";
		location.href=url;				
	});	
			
	$( ".menu-imagenologia" ).click(function() {		
		var url = "http://" + location.host + tipo + "servicios/imagenologia/imagenologia.php";
		location.href=url;				
	});
	
	$( ".menu-cirugia" ).click(function() {		
		var url = "http://" + location.host + tipo + "servicios/cirugia/cirugia.php";
		location.href=url;				
	});
	
	$( ".menu-cirugia-proceso" ).click(function() {		
		var url = "http://" + location.host + tipo + "servicios/cirugia/proceso.quirurgico.php";
		location.href=url;				
	});
				
	$( ".menu-urgencias" ).click(function() {			
		var url = "http://" + location.host + tipo + "servicios/urgencias/urgencias.php";
		location.href=url;				
	});
	
	$( ".menu-unidad-quirurgica" ).click(function() {			
		var url = "http://" + location.host + tipo + "servicios/unidad.quirurgica/unidad.quirurgica.php";
		location.href=url;				
	});	
	
	$( ".menu-unidad-quirurgica-galeria" ).click(function() {			
		var url = "http://" + location.host + tipo + "servicios/unidad.quirurgica/galeria.php";
		location.href=url;				
	});	
	
	
	$( ".menu-ucin-galeria" ).click(function() {			
		var url = "http://" + location.host + tipo + "servicios/pediatria/galeria.ucin.php";
		location.href=url;				
	});		
	
	
	/////////////////////////////////Departamento/ensenanza.php///////////////////////////////////////
	
	$( ".menu-calidad" ).click(function() {	
		var url = "http://" + location.host + tipo + "departamentos/calidad/calidad.php";
		location.href=url;				
	});			
	
	$(".menu-nutricion").click(function(){
		var url= "http://" + location.host + tipo + "departamentos/nutricion.cocina/nutricion.cocina.php";
		location.href=url;
	});
	
	$(".menu-ensenanza").click(function(){
		var url= "http://" + location.host + tipo + "departamentos/ensenanza/ensenanza.php";
		location.href=url;
	});	
	
	$(".menu-informatica").click(function(){
		var url= "http://" + location.host + tipo + "departamentos/informatica/informatica.php";
		location.href=url;
	});
	
	$(".menu-ts").click(function(){
		var url= "http://" + location.host + tipo + "departamentos/trabajo.social/trabajo.social.php";
		location.href=url;
	});	

});
