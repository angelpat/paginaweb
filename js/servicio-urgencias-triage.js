$(function(){	
	
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
	
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
	});	
	
	$(".btn").button();	

	
	$( "#accordion" ).accordion({
		fillSpace: true
	});
	
	$( "#accordionResizer" ).resizable({
		minHeight: 140,
		resize: function() {
			$( "#accordion" ).accordion( "resize" );
		}
	});
	
	$( "#dialog:ui-dialog" ).dialog( "destroy" );

	var nivel = 0;
	
	var nombre = $( "#nombre" ),
	edad = $( "#edad" ),
	sexo = $( "#sexo" ),
	presion = $("#presion"),
	presion2 = $("#presion2"),
	temperatura = $("#temp"),
	estadoAlerta = $("#estado"),
	respiracion = $("#resp"),
	pulso = $("#pulso"),
	todosLosCampos = $( [] ).add( nombre ).add( edad ).add( sexo ).add(presion).add(presion2).add(temperatura).add(estadoAlerta).add(respiracion).add(pulso),
	tips = $( ".validateTips" );

	function Presion()
	{
		if (presion.val() >= 90 && presion.val() <= 140) 
		{
			return 1;
		}
		else
		{
			if(presion.val() > 140 && presion.val() <= 180)
			{
				return 2;
			}
			else
			{
				if(presion.val() < 90 || presion.val() > 180)
				{
					return 3;
				}
			}
		}
	}

	function Temperatura()
	{
		if(temperatura.val() >= 35.5 && temperatura.val() < 37.5)
		{
			return 1;
		}
		else
		{
			if(temperatura.val() >= 37.5 && temperatura.val() < 39)
			{
				return 2;
			}
			else
			{
				if(temperatura.val() < 35.5 || temperatura.val() >= 39)
				{
					return 3;
				}
			}
		}
	}

	function Respiracion()
	{
		if(respiracion.val() >= 10 && respiracion.val() <= 20)
		{
			return 1;
		}
		else
		{
			if(respiracion.val() > 20 && respiracion.val() <= 30)
			{
				return 2;
			}
			else
			{
				if(respiracion.val() < 10 || respiracion.val() > 30)
				{
					return 3;
				}
			}
		}
	}

	//Frecuencia cardiaca (F.C)
	function Pulso()
	{
		if(pulso.val() >= 50 && pulso.val() <= 90)
		{
			return 1;
		}
		else
		{
			if(pulso.val() > 90 && pulso.val() <= 120)
			{
				return 2;
			}
			else
			{
				if(pulso.val() < 50 || pulso.val() > 120)
				{
					return 3;
				}
			}
		}
	}

	function Orientacion()
	{
		if(estadoAlerta.val() == "Orientado")
		{
			return 1;
		}
		else
		{
			if(estadoAlerta.val() == "Desorientado")
			{
				return 2;
			}
			else
				if(estadoAlerta.val() == "Estuporoso")
				{
					return 3;
				}
		}
	}

	function Prioridad(puntuaje)
	{
		if(puntuaje >= 12)
		{
			return 1;
		}
		else
		{
			if(puntuaje >= 9 && puntuaje <= 11)
			{
				return 2;
			}
			else
			{
				if(puntuaje >= 6 && puntuaje  <= 8)
				{
					return 3;
				}
				else
				{
					return 4;
				}
			}
		}
	}

	function AsignaColor(prioridad)
	{
		if(prioridad == 1)
		{
			return "#ff0000";//rojo
		}
		else
		{
			if(prioridad == 2)
			{
				return "#ffff00";//amarillo
			}
			else
			{
				if(prioridad == 3)
				{
					return "#00A859";//verde
				}
				else
					return "#00B6FF";//azul
			}
		}
	}

	function actualizarTips( t ) {
		tips
		.text( t )
		.addClass( "ui-state-highlight" );
	//	setTimeout(function() {
	//		tips.removeClass( "ui-state-highlight", 5500 );
	//	}, 500 );
	}

	function checarLongitud( o, n, min, max ) {
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			actualizarTips( "La longitud de " + n + " debe de ser entre " +
					min + " y " + max + "." );
			return false;
		} else {
			return true;
		}
	}

	function checarExpRegular( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			actualizarTips( n );
			return false;
		} else {
			return true;
		}
	}

	$( "#modal-tabla-triage" ).dialog({
		title:'Material de valoraci&oacute;n medica',
		autoOpen: false,
		height: 550,
		width: 780,
		modal: true,

		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			todosLosCampos.val( "" ).removeClass( "ui-state-error" );
		}
	});			

	$( "#dialog-form" ).dialog({
		autoOpen: false,
		height: 400,
		width: 650,
		modal: true,

		buttons: {
			"Evaluar": function() {
				var esVerdadero = true;
				var pts = 0;
				var color
				todosLosCampos.removeClass( "ui-state-error" );
				
				esVerdadero = esVerdadero && checarLongitud( nombre, "nombre", 3, 15 );
				esVerdadero = esVerdadero && checarLongitud( edad, "edad",1,2 );
				esVerdadero = esVerdadero && checarLongitud( presion, "presion",2,3);
				esVerdadero = esVerdadero && checarLongitud( presion2, "presion2",2,3);
				esVerdadero = esVerdadero && checarLongitud( temperatura, "temperatura",1,4);

				esVerdadero = esVerdadero && checarExpRegular( nombre,/^[a-zA-Z' ']+$/, "El campo nombre solo permite letras" );
				esVerdadero = esVerdadero && checarExpRegular( edad, /^[0-9]+$/, "El campo edad solo permite numeros");
				esVerdadero = esVerdadero && checarExpRegular( presion, /^[0-9]+$/, "El campo presion solo permite numeros" );
				esVerdadero = esVerdadero && checarExpRegular( presion2, /^[0-9]+$/, "El segundo campo de presion solo permite numeros" );
				esVerdadero = esVerdadero && checarExpRegular( temperatura, /^\d{1,2}(\.\d)?$/, "El  campo temperatura solo permite numeros enteros o decimales ejemplo: 35 o 35.0");
				esVerdadero = esVerdadero && checarExpRegular( respiracion, /^([0-9]+)$/, "El  campo respiracion solo permite numeros" );
				esVerdadero = esVerdadero && checarExpRegular( pulso, /^([0-9]+)$/, "El  campo pulso solo permite numeros" );

				pts = Presion() + Temperatura() + Respiracion() + Pulso() + Orientacion();
				nivel = Prioridad(pts);
				color = AsignaColor(nivel);

				if ( esVerdadero ) {
						$( "." + nivel ).append(
						"<tr >" +
							"<td class='celda-traige' bgcolor='" + color + "'>" + nivel + "</td>" + 
							"<td class='celda-traige'>" + nombre.val() + "</td>" + 
							"<td class='celda-traige'>" + sexo.val() + "</td>" +
							"<td class='celda-traige'>" + edad.val() + "</td>" + 
							"<td class='celda-traige'>" + presion.val() + "-" + presion2.val() + "</td>" +
							"<td class='celda-traige'>" + temperatura.val() + " &deg;C" + "</td>" +
							"<td class='celda-traige'>" + respiracion.val() + "</td>" +
							"<td class='celda-traige'>" + pulso.val() + "</td>" +
							"<td class='celda-traige'>" + estadoAlerta.val() + "</td>" +
						"</tr>"
						);
					$( this ).dialog( "close" );
				}
			},
			Cancelar: function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			todosLosCampos.val( "" ).removeClass( "ui-state-error" );
		}
	});

	$( ".esfigmomanómetro" ).click(function() {
		$( "#modal-tabla-triage" ).load('../../ajax_servicio_urgencias_triage.php?type=1');
		$( "#modal-tabla-triage" ).dialog( "open" );
	});		

	$( ".termometro" ).click(function() {
		$( "#modal-tabla-triage" ).load('../../ajax_servicio_urgencias_triage.php?type=2');
		$( "#modal-tabla-triage" ).dialog( "open" );
	});	

	$( ".Medidor_de_Flujo_Pulmonar" ).click(function() {
		$( "#modal-tabla-triage" ).load('../../ajax_servicio_urgencias_triage.php?type=3');
		$( "#modal-tabla-triage" ).dialog( "open" );
	});	

	$( ".Medidor_pulso" ).click(function() {
		$( "#modal-tabla-triage" ).load('../../ajax_servicio_urgencias_triage.php?type=4');
		$( "#modal-tabla-triage" ).dialog( "open" );
	});	

	$( "#simulacion-boton" ).button().click(function() {
		$( "#dialog-form" ).dialog( "open" );
	});

});
