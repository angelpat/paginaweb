$(function(){

	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});

	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
	
	$(document).ready( function(){	
		
		$('#lofslidecontent45').lofJSidernews( { 
			interval:8000,
		 	easing:'easeInOutQuad',
			duration:1200,
			auto:true 
		});		
		tooltip();
		var data = "type=1&tipoNoticia=P";
		$('#noticias').load('ajax_noticia.php', data, '');	
		
	});			
	
	$( "#modal-map" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,		
		height: 610,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			//allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( "#modal-video" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 470,
		width: 670,
		modal: false,
		show: "blind",
		hide: "explode",
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			//allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});	
	
		
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-map" ).load('ajax_index.php?type=1');
		$( "#modal-map" ).dialog( "open" );
	});	

	
	$( "#modal-bienvenida" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 680,
		width: 630,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		}
	});			
		
	$( "#btn-bienvenida" )
	.button({
		icons: {
            primary: "ui-icon-comment"
        }
		})
	.click(function() {
		$( "#modal-bienvenida" ).load('ajax_index.php?type=2');
		$( "#modal-bienvenida" ).dialog( "open" );
	});	
	
	$( "#vid-recien-nacido" ).click(function() {
		$( "#modal-video" ).load('ajax_index.php?type=3');
		$( "#modal-video" ).dialog( "open" );
	});	
	
	
});
