$(function(){

	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
	
	$(".btn").button();	

	$( "#modal-localizacion" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 650,
		width: 940,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-localizacion" ).load('ajax_localizacion.php?type=1');
		$( "#modal-localizacion" ).dialog( "open" );
	});	
			
});
