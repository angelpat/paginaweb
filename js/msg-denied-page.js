$(function(){
	
	$(document).ready(function() {        

		$("#msg-correo").hide();

	    return false;	        
	});	
	
	function checarLongitud( o, n, min, max ) {			
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			actualizarTips( "La longitud de " + n + " debe de ser entre " + min + " y " + max );
			return false;
		} 
		else{
			return true;
		}
	}
	
	function checarExpRegular( o, regexp, n ) {
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			actualizarTips( n );
			return false;
		} else {
			return true;
		}
	}
	
	function campoRequerido( o, n, isRequerido ){
		if(o.val() == "" && isRequerido){
			o.addClass( "ui-state-error" );
			actualizarTips( "El campo " + n + " es requerido" );					
			return false;
		}
		else{
			return true;
		}
	}
	
	function actualizarTips( t ) {
		tips
		.text( t )
		.addClass( "ui-state-error" );
	}		
	

	$( "#btn-enviar" ).click(function() {		
		
		var reason = $( "#reason" ), 
			urlhost = $( "#urlhost" ),
			nombre = $( "#nombre" ),
			servicio = $( "#servicio" ),
			correo = $( "#correo" ),
			motivo = $( "#motivo" );
		    allFields = $([]).add( nombre ).add( servicio ).add( correo );
			tips = $( "#tip" );
			
			
		var bValid = true;

		    allFields.removeClass( "ui-state-error" );
		    tips.removeClass( "ui-state-error" );			
			
		    bValid = bValid && campoRequerido( nombre, "Nombre",true);
		    bValid = bValid && checarLongitud( nombre, "Nombre", 3, 50 );
		   // bValid = bValid && checkRegexp( nombre, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar un nombre" );
		    
		    bValid = bValid && campoRequerido( servicio, "Servicio",true);
		    bValid = bValid && checarLongitud( servicio, "Servicio", 2, 50 );
		   // bValid = bValid && checkRegexp( nombre, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar un nombre" );
		    		    
		    bValid = bValid && campoRequerido( correo, "correo-e",true);
		    bValid = bValid && checarLongitud( correo, "correo-e", 7, 75 );		    
		    bValid = bValid && checarExpRegular( correo, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Ejemplo: nombre@correo.com" );
		    
        if ( bValid ) {
			$.post("ajax_msg_deniedpage.php?",
					{type: 1, 
					reason: reason.val(),
					urlhost: urlhost.val(),
					nombre: nombre.val(),
					servicio: servicio.val(),
					correo: correo.val(),
					motivo: motivo.val()
					}, function(){
			});		
				allFields.removeClass( "ui-state-error" );
				tips.removeClass( "ui-state-error" );									
				
				$( "#msg-error" ).hide();
				$( "#msg-alerta" ).hide();
				$( "#tabla" ).hide();
				
				$( "#msg-correo" ).show();
				
		}									    
        return false;
		
		
	});			
	
});
