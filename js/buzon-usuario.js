$(function(){
	
	
	$(document).ready(function() {
		$(".date").val();
		$( "#txt-servicio-otro" ).attr( "disabled", "disabled" );	
		$( "#txt-derecho-otro" ).attr( "disabled", "disabled" );	
		
		$("#tipo-felicitacion").attr("checked", true);
		$("#tipo-felicitacion").next().addClass("rb-buzon");

		$("#derecho-ssa").attr("checked", true);
		$("#derecho-ssa").next().addClass("rb-buzon");
		
		$("#servicio-urgencias").attr("checked", true);
		$("#servicio-urgencias").next().addClass("rb-buzon");		
	});

	$( "#modal-mensaje" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
	
	// Fechas	
	$(".date").datepicker();          
		
	$(".date" ).datepicker( "option", {dateFormat: "dd/mm/yy"});
		
	var monthNamesShort = $( ".date" ).datepicker( "option", "monthNamesShort" );
	$( ".date" ).datepicker( "option", "monthNamesShort", ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'] );

	var monthNames = $( ".date-picker" ).datepicker( "option", "monthNames" );
	$( ".date" ).datepicker( "option", "monthNames", ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] );
	
	var dayNames = $( ".date-picker" ).datepicker( "option", "dayNames" );
	$( "date" ).datepicker( "option", "dayNames", ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'] );
	
	var dayNamesMin = $( ".fecha, .date-picker" ).datepicker( "option", "dayNamesMin" );
	$( ".date" ).datepicker( "option", "dayNamesMin", ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'] );            	

	var nextText = $( ".fecha, .date-picker" ).datepicker( "option", "nextText" );
	$( ".date" ).datepicker( "option", "nextText", 'Siguiente' );

	var prevText = $( ".fecha, .date-picker" ).datepicker( "option", "prevText" );
	$( ".date" ).datepicker( "option", "prevText", 'Anterior' );     		

	$("#servicio-otro").click(function(){		
		$( "#txt-servicio-otro" ).attr( "disabled", false );			
		$( "#txt-servicio-otro" ).val("");			
	});

	$("#derecho-otro").click(function(){		
		$( "#txt-derecho-otro" ).attr( "disabled", false );			
		$( "#txt-derecho-otro" ).val("");			
	});
	
	$(".rb-ser-activo").click(function(){
				
		var clase = $(this).attr("class");
		
		if(clase.search("rb-ser-inactivo") == -1)
		{
			$( "#txt-servicio-otro" ).attr( "disabled", "disabled" );	
			$( "#txt-servicio-otro" ).val("");	
		}
				
		$(".rb-ser-activo").next().removeClass("rb-buzon");		
		$(this).next().addClass("rb-buzon");
		
	});
		
	$(".rb-tipo-activo").click(function(){
		$(".rb-tipo-activo").next().removeClass("rb-buzon");		
		$(this).next().addClass("rb-buzon");		
	});
	
	
	$(".rb-derecho-activo").click(function(){
		
		var clase = $(this).attr("class");
		
		if(clase.search("rb-derecho-inactivo") == -1)
		{
			$( "#txt-derecho-otro" ).attr( "disabled", "disabled" );	
			$( "#txt-derecho-otro" ).val("");	
		}
				
		$(".rb-derecho-activo").next().removeClass("rb-buzon");		
		$(this).next().addClass("rb-buzon");		
	});
		
	// Validaciones
		
	function updateTips( t ) {
		tips
			.text( t )
			//.addClass( "ui-state-highlight" );
			.addClass( "ui-state-error" );
	//	setTimeout(function() {
	//		tips.removeClass( "ui-state-highlight", 1500 );
	//	}, 500 );		
	}

	function updateMessage( t ) {
		tips
			.text( t )
			.addClass( "ui-state-highlight" );		
	}

	function isRequired( o, n ){
		if ( ! o.val() ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}
	
	function checkRegexp( o, regexp, n ) {		
		if ( !( regexp.test( o.val() ) ) ) {
			o.addClass( "ui-state-error" );
			updateTips( n );
			return false;
		} else {
			return true;
		}
	}	
	
	function checkLength( o, n, min, max ) {	
	
		if ( o.val().length > max || o.val().length < min ) {
			o.addClass( "ui-state-error" );
			updateTips( "La longitud de " + n + " debe estar entre " +
				min + " y " + max + " caracteres." );
			return false;
		} else {
			return true;
		}
	}		
		
	function validaFecha(o){  
		//El Formato es dd-mm-aaaa  		
		
		var fecha = o.val();
			    
	    // Cadena A�o  
	    var Ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
	    // Cadena Mes  
	    var Mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
	    // Cadena D�a  
	    var Dia= new String(fecha.substring(0,fecha.indexOf("/")))  
	  
	    // Valido el a�o  
	    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El anio esta incorrecto" ); 
	        return false  
	    }  
	    // Valido el Mes  
	    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El mes esta incorrecto" ); 
	        return false  
	    }  
	    // Valido el Dia  
	    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){  
	    	o.addClass( "ui-state-error" );
			updateTips( "El dia esta incorrecto" );
	        return false  
	    }  
	    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {  
	        if (Mes==2 && Dia > 28 || Dia>30) {  
		    	o.addClass( "ui-state-error" );
				updateTips( "El dia esta incorrecto" );
	            return false  
	        }  
	    }  
	  	return true    
	}  
	  		
	function concatenaFecha(fecha, hora, meridiano){
		
		var horaCompleta, fechaCompleta;
		
		if(meridiano == "AM"){
			horaCompleta = hora.toString() + ':00:00.000';			
		}		
		else{
			horaCompleta = parseInt(hora) + parseInt('12');
			horaCompleta = horaCompleta.toString() + ':00:00.000';
		}
				
		fechaCompleta = fecha + ' ' + horaCompleta; 
		
		return fechaCompleta;
	}
	
		
	$( "#btn-enviar" )
	.button({
		icons: {
            primary: "ui-icon-comment"
        }
		})
	.click(function() {
		
		var rbQueja = $( "input:radio[name=rb-tipo]:checked" ),
			rbDerecho = $( "input:radio[name=rb-derecho]:checked" ),
		    txtDerechoOtro = $( "#txt-derecho-otro" ),
			txtNombrePersonal = $( "#txt-nombre-personal" ),
			fecha = $( "#fecha" ),
			hora = $( "#hora" ),
			meridiano = $( "#meridiano" ),
			turno = $( "#turno" ),
			rbServicio = $('input:radio[name=rb-servicio]:checked'),
			txtServicioOtro = $( "#txt-servicio-otro" ),
			narrativa = $ ( "#narrativa" ),
			nombre = $( "#nombre" ),
			domicilio = $( "#domicilio" ),
			telefono = $( "#telefono" ),
			correo = $( "#correo-e" )
			allFields = $( [] ).add( txtDerechoOtro ).add( txtNombrePersonal ).add( fecha ).add( txtServicioOtro ).add( narrativa ).add( nombre ).add( domicilio ).add ( telefono ).add( correo ) , 
			tips = $( "#tip" );
	
			allFields.removeClass( "ui-state-error" );
			updateTips( "" );
			tips.removeClass( "ui-state-error" );
		
		var bValid = true;
				
		if (rbDerecho.val() == "otro") {	
			bValid = bValid && checkLength( txtDerechoOtro, "derechohabiencia", 3, 50 );
			bValid = bValid && checkRegexp( txtDerechoOtro, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar la derechohabiencia" );
		}		
			
		if(txtNombrePersonal.val().length >= 1){
			bValid = bValid && checkLength( txtNombrePersonal, "nombre de quien recibio la atencion", 3, 50 );
			bValid = bValid && checkRegexp( txtNombrePersonal, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar el nombre del quejoso" );		
		}
		
		bValid = bValid && checkRegexp( fecha, /^([0-9][0-9])\/([0-9][0-9])\/([0-9][0-9][0-9][0-9])$/, "Formato de Fecha incorrecta" );
		
		bValid = bValid && validaFecha( fecha );
		
		if (rbServicio.val() == "otro") {	
			bValid = bValid && checkLength( txtServicioOtro, "servicio", 3, 50 );
			bValid = bValid && checkRegexp( txtServicioOtro, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar la el servicio" );
		}
		
		bValid = bValid && checkLength( narrativa, "narrativa", 5, 4000 );
		
		if(nombre.val().length >= 1){
			bValid = bValid && checkLength( nombre, "nombre", 5, 75 );
			bValid = bValid && checkRegexp( nombre, /^[a-zA-Z �����A������,:;-]+$/i, "Debe especificar el nombre" );
		}
		
		if(domicilio.val().length >= 1){
			bValid = bValid && checkLength( domicilio, "domicilio", 5, 100 );
			bValid = bValid && checkRegexp( domicilio, /^[a-zA-Z 0-9 �����A������,:;-]+$/i, "Debe especificar el domicilio" );
		}
		
		if(telefono.val().length >= 1){
			bValid = bValid && checkLength( telefono, "telefono", 5, 25 );		
			bValid = bValid && checkRegexp( telefono, /^[a-zA-Z 0-9 ()-.]+$/i, "El numero de telefono solo acepta numeros y espacios en blanco" );
		}
		
		if(correo.val().length >= 1){
			bValid = bValid && checkLength( correo, "correo-e", 7, 75 );
			// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
			bValid = bValid && checkRegexp( correo, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "Ejemplo: nombre@correo.com" );
		}
				
		if ( bValid ) {
			
			var derechoabiencia = rbDerecho.val();
			var fecha = concatenaFecha(fecha.val(), hora.val(), meridiano.val());
			var servicioAtencion = rbServicio.val();

			if(derechoabiencia == 'otro' )
				derechoabiencia = txtDerechoOtro.val();
			
			if(servicioAtencion == 'otro' )
				servicioAtencion = txtServicioOtro.val();
			
			var data = 'type=1&tipo=' + rbQueja.val() + 
			                 '&derechoabiencia=' + derechoabiencia + 
			                 '&nombrePersonal=' + txtNombrePersonal.val() +
			                 '&fecha=' + fecha +
			                 '&turno=' + turno.val() +
			                 '&servicioAtencion=' + servicioAtencion +
			                 '&narrativa=' + narrativa.val() +
			                 '&nombre=' + nombre.val() +
			                 '&domicilio=' + domicilio.val() +
			                 '&telefono=' + telefono.val() +
			                 '&mail=' + correo.val();
			
			$.post('ajax_buzon_usuario.php', data, function(){				
				//alert("Mensaje Enviado");
				$( "#modal-mensaje" ).dialog( "open" );
			});			
		}
		
	});	

	 $(".cleditor").cleditor({
	        width:        735, // width not including margins, borders or padding
	        height:       350, // height not including margins, borders or padding
	        controls:     // controls to add to the toolbar
	                      "bold italic underline strikethrough subscript superscript | font size " +
	                      "style | removeformat | bullets numbering  " +
	                      " | alignleft center alignright justify | undo redo | " +
	                      "rule link unlink | cut copy paste pastetext | print source",
	        colors:       // colors in the color popup
	                      "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
	                      "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
	                      "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
	                      "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
	                      "666 900 C60 C93 990 090 399 33F 60C 939 " +
	                      "333 600 930 963 660 060 366 009 339 636 " +
	                      "000 300 630 633 330 030 033 006 309 303",    
	        fonts:        // font names in the font popup
	                      "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
	                      "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
	        sizes:        // sizes in the font size popup
	                      "1,2,3,4,5,6,7",
	        styles:       // styles in the style popup
	                      [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
	                      ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
	                      ["Header 6","<h6>"]],
	        useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
	        docType:      // Document type contained within the editor
	                      '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
	        docCSSFile:   // CSS file used to style the document contained within the editor
	                      "", 
	        bodyStyle:    // style to assign to document body contained within the editor
	                      "margin:4px; font:10pt Arial,Verdana; cursor:text"
		});
	
	
});
