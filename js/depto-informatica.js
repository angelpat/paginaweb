$(function(){
	
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});	
	
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		$('#features').jshowoff();
	});	
	
	$(".btn").button();	
	
	$( "#btn-organigrama" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-organigrama" ).load('../../ajax_depto_informatica.php?type=3');
		$( "#modal-organigrama" ).dialog( "open" );
	});	
	
	$( "#modal-sigho-modelo" ).dialog({
		title:'Modelo SIGHO',
		autoOpen: false,
		height: 620,
		width: 770,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		

	$( "#modal-sigho-ubicando" ).dialog({
		title:'Ubicando al SIGHO',
		autoOpen: false,
		height: 460,
		width: 640,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	
	$( "#modal-organigrama" ).dialog({
		title:'Organigrama',
		autoOpen: false,
		height: 460,
		width: 640,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});	
	
	$( "#btn-sigho-modelo" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-sigho-modelo" ).load('../../ajax_depto_informatica.php?type=1');
		$( "#modal-sigho-modelo" ).dialog( "open" );
	});		
	
	$( "#btn-sigho-ubicando" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-sigho-ubicando" ).load('../../ajax_depto_informatica.php?type=2');
		$( "#modal-sigho-ubicando" ).dialog( "open" );
	});	
		
	$( ".bullet" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-radio-on" );
		$(this).find("span").addClass( "ui-icon-bullet" );		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-bullet" );
		$(this).find("span").addClass( "ui-icon-radio-on" );
	});		
			
});
