$(function(){
		
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});			
	
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');		
		$('#features').jshowoff();
	});		
	
	$(".btn").button();	

	$( "#modal-organigrama" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 570,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-organigrama" ).load('../../ajax_servicio_archivo.php?type=1');
		$( "#modal-organigrama" ).dialog( "open" );
	});	
		
	
	$( ".bullet" ).hover(function() {		
		$(this).find("span").removeClass( "ui-icon-radio-on" );
		$(this).find("span").addClass( "ui-icon-bullet" );		
	}, function(){
		$(this).find("span").removeClass( "ui-icon-bullet" );
		$(this).find("span").addClass( "ui-icon-radio-on" );
	});	
	
	
});
