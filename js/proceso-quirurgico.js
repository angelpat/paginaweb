$(function(){
		
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});			
	
	$(document).ready(function () {		
		
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		
    	$( "#pro-quirurgico-login").slideUp('fast', function(){    		
    		var data1 = "type=1";    		
        	$(this).load( "../../ajax_proceso_quirurgico.php", data1, function(){        		
        		$(this).slideDown( "fast" );
			});
        });			    			    	
    	
    	$( "#pro-quirurgico-info").slideUp('fast', function(){
    		var data2 = "type=2";
        	$(this).load( "../../ajax_proceso_quirurgico.php", data2, function(){
        		$(this).slideDown( "fast" );
			});
        });	

	});	
	
	$(".btn").button();
	
});
