$(function(){
		
	$( "#modal-contacto" ).dialog({
		autoOpen: false,
		show: "blind",
		hide: "explode"
	});
		
	$("#menu-contact").click(function(){
		$( "#modal-contacto" ).dialog( "open" );
		return false;		
	});		
		
	$(document).ready(function(){
		$('#contador').load('../../ajax_contador.php', 'url=' + location.href,'');
		$('#features').jshowoff();
	});		
	
	$(".btn").button();	

	$( "#modal-ensenanza" ).dialog({
		title:'Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra',
		autoOpen: false,
		height: 510,
		width: 780,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			allFields.val( "" ).removeClass( "ui-state-error" );
		}
	});		
	
	$( "#btn-zoom" )
	.button({
		icons: {
            primary: "ui-icon-circle-zoomin"
        }
		})
	.click(function() {
		$( "#modal-ensenanza" ).load('../../ajax_depto_ensenanza.php?type=1');
		$( "#modal-ensenanza" ).dialog( "open" );
	});	

			
});
