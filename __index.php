<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);

?>
<!DOCTYPE html>
<html>
    <head>
    	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">

        <meta name="author" content="Hospital General con Especialidades Juan Maria de Salvatierra" />         
        <meta name="description" content="Sitio Oficial del Benemerito Hospital General con Especialidades Juan Maria de Salvatierra" /> 
		<meta name="keywords" content="hospital, hgejms, hospital salvatierra,la paz, bcs, mexico, salud, portal gubernamental, secretarias, especialidades medicas, especialidades quirurgicas, diagnostico y tratamiento, unidades medicas, cursos medicos, investigacion medica, administracion, sigho, urgencias, salud mental" />
 		<meta name="robots" content="index" > 
 		<meta name="revisit-after" content="1 month">
  <!--  <meta name="revised" content="Sergio Garcia, 04/07/2012" />   -->

		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css"/>
		<link rel="stylesheet" href="css/master.css"/>
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />		
		<link rel="stylesheet" href="css/easing.css" />
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="js/menu.js"></script>
		<script src="js/index.js"></script>
		<script src="js/tooltip.js"></script>    		
		<script src="js/tip-salud.js"></script>		
		<script src="js/jquery.easing.js"></script>
		<script src="js/easing.js"></script>

 		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
		</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38641570-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	
		<style>	
			ul.lof-main-wapper li {
				position:relative;	
			}
		</style>
 		<title>Benemerito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra, La Paz, B.C.S.</title>
<!--[if IE 6]>
<style>
body {behavior: url("csshover3.htc");}
#menu li .drop {background:url("img/drop.gif") no-repeat right 8px; 
</style>
<![endif]--> 		
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
		<div id="modal-tip"></div>
		<div id="modal-bienvenida"></div>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>				
		<div id="modal-video"></div>
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>		
			<section style="float: left;display: block; padding: 0px 10px">
				<div id="lofslidecontent45" class="lof-slidecontent">
					<div class="preload"><div></div></div>
						<div class="lof-main-outer">
							<ul class="lof-main-wapper">	
						        <li>						
									<img src="img/easing/images/2012/10/26-01.JPG" title="GALARDON REY PACAL" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						        		<h3><a target="_parent" title="Laboratorio" href="galeria/2012.10.26.php">Galard&oacute;n Rey Pacal (Galer&iacute;a)</a></h3>						            	
						                <p>El programa de aseguramiento de calidad otorgo el Galard&oacute;n Rey Pacal al Laboratorio Cl&iacute;nico del B. H. General con Especialidades Juan Ma. de Salvatierra
						               
						                </p>
						            </div>
								</li> 		
						        <li>						
									<img src="img/easing/images/2012/10/24-01.jpg" title="GALARDON REY PACAL" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						        		<h3><a target="_parent" title="Laboratorio" href="galeria/2012.10.24.php">Reconocimiento(Galer&iacute;a)</a></h3>						            	
						                <p>La Asociaci&oacute;n M&eacute;dica del Hospital realizo reconocimiento a la trayectoria de dos grandes m&eacute;dicos<br>
						                El Miguel Mondrag&oacute;n Gonz&aacute;lez y el Dr. Juan Manuel Cota Abaroa 
						                </p>
						            </div>
								</li>																						
						        <li>						
									<img src="img/easing/images/2012/09/11-01.png" title="Visita del Secretario de Salud del Estado de Baja California Sur" height="300" width="655">            
						        	<div class="lof-main-item-desc">						            	
						                <p>El Benem&eacute;rito Hospital General con Especialidades &quot;Juan Mar&iacute;a de Salvatierra&quot; ha realizado entre 2011 y 2012, 6 trasplantes de ri&ntilde;&oacute;n </p>
						            </div>
								</li> 							
						        <li>						
									<img src="img/easing/images/2012/08/29-01.jpg" title="Visita del Secretario de Salud del Estado de Baja California Sur" height="300" width="655">            
						        	<div class="lof-main-item-desc">						            	
						                <p>Visita del Secretario de Salud del Estado de Baja California Sur a la comunidad M&eacute;dica del Benem&eacute;rito Hospital General con Especialidades &quot;Juan Mar&iacute;a de Salvatierra&quot;</p>
						            </div>
								</li> 																
						        <li>						
									<img src="img/easing/images/2012/08/29-03.jpg" title="Visita del Secretario de Salud del Estado de Baja California Sur" height="300" width="655">            
						        	<div class="lof-main-item-desc">						            	
						                <p>Visita del Secretario de Salud del Estado de Baja California Sur a la comunidad M&eacute;dica del Benem&eacute;rito Hospital General con Especialidades &quot;Juan Mar&iacute;a de Salvatierra&quot;</p>
						            </div>
								</li> 															
						  		<li>
									<img src="img/easing/images/2012/01/20-01.jpg" title="Acceso" height="300" width="655">           
						            <div class="lof-main-item-desc">
						            	<h3><a target="_parent" title="Acceso" href="#">Acceso</a></h3>
						                <p>Acceso al &aacute;rea de consulta externa</p>
						            </div>
						        </li> 
						       	<li>
						        	<img src="img/easing/images/2012/01/20-02.jpg" title="Hospital Bicentenario" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						                <h3><a target="_parent" title="Hospital Bicentenario" href="#">Hospital Bicentenario</a></h3>
						                <p>Hospital General con Especialidades Juan Maria de Salvatierra - 8 de Octubre de 2007</p>						
						            </div>
						       	</li> 
 					       	
						       	<li>
						        	<img src="img/easing/images/2012/01/20-03.jpg" title="BENEMERITO NUEVO HOSPITAL GENERAL CON ESPECIALIDADES" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						            	
						            	<p>BENEMERITO NUEVO HOSPITAL GENERAL CON ESPECIALIDADES <br/>"JUAN MARIA DE SALVATIERRA"</p>
						            </div>
						        </li> 
						        <!--  
						        <li>						
						        	<img src="img/easing/images/2012/01/20-04.jpg" title="Laboratorio" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						                <h3><a target="_parent" title="Laboratorio" href="#">Laboratorio</a></h3>
						                <p>Sala de espera de Laboratorio de Anal&iacute;sis Cl&iacute;nico</p>
						            </div>
						        </li> 	-->					        
						        <li>						
						        	<img src="img/easing/images/2012/01/20-05.jpg" title="Consulta Externa" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						            	<h3><a target="_parent" title="Consulta Externa" href="#">Consulta Externa</a></h3>
						                <p>Consulta externa, consultorios del 19 al 37</p>
						            </div>
						        </li> 
						        <li>						
									<img src="img/easing/images/2012/01/20-07.jpg" title="Laboratorio de Anal&iacute;sis Cl&iacute;nico" height="300" width="655">            
						        	<div class="lof-main-item-desc">
						            	<h3><a target="_parent" title="Laboratorio de Anal&iacute;sis Cl&iacute;nico" href="#">
						            	Laboratorio de Anal&iacute;sis Cl&iacute;nico</a></h3>
						                <p>&Aacute;rea de Hormonas y Toxicolog&iacute;a</p>
						            </div>
								</li> 	
						
							</ul>  	
						</div>
					  	<div class="lof-navigator-outer">
					  	
					  	
					  		<ul class="lof-navigator">
					  			<li>
					            	<div>
					                	<img src="img/easing/thumbs/2012/10/26-01.JPG" alt="Galard&oacute;n Rey Pacal" />
					                	<h3> Galard&oacute;n Rey Pacal al Laboratorio Cl&iacute;nico</h3>
					                  	<span>Octubre de 2102</span>
					                </div>    
					            </li>
					           	<li>
					           		<div>
					                    <img src="img/easing/thumbs/2012/10/24-01.jpg" alt="Laboratorio" />
					                    <h3>Reconociento a la trayectoria</h3>
					                    <span></span>24 de Octubre de 2012
					                </div>
					            </li>					            
					  			<li>
					            	<div>
					                	<img src="img/easing/thumbs/2012/09/11-01.png" alt="Aula de Ense&ntilde;anza" />
					                	<h3>Seis trasplantes de ri&ntilde;&oacute;n entre 2011 y 2012</h3>
					                  	<span>11 de Septiembre de 2102</span>
					                </div>    
					            </li>
					            <li>
					            	<div>
					                	<img src="img/easing/thumbs/2012/08/29-01.JPG" alt="Aula de Ense&ntilde;anza" />
					                	<h3>Aula de Ense&ntilde;anza e Investigaci&oacute;n </h3>
					                  	<span>29 de Agosto de 2102</span>
					                </div>    
					            </li>					  		
					            <li>
					            	<div>
					                	<img src="img/easing/thumbs/2012/08/29-03.JPG" alt="Aula de Ense&ntilde;anza" />
					                	<h3>Aula de Ense&ntilde;anza e Investigaci&oacute;n </h3>
					                  	<span>29 de Agosto de 2102</span>
					                </div>    
					            </li>	
					  			
					            <li>
					            	<div>
					                	<img src="img/easing/thumbs/2012/01/20-01.JPG" alt="Acceso" />
					                	<h3>Acceso</h3>
					                  	<span></span>Acceso al &aacute;rea de consulta externa
					                </div>    
					            </li>
					             <li>
					             	<div>
					                	<img src="img/easing/thumbs/2012/01/20-02.JPG" alt="Hospital Bicentenario" />
					                 	<h3>Hospital Bicentenario</h3>
					                  	<span></span>Hospital General con Especialidades Juan Maria de Salvatierra - 8 de Octubre de 2007
					                </div>    
					            </li>		
			
					            <li>
					            	<div>
					                    <img src="img/easing/thumbs/2012/01/20-03.JPG" alt="BENEMERITO NUEVO HOSPITAL GENERAL CON ESPECIALIDADES 'JUAN MARIA DE SALVATIERRA'" />
					                    <h3></h3>
					                	<span></span>BENEMERITO NUEVO HOSPITAL GENERAL CON ESPECIALIDADES "JUAN MARIA DE SALVATIERRA"
					                </div>     
					            </li>	
					            <!--  				            
					           	<li>
					           		<div>
					                    <img src="img/easing/thumbs/2012/01/20-04.JPG" alt="Laboratorio" />
					                    <h3>Laboratorio</h3>
					                    <span></span>Sala de espera de Laboratorio de Anal&iacute;sis Cl&iacute;nico
					                </div>
					            </li>    -->	
					            <li>
					           		 <div>
					                 	<img src="img/easing/thumbs/2012/01/20-05.JPG" alt="Consulta Externa" />
					                 	<h3>Consulta Externa</h3>
					                 	<span></span>Consulta externa, consultorios del 19 al 37
					                 </div>   
					            </li>   
					           <li>
					           		<div>
					                    <img src="img/easing/thumbs/2012/01/20-07.JPG" alt="Laboratorio de Anal&iacute;sis Cl&iacute;nico" />
					                    <h3>Laboratorio de Anal&iacute;sis Cl&iacute;nico</h3>
					               		<span></span>&Aacute;rea de Hormonas y Toxicolog&iacute;a
					                </div>
					            </li>	
					             			              		
					        </ul>
					  	</div>
 					</div> 
				</section>
			<div id="content">
				<section class="index-izq">				
					<div id="noticias" style="padding-right: 10px;">							
					</div>					
					<br>
					<section class="folletos">
						<div class="folleto">							
							<a target="_blank" class="tooltip" href="http://promocion.salud.gob.mx/dgps/interior1/programas/cartillas.html" title="Cartilla Nacional de Vacunaci&oacute;n"><img src="img/departamentos/med-prev/folletos/cns-n-n-0-9.jpg" width="100" alt="Vacunación">
								Cartilla Nacional de Vacunaci&oacute;n
							</a>							
						</div>
						<div class="folleto">
							<a target="_blank" class="tooltip" href="http://promocion.salud.gob.mx/dgps/interior1/materiales2.html" title="Influenza A (H1N1)"><img src="img/departamentos/med-prev/folletos/Influenza.jpg" width="100" height="130" alt="Influenza A (H1N1)">
								Influenza A (H1N1)
							</a>							
						</div>
						<div class="folleto">
							<a target="_blank" class="tooltip" href="resources/departamentos/med-prev/folletos/ViveLibreDeTuberculosis.pdf" title="La tos latosa puede ser tuberculosa"><img src="img/departamentos/med-prev/folletos/tuberculosis.jpg" width="100" height="130" alt="La tos latosa puede ser tuberculosa">
								Vive libre de Tuberculosis
							</a>							
						</div>
						<div class="folleto">
							<a id="vid-recien-nacido" class="tooltip" href="#" title="Recien Nacido"><img src="img/departamentos/med-prev/folletos/recien-nacido.jpg" width="100" height="130" alt="Recien Nacido">
								Los cuidados del recien nacido
							</a>							
						</div>		
						<div class="folleto">
							<a target="_blank" class="tooltip" href="resources/departamentos/med-prev/folletos/TamizAudutivoNeonatal.pdf" title="Tamiz Auditivo Neonatal"><img src="img/departamentos/med-prev/folletos/tamiz-audutivo-neonatal.jpg" width="100" height="130" alt="Tamiz Auditivo Neonatal">
								Tamiz Auditivo Neonatal
							</a>							
						</div>							
						<div class="folleto">
							<a target="_blank" class="tooltip" href="http://promocion.salud.gob.mx/dgps/descargas1/influenza/mat/triptico_embarazadas_2_1.pdf" title="Prevenci&oacute;n de la Influenza A(H1N1)"><img src="img/departamentos/med-prev/folletos/prevencion-influencia-A(H1N1).jpg"  width="100" height="130" alt="Prevenci&oacute;n de la Influenza A(H1N1)">
								Prevenci&oacute;n de la Influenza
							</a>							
						</div>		
					</section>					
				</section>									
				<section class="index-der">				
			<!-- 	
					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 20px;">Dos grandes M&eacute;dicos</p><br/>	
					</div>						
 					<a href="galeria/2012.10.24.php" class="tooltip"  title="Ir a galeria">
 						<img alt="" src="img/galeria/2012.10.24/galeria.png" class="imgshadow" style="width: 285px; margin: 10px 0;">
					</a>
 -->
					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 18px;">D&iacute;a del Padre Salvatierra</p><br/>	
					</div>						
 					<a href="galeria/2012.11.15.php" class="tooltip"  title="Ir a galeria">
 						<img alt="" src="img/galeria/2012.11.15/galeria.png" class="imgshadow" style="width: 285px; margin: 10px 0;">
					</a>

					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 22px;">Galard&oacute;n Rey Pacal</p><br/>	
					</div>						
 					<a href="galeria/2012.10.26.php" class="tooltip"  title="Ir a galeria">
 						<img alt="" src="img/galeria/2012.10.26/galeria.jpg" class="imgshadow" style="width: 285px; margin: 10px 0;">
					</a>				
				
					<div style="padding-bottom: 10px;">
						<a href="#" id="btn-bienvenida" style="float: left; ">Bienvenida</a>						
						<div class="visitas">Visitas <?php echo $objContador->insertContadorVisitas($url) ?></div>
						<a target="_blank" href="http://www.facebook.com/hospitalsalvatierra"><img src="img/content/facebook.jpg" alt="Siguenos en Facebook" style="padding-left: 3px; width: 26px;" ></a>
						<!-- <a target="_blank" href="http://www.facebook.com/hospitalsalvatierra"><img src="img/content/facebook_text.jpg" alt="Siguenos en Facebook" style="padding-left: 3px;"></a>-->
					</div>	
		 			<?php echo $objCabecera->tipSalud("") ?>
		 			<!-- 
		 			<a id="vid-recien-nacido" class="tooltip" target="_blank" href="resources/content/2012-09-11-Carrera.pdf" title="Carrera en pro de la donaci&oacute;n de &oacute;rganos">
		 				<img alt="" src="img/noticias/2012/2012-09-22-carrera-thumb.JPG" class="imgshadow" width="285"/>
		 			</a>
		 			<br>
		 			
					<p style="color: #053B64; font-weight: bold;">Carrera en pro de la donaci&oacute;n de &oacute;rganos</p><br/>
					<p><b style="color: #053B64">Lugar:</b> Malec&oacute;n de La Paz</p><br/>
					<p><b style="color: #053B64">Fecha:</b> 22 de Septiembre de 2012</p><br/>
					
					<b style="color: #053B64">Categor&iacute;as:</b>
					<ul style="margin-left:60px;">
						<li>9 a 12 a&ntilde;os</li> 
						<li>13 a 15 a&ntilde;os</li>
						<li>16 a 20 a&ntilde;os</li>
					</ul>
					<br/>
					<b style="color: #053B64">Informes e inscripciones:</b><br/> 
					<p>Oficina de donaci&oacute;n y trasplante del B. Hospital General con especialidades &quot;Juan Maria de Salvatierra&quot;</p><br>
					<p><b style=" color: #053B64;">Tel&eacute;fono:</b> 17 5 05 01 <b style=" color: #053B64;">Ext: </b>6114</p><br/>
					<a style="color: #053B64; font-weight: bold;" target="_blank" href="resources/content/2012-09-11-Carrera.pdf"><img src="img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Descargar convocatoria</a>
					 -->
				</section>
			</div>
			<?php 		
				$objCabecera->pie();		
			?>		
		</div>             	 
    </body>
</html>
