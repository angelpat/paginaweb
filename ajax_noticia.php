<?php

include_once 'cls/clsNoticia.php';

$objNoticia = new Noticia();

$datos = array();

$type = $_GET['type'];
$tipoNoticia = $_GET['tipoNoticia'];

/**
 * Type 1: ""  Muestra las noticias publicadas
 * Type 2: ""  Plantilla sin imagen
 * 		 : "c" Plantilla con imagen centrada
 *       : "i" Plantilla con imagen del lado derecho
 *       : "d" Plantilla con imagen del lado izquierdo
 */

?>
<script type="text/javascript">

	$(".ver-noticia").click(function(){
		var data = 'type=2&idNoticia=' + $(this).attr('title');
		location.href='noticia.php?' + data;	
	});	
	
	/* ===================== Tip salud ===================== */
	
	$( "#modal-tip" ).dialog({
		title:'Tip Salud',
		autoOpen: false,
		height: 400,
		width: 600,
		modal: true,
		buttons: {
			"Cerrar": function() {
				$( this ).dialog( "close" );
			}
		},
		close: function() {
			// allFields.val( "" ).removeClass( "ui-state-error" );
		}				
	});			
		
	$( ".tip-salud-embarazada" ).click(function() {	
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=1');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-diabetico" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=2');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-hipertenso" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=3');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-tosedor" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=4');
		$( "#modal-tip" ).dialog( "open" );
	});		
	
	$( ".tip-salud-sida" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=5');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-violencia" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=6');
		$( "#modal-tip" ).dialog( "open" );
	});		
		
	$( ".tip-salud-diarrea" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=7');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-neumonia" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=8');
		$( "#modal-tip" ).dialog( "open" );
	});		
		
	$( ".tip-salud-vacunas" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=9');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-dengue" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=10');
		$( "#modal-tip" ).dialog( "open" );
	});			
	
	$( ".tip-salud-planificacion" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=11');
		$( "#modal-tip" ).dialog( "open" );
	});	
	
	$( ".tip-salud-recien-nacido" ).click(function() {		
		$( "#modal-tip" ).load('ajax_tip_salud.php?type=12');
		$( "#modal-tip" ).dialog( "open" );
	});	

</script>
<?php 
switch ($type) {

	case 1:
		
		$datos = $objNoticia->selectNoticias($tipoNoticia);
	
		if (empty($datos)) {
		?>
			<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 100%; width:500px;" >
				<p><span class="ui-icon ui-icon-document" style="float: left; margin-right: .3em;"></span>
				<strong>La consulta no produjo ningun resultado</strong></p>
			</div>		
		<?php 
		}else{		
		?>
    	<table class="noticia-zebra-azul" >
	    	<tr>	            
	            <th style="text-align: center;">Noticias</th>
	    	</tr>
	    	<?php  				    	   				
	    	foreach ($datos as $dato){
	    		
	    		if (($dato['plantilla'] == "") || ($dato['plantilla'] == " ")){?>
				<tr>
					<td>		
						<div class="info-noticia">
							<div class="noticia-desc">
								<a class="ver-noticia titulo" href="#" title="<?php echo $dato['idNoticia'] ?>" > <?php echo $dato['titulo'] ?></a><br><br>
								<a class="ver-noticia descripcion" href="#" title="<?php echo $dato['idNoticia'] ?>"> <?php echo $dato['descripcion'] ?></a><br><br>									
								<p class="fecha"> <?php echo htmlentities( $dato['lugarFecha'] )?></p>								
							</div>
						</div>						
					</td>	
				</tr>				
				<?php } elseif ($dato['plantilla'] == "c" || $dato['plantilla'] == "i" ){?>	
				<tr>				
					<td>	
						<div class="info-noticia">
							<div class="noticia-desc-img-izq">
								<a href="#" class="izq ver-noticia" title="<?php echo $dato['idNoticia'] ?>">
									<img alt="" src="<?php echo $dato['thumbRuta'] ?>" style="width:<?php echo $dato['thumbAncho'] ?>px; height:<?php echo $dato['thumbAltura'] ?>px;" class="der imgshadow" title=""/>
								</a>
							</div>
							<div class="noticia-desc">
								<a class="ver-noticia titulo" href="#" title="<?php echo $dato['idNoticia'] ?>" > <?php echo $dato['titulo'] ?></a><br><br>
								<a class="ver-noticia descripcion" href="#" title="<?php echo $dato['idNoticia'] ?>"> <?php echo $dato['descripcion'] ?></a>
								<p class="fecha"><?php echo htmlentities( $dato['lugarFecha'] )?></p>						
							</div>							
						</div>								
					</td>	
				</tr>
				<?php } elseif ($dato['plantilla'] == "d"){?>	
				<tr>
					<td>					
						<div class="info-noticia">
							<div class="noticia-desc-img-der">
								<a href="#" class="der ver-noticia" title="<?php echo $dato['idNoticia'] ?>">
									<img alt="" src="<?php echo $dato['thumbRuta'] ?>" style="width:<?php echo $dato['thumbAncho'] ?>px; height:<?php echo $dato['thumbAltura'] ?>px;" class="der imgshadow" title=""/>
								</a>
							</div>
							<div class="noticia-desc">
								<a class="ver-noticia titulo" href="#" title="<?php echo $dato['idNoticia'] ?>"> <?php echo $dato['titulo'] ?></a><br><br>
								<a class="ver-noticia descripcion" href="#" title="<?php echo $dato['idNoticia'] ?>"> <?php echo $dato['descripcion'] ?></a><br>
								<p class="fecha"> <?php echo htmlentities( $dato['lugarFecha'] )?></p>							
							</div>							
						</div>																
					</td>																							
				</tr>								
				<?php } else {?>	
				<tr>
				   <td>No hay registros para mostrar</td>
				</tr>
	    		<?php 
				}
	    	}			
			?>			
		</table>
		<?php 
		}			
	break;		

	case 2:
		$datos = $objNoticia->selectNoticia($_GET['idNoticia']);	
		if (($datos["plantilla"]  == "") || ($datos["plantilla"]  == " ") || ($datos["plantilla"]  == "c")){?>		
			<section class="noticia-izq">
			<p class="noticia-titulo"><?php echo $datos["titulo"]?></p><br/>			
			<?php  if($datos["plantilla"]  == "c"){?>
				<img src="<?php echo $datos["imgRuta"]?>" style="width:<?php echo $datos["imgAncho"]?>px; height:<?php echo $datos["imgAlto"]?>px;" class="noticia-img" />
				<?php  if($datos["imgDescripcion"] <> ""){?>
					<div class="noticia-img-desc" style="width: <?php echo $datos["imgAncho"]?>px">
						<p><?php echo $datos["imgDescripcion"]?></p>
					</div>	
				<?php }
			}
			?>
			<p><?php echo $datos["descripcion"]?></p><br>			
			<p class="fecha"><?php echo htmlentities($datos["lugarFecha"])?></p><br/>
			</section>			
			<section class="noticia-der">			
				<div class="blue-top">
					<img src="img/content/crn-tl-blue.gif" alt="" class="crn-tl-blue" />
					<img src="img/content/crn-tr-blue.gif" alt="" class="crn-tr-blue" />
				</div>
				<div class="blue-content ">
					<div id="boxcontrol" class="boxcontrol">
						<table id="tabla-tips">
							<thead>
								<tr>
									<td colspan="4">TIPS DE SALUD</td>
								</tr>
							</thead>					
							<tr>
								<td><a href="#" class="tip-salud-embarazada"><img src="img/tip-salud/embarazada.png" alt="�Estas Embarazada?" /></a></td>
								<td><a href="#" class="tip-salud-embarazada">Embarazada</a></td>
								<td><a href="#" class="tip-salud-diabetico"><img src="img/tip-salud/diabetico.png" alt="�Eres Diabetico?" /></a></td>
								<td><a href="#" class="tip-salud-diabetico">Diabetico</a></td>
							</tr>
							<tr>
								<td><a href="#" class="tip-salud-hipertenso"><img src="img/tip-salud/hipertenso.png" alt="�Estas Hipertenso?" /></a></td>
								<td><a href="#" class="tip-salud-hipertenso">Hipertenso</a></td>
								<td><a href="#" class="tip-salud-tosedor"><img src="img/tip-salud/tosedor.png" alt="�Eres Tosedor Cronico?" /></a></td>
								<td><a href="#" class="tip-salud-tosedor">Tosedor Cr&oacute;nico</a></td>
							</tr>
							<tr>
								<td><a href="#" class="tip-salud-sida"><img src="img/tip-salud/sida.png" alt="�Quien puede tener SIDA?" /></a></td>
								<td><a href="#" class="tip-salud-sida">SIDA</a></td>
								<td><a href="#" class="tip-salud-violencia"><img src="img/tip-salud/violencia.png" alt="�Sufres de Violencia Intrafamiliar?" /></a></td>
								<td><a href="#" class="tip-salud-violencia">Violencia Intrafamiliar</a></td>
							</tr>			
							<tr>
								<td><a href="#" class="tip-salud-diarrea"><img src="img/tip-salud/diarrea.png" alt="�Tu hijo tiene Diarrea?" /></a></td>
								<td><a href="#" class="tip-salud-diarrea">Diarrea</a></td>
								<td><a href="#" class="tip-salud-neumonia"><img src="img/tip-salud/neumonia.png" alt="Neumonia" /></a></td>
								<td><a href="#" class="tip-salud-neumonia">Neumonia</a></td>
							</tr>
							<tr>
								<td><a href="#" class="tip-salud-vacunas"><img src="img/tip-salud/vacunas.png" alt="Vacunas" /></a></td>
								<td><a href="#" class="tip-salud-vacunas">Vacunas</a></td>
								<td><a href="#" class="tip-salud-dengue"><img src="img/tip-salud/dengue.png" alt="Dengue" /></a></td>
								<td><a href="#" class="tip-salud-dengue">Dengue</a></td>
							</tr>									
							<tr>
								<td><a href="#" class="tip-salud-planificacion"><img src="img/tip-salud/planificacion.png" alt="Planificacion Familiar" /></a></td>
								<td><a href="#" class="tip-salud-planificacion">Planificaci&oacute;n Familiar</a></td>
								<td><a href="#" class="tip-salud-recien-nacido"><img src="img/tip-salud/recien-nacido.png" alt="Hipotiroidismo (Recien Nacido)" /></a></td>
								<td><a href="#" class="tip-salud-recien-nacido">Hipotiroidismo (Reci&eacute;n Nacido)</a></td>
							</tr>						
						</table>	
					</div>
				</div>
				<div class="blue-btm">
					<img src="img/content/crn-bl-blue.gif" alt="" class="crn-bl-blue" />
					<img src="img/content/crn-br-blue.gif" alt="" class="crn-br-blue" />
				</div>	
			</section>
		<?php 
		} elseif ($datos["plantilla"] == "i") {?>			
			<div class="img-izq">
				<img src="<?php echo $datos["imgRuta"]?>" style="width:<?php echo $datos["imgAncho"]?>px; height:<?php echo $datos["imgAlto"]?>px;" class="noticia-img" />			
				<?php  if($datos["imgDescripcion"] <> ""){?>
					<div class="noticia-img-desc" style="width: <?php echo $datos["imgAncho"]?>px">
						<p><?php echo $datos["imgDescripcion"]?></p>
					</div>	
				<?php }?>				
			 </div>		
			<p class="noticia-titulo"><?php echo $datos["titulo"]?></p><br/>			
			<p><?php echo $datos["descripcion"]?></p><br>		
			<p class="fecha"><?php echo htmlentities($datos["lugarFecha"])?></p><br/>		
		<?php 
		} elseif ($datos["plantilla"] == "d") {?>
			<div class="img-der">
				<img src="<?php echo $datos["imgRuta"]?>" style="width:<?php echo $datos["imgAncho"]?>px; height:<?php echo $datos["imgAlto"]?>px;" class="noticia-img" />			
				<?php  if($datos["imgDescripcion"] <> ""){?>
					<div class="noticia-img-desc" style="width: <?php echo $datos["imgAncho"]?>px">
						<p><?php echo $datos["imgDescripcion"]?></p>
					</div>	
				<?php }?>				
			</div>		
			<p class="noticia-titulo"><?php echo $datos["titulo"]?></p><br/>			
			<p><?php echo $datos["descripcion"]?></p><br>		
			<p class="fecha"><?php echo htmlentities($datos["lugarFecha"])?></p><br/>
		<?php } else { 
		?>
			<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width:240px; float: right;" > 
				<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				<strong>Nota!</strong></p><br/>
				<div >* No hay noticia para mostrar</div>
			</div>
		<?php 
		}
		 
    break;
	
    case 3:
?>



<?php
    break;
 
    case 4:
?>    	

<?php
    break;
    case 5:
?>
<?php    	
	break;    
    default:
    break;
?>			
<?php      
}