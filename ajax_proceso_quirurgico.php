<?php

include_once 'cls/clsCirugia.php';

$objCirugia = new Cirugia();

$type = $_GET['type'];

/**
 * Type 1: Muestra el lado izq. Login
 * Type 2: Muestra el lado der. Informacion
 */

$datos = array();

$folio = $_GET['folio'];
$date = $_GET['date'];

if (($folio == "") || ($date=="")){	
}else{	
	if(!isset($datos['estado'])){		
		$datos = $objCirugia->selectSolicitudCirugia($folio, $date);
	}	
}
?>

<script type="text/javascript">

$(function(){

	$( ".btn" ).button();

	$( ".date" ).datepicker(
		{
			changeMonth: true,
			changeYear: true
		}
	);
		
	$( ".date" ).datepicker( "option", {
		
		dateFormat: "dd/mm/yy"});

		var monthNamesShort = $( ".date" ).datepicker( "option", "monthNamesShort" );
		$( ".date" ).datepicker( "option", "monthNamesShort", ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'] );

		var monthNames = $( ".date-picker" ).datepicker( "option", "monthNames" );
		$( ".date" ).datepicker( "option", "monthNames", ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] );

		var dayNames = $( ".date-picker" ).datepicker( "option", "dayNames" );
		$( "date" ).datepicker( "option", "dayNames", ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'] );

		var dayNamesMin = $( ".fecha, .date-picker" ).datepicker( "option", "dayNamesMin" );
		$( ".date" ).datepicker( "option", "dayNamesMin", ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'] );

		var nextText = $( ".fecha, .date-picker" ).datepicker( "option", "nextText" );
		$( ".date" ).datepicker( "option", "nextText", 'Siguiente' );

		var prevText = $( ".fecha, .date-picker" ).datepicker( "option", "prevText" );
		$( ".date" ).datepicker( "option", "prevText", 'Anterior' );


		function updateTips( t ) {
			tips
			.text( t )
			//.addClass( "ui-state-highlight" );
			.addClass( "ui-state-error" );
			//	setTimeout(function() {
			//		tips.removeClass( "ui-state-highlight", 1500 );
			//	}, 500 );
		}

		function updateMessage( t ) {
			tips
			.text( t )
			.addClass( "ui-state-highlight" );
		}

		function isRequired( o, n ){
			if ( ! o.val() ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		}

		function checkLength( o, n, min, max ) {
			if ( o.val().length > max || o.val().length < min ) {
				o.addClass( "ui-state-error" );
				updateTips( "La longitud de " + n + " debe estar entre " +
				min + " y " + max + " caracteres." );
				return false;
			} else {
				return true;
			}
		}

		$( "#btn-enter" ).click(function() {
				
			var folio = $( "#txt-folio" ),
			dateSelected = $( "#txt-date-birth" );
				
			allFields = $( [] ).add( folio ).add( dateSelected ),

			tips = $( "#tip" );
			var bValid = true;

			bValid = bValid && isRequired( folio , "El Folio es requerido" );
			bValid = bValid && checkRegexp( folio, /^(?:\+|-)?\d+$/, "El folio solo acepta n�meros" );
			bValid = bValid && checkLength( folio , "La longitud del folio", 3, 5 );
			bValid = bValid && isRequired( dateSelected , "La fecha es requerida" );
			
			//bValid = bValid && checkRegexp( dateSelected, /^\d{2}-\d{2}-\d{4}$/, "Fecha incorrecta" );
			
			bValid = bValid && checkRegexp( dateSelected, /^([0-9][0-9])\/([0-9][0-9])\/([0-9][0-9][0-9][0-9])$/, "Fecha incorrecta" );				
			allFields.removeClass( "ui-state-error" );
				
			if ( bValid ) {

				updateTips( "" );
				tips.removeClass( "ui-state-error" );
				 
				$( "#pro-quirurgico-login").slideUp('fast', function(){
					var data1 = "type=1&folio=" + folio.val() + "&date=" + dateSelected.val();
					$(this).load( "../../ajax_proceso_quirurgico.php", data1, function(){
						$(this).slideDown( "fast" );
					});
				});

				$( "#pro-quirurgico-info").slideUp('fast', function(){
					var data2 = "type=2&folio=" + folio.val() + "&date=" + dateSelected.val();
					$(this).load( "../../ajax_proceso_quirurgico.php", data2, function(){
						$(this).slideDown( "fast" );
					});
				});				
			}
			return false;				
		});

		$( "#btn-exit" ).click(function() {
			
			var folio = $( "#txt-folio" ),
			dateSelected = $( "#txt-date-birth" );
				
			allFields = $( [] ).add( folio ).add( dateSelected ),

			tips = $( "#tip" );
				
			allFields.removeClass( "ui-state-error" );
			updateTips( "" );
			tips.removeClass( "ui-state-error" );
			 
			$( "#pro-quirurgico-login").slideUp('fast', function(){
				var data1 = "type=1";
				$(this).load( "../../ajax_proceso_quirurgico.php", data1, function(){
					$(this).slideDown( "fast" );
				});
			});
				
			$( "#pro-quirurgico-info").slideUp('fast', function(){
				var data2 = "type=2";
				$(this).load( "../../ajax_proceso_quirurgico.php", data2, function(){
					$(this).slideDown( "fast" );
				});
			});
			
			return false;
				
		});
		
	});
</script>	

<?php 

switch ($type) {
    case 1:    	
		if (($folio == "") || ($date=="") || ($datos['estado']=='I')) 
    	{?>
    	
			<div class="ui-state-highlight ui-corner-all"
				style="margin-top: 10px; padding: 0.7em; font-size: 95%; width: 190px;">
				<p>
					<span class="ui-icon ui-icon-info"style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
				</p>
				<div>
					* Ingrese el n&uacute;mero de folio
					<p style="margin-left: 1.6em;">* Seleccione la fecha de nacimiento.</p>
				</div>
			</div>	    	    	
	    	<div class="ui-widget-content" style="margin-top: 10px; padding: 10px; width: 185px;" >		
				<p id="tip" class="validateTips ui-corner-all" style='padding: 0 .7em; font-size: 95%; width:170px;'></p><br>			
				<?php 
				if ($datos['estado']=='I'){		
					echo " 
						<div class='ui-widget'>
							<div class='ui-state-error ui-corner-all' style='padding: 0 .7em; font-size: 95%; width:170px;'> 
								<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span> 
								<strong>Alerta:</strong> El folio y la fecha no coinciden.</p>
							</div>
						</div>
						<br/>								
					";
				}
				?>
				<p>Folio</p>			
				<input type="number" placeholder="folio" id="txt-folio" name="txt-folio" maxlength="4" style="width: 175px;">
				<br/><br/>
				<p>Fecha de nacimiento</p>
				<input name="txt-date-birth" type="text" id="txt-date-birth" maxlength="10" class="date" style="width:175px;" /> 
				<br/><br/>					
				<input type="button" id="btn-enter" name="btn-entrar" value="Entrar" class="btn" />		
				<br><br>
	    	</div>

		<?php
    	}else{
		?>
			<br><br>
			<div class="ui-state-highlight ui-corner-all"
				style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 185px;">
				<p>
					<span class="ui-icon ui-icon-document"style="float: left; margin-right: .3em;"></span> 
					<strong>Folio y fecha correcto!</strong>
				</p>
			</div>
			<br><br>
			<input type="button" id="btn-exit" name="btn-exit" value="Salir" class="btn" />
			    		
		<?php
    	}
    break;
    case 2:      	
    	if (($folio == "") || ($date=="") || ($datos['estado']=='I'))
    	{
    	?>
			<div class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0.7em; font-size: 95%; width: 490px;">
				<p>
					<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
				</p>
				<div>
					* Localice el n&uacute;mero de folio en la parte superior de su <strong>Solicitud de Cirug&iacute;a</strong>
				</div>
				<div>
					<p style="margin-left: 1.6em;">
						* En este ejemplo el folio se encuentra marcado en rojo 
						<strong style="color: red;">* 216 *</strong>
					</p>
				</div>
			</div>
			<br />
			<img alt="Solicitud de Cirugia" src="../../img/servicios/cirugia/solicitud-cirugia.png" class="imgshadow" style="width: 495px;">

    	<?php     		
    	}else{
    		if($datos['estado'] == "P")
    		{    			
	    		?>
				<br>
				<table id="tabla-pro-quirurgico">
					<tr>
						<th colspan="2"></th>
					</tr>
					<tr>
						<td>Folio:</td>
						<td><?php echo $folio ?></td>
					</tr>
					<tr>
						<td>Fecha:</td>
						<td><?php echo htmlentities($datos['fecha']); ?></td>
					</tr>
					<tr>
						<td>Turno:</td>
						<td><?php echo $datos['turno']; ?></td>
					</tr>
					<tr>
						<td>Paciente:</td>
						<td><?php echo htmlentities($datos['paciente']); ?></td>
					</tr>
					<tr>
						<td>Doctor:</td>
						<td><?php echo htmlentities($datos['medico']); ?></td>
					</tr>
					<tr>
						<td>Derechohabiencia:</td>
						<td><?php echo $datos['derecho']; ?></td>
					</tr>
					<tr>
						<td>Sala:</td>
						<td><?php echo $datos['sala']; ?></td>
					</tr>
					<tr>
						<td>Ambulatoria:</td>
						<td><?php echo $datos['ambulatoria']; ?></td>
					</tr>
					<?php 
				  	if(!$datos['unidadSangre'] == 0)
					  	echo "<tr>";
					  		echo"<td>Unidades de sangre:</td>";
					  		echo"<td>" . $datos['unidadSangre'] . " - " ; 
							  	if ($datos['VoBoBDS'] == "Pendiente"){
							  		echo "<strong style='color: red'>" . $datos['VoBoBDS'] . "</strong>";
							  	}else{
							  		echo $datos['VoBoBDS'];
							  	}							  				  			
					  		echo"</td>";				
						echo "</tr>";
					?>	 	  	  	  
				</table>
				<br>
				<br>
				<div class="ui-state-highlight ui-corner-all"
					style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 490px;">
					<p>
						<span class="ui-icon ui-icon-info"
							style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
					</p>
					<div>
						* Su solicitud se programo con &eacute;xito
						<p style="margin-left: 1.6em;">* Es importante avisar si se podra
							asistir a la cirug&iacute;a</p>
						<p style="margin-left: 1.6em;">
							* Conmutador: <strong>612-175-0500</strong>
						</p>
					</div>
				</div>
				
			<?php
			}elseif ($datos['estado'] == "A")  {		
				?>
				<table id="tabla-pro-quirurgico">
					<tr>
						<th colspan="2"></th>
					</tr>
					<tr>
						<td>Folio:</td>
						<td><?php echo $folio ?></td>
					</tr>
					<tr>
						<td>Fecha tentativa:</td>
						<td><?php echo htmlentities($datos['fecha']); ?></td>
					</tr>
					<tr>
						<td>Turno:</td>
						<td><?php echo $datos['turno']; ?></td>
					</tr>
					<tr>
						<td>Paciente:</td>
						<td><?php echo htmlentities($datos['paciente']); ?></td>
					</tr>
					<tr>
						<td>Doctor:</td>
						<td><?php echo htmlentities($datos['medico']); ?></td>
					</tr>
					<tr>
						<td>Derechohabiencia:</td>
						<td><?php echo $datos['derecho']; ?></td>
					</tr>
					<tr>
						<td>Sala:</td>
						<td><?php echo $datos['sala']; ?></td>
					</tr>
					<tr>
						<td>Ambulatoria:</td>
						<td><?php echo $datos['ambulatoria']; ?></td>
					</tr>
				  	<?php 
				  	if(!$datos['unidadSangre'] == 0)
					  	echo "<tr>";
					  		echo"<td>Unidades de sangre:</td>";
					  		echo"<td>" . $datos['unidadSangre'] . " - " ; 
							  	if ($datos['VoBoBDS'] == "Pendiente"){
							  		echo "<strong style='color: red'>" . $datos['VoBoBDS'] . "</strong>";
							  	}else{
							  		echo $datos['VoBoBDS'];
							  	}							  				  			
					  		echo"</td>";				
					  	echo "</tr>";
					?>	  	  	  
				</table>
				<br />

				<?php 			
				if($datos['dias'] <= 0) {?>
					<div class="ui-state-highlight ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 490px;">
						<p>
							<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
						</p>
						<div>* La solicitud no fue programada.</div>
					</div>										
				<?php 
				}				
				elseif (($datos['dias'] > 0) &&  ($datos['dias'] <= 7)) {?>
					<div class="ui-state-highlight ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 490px;">
						<p>
							<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
						</p>
						<div>
							<p style="margin-left: 1.6em;">* La solicitud no fue programada.</p>
						</div>
						<div>
							<p style="margin-left: 1.6em;">* Favor de contactar a su m&eacute;dico.</p>
						</div>
					</div>
					<br />
															
				<?php 
				}elseif ($datos['dias'] > 7) {?>
					<div class="ui-state-highlight ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 490px;">
						<p>
							<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
						</p>
						<div>
							* Consulte nuevamente a partir del d&iacute;a <strong
								style="color: red"> <?php echo htmlentities($datos['fecha'])?> </strong>
						</div>
						<div>
							<p style="margin-left: 1.6em;">* Recuerde que la programaci&oacute;n
								de cirugias se realiza el jueves previo a la misma</p>
						</div>
					</div>
					<br />
								
				<?php										
				}						
			}elseif ($datos['estado'] == "C") {?>
				<br>
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width: 490px;">
					<p>
						<span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
					</p>
					<div>* Su solicitud de cirugia ha sido cancelada</div>
					<div>
						<p style="margin-left: 1.6em;">
							* Para aclaraciones comuniquese al <strong>612 17 5 05 00</strong>
						</p>
					</div>
				</div>
				<br />
				<img alt="Solicitud de Cirugia" src="../../img/servicios/cirugia/solicitud-cirugia.png">
	    		
			<?php			
			}   		
    	}
    break;
    case 3:   
?>

<?php    	
	break;    
    default:
    break;
?>			
<?php      
}