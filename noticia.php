<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$nombre_archivo = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$nombre_archivo";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>	
		<script src="js/noticias.js"></script>     
		<script src="js/tip-salud.js"></script>	
		<script src="js/menu.js"></script>
		<script>					
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>           
		<div id="wrapper">	
		<div id="modal-tip"></div>		
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>	
			<section id="content" style="padding-top: 0px">																
			</section>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>