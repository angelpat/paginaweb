<?php 
include_once '../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../css/master.css" />
		<link rel="stylesheet" href="../css/menu.css"/>				
		<link rel="stylesheet" href="../galleriffic/css/galleriffic-3.css" type="text/css" />				
		<script src="../js/jquery-1.6.2.min.js"></script>
		<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../js/menu.js"></script>    
		<script src="../js/servicio-consulta-externa.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.history.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.opacityrollover.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '290px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 14,
					preloadAhead:              14,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            5,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Carrusel',
					pauseLinkText:             'Pause Carrusel',
					prevLinkText:              '&lsaquo; Anterior Imagen',
					nextLinkText:              'Siguiente Imagen &rsaquo;',
					nextPageLinkText:          'Sig &rsaquo;',
					prevPageLinkText:          '&lsaquo; Ant',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;
					
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("1");		
				$objCabecera->menu("../");			
			?>		
			<div id="content">
				<div class="barra-01" style="background-image: url('../img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="../img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center; font-size: 18px; padding: 4px;">				
					La Asociaci&oacute;n M&eacute;dica del Hospital realizo <br>reconocimiento a la trayectoria de dos grandes m&eacute;dicos
					</p><br/>	
				</div><br>		
				<div id="container">
					<!-- Start Advanced Gallery Html Containers -->
					<div id="gallery" class="content ">
						<div id="controls" class="controls"></div>
						<div class="slideshow-container">
							<div id="loading" class="loader"></div>
							<div id="slideshow" class="slideshow"></div>
						</div>
						<div id="caption" class="caption-container"></div>
					</div>
					<div id="thumbs" class="navigation">
						<ul class="thumbs noscript">
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-01.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-01-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Presidium</div>
									<div class="image-desc">El Director del Hospital Juan Mar&iacute;a de Salvatierra el Dr. Carlos Arriola Isais, en representaci&oacute;n del Secretario de Salud Dr. Santiago Alan Cervantes el Dr. Mario Garc&iacute;a Isais, Comisionado Estatal para la Protecci&oacute;n Contra Riesgos Sanitarios, el Presidente de la Asociaci&oacute;n Medica el Dr. Andrik Cruz Morales y los invitados de honor el Dr. Juan Manuel Cota Abaroa y el Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-02.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-02-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Presentaci&oacute;n e inicio de ceremonia</div>
									<div class="image-desc">El Dr. Miguel Liera Ba&ntilde;uelos fue el encargado de la presentaci&oacute;n de los invitados y la ceremonia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-03.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-03-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Asistentes</div>
									<div class="image-desc">Hubo invitados del Sector Salud del estado, familiares de los m&eacute;dicos homenajeados y personal del Hospital Juan Mar&iacute;a de Salvatierra</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-04.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-04-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Presentaci&oacute;n</div>
									<div class="image-desc">Presentaci&oacute;n en el presidio del Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-05.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-05-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Presentaci&oacute;n</div>
									<div class="image-desc">Presentaci&oacute;n en el presidio Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez </div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-06.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-06-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Semblanza en honor al Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
									<div class="image-desc">El Dr. Victor Fenech Bare&ntilde;o fue el encargado de presentar la semblanza en honor al Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-07.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-07-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Semblanza en honor al Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
									<div class="image-desc">Inicio de la presentaci&oacute;n de la semblanza de Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-08.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-08-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Asistentes y homenajeados</div>
									<div class="image-desc">P&uacute;blico y homenajeados atentos a la rese&ntilde;a del Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>	
							<!-- 
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-09.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-09-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Titulo</div>
									<div class="image-desc">Descripcion 01</div>
								</div>
							</li> -->
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-10.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-10-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Entrega de placa de reconocimiento</div>
									<div class="image-desc">Entrega de una placa de reconocimiento y una pluma por parte del el Presidente de la Asociaci&oacute;n Medica el Dr. Andrik Cruz Morales y la Secretaria de la asociaci&oacute;n la Q.F.B. Mar&iacute;a Maricela Zaragoza N&uacute;&ntilde;ez</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-11.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-11-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Agradecimientos</div>
									<div class="image-desc">El Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez muy emotivo dio sus palabras de agradecimiento al hospital, a sus compa&ntilde;eros, a personal que ha trabajado directamente con &eacute;l y a su familia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-12.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-12-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Un gran d&iacute;a</div>
									<div class="image-desc">Un d&iacute;a muy triste para el hospital por ser el &uacute;ltimo d&iacute;a laboral de los festejados y el inicio de una nueva etapa para los homenajeados</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-13.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-13-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Semblanza en honor al Dr. Juan Manuel Cota Abaroa</div>
									<div class="image-desc">El Dr. Mercado Castro Pedro fue el encargado de presentar la semblanza en honor al Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>
						<!-- 	<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-14.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-14-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Titulo</div>
									<div class="image-desc">Descripcion 01</div>
								</div>
							</li>-->
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-15.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-15-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Asistentes</div>
									<div class="image-desc">La ceremonia de reconocimiento fue muy concurrida y emotiva para todos los asistentes</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-16.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-16-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Entrega placa de reconocimiento</div>
									<div class="image-desc">El Dr. Juan Manuel Cota Abaroa recibi&oacute; una placa de reconocimiento y una pluma por parte del Presidente de la Asociaci&oacute;n Medica el Dr. Andrik Cruz Morales y la Secretaria de la asociaci&oacute;n la Q.F.B. Mar&iacute;a Maricela Zaragoza N&uacute;&ntilde;ez</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-17.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-17-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Agradecimientos</div>
									<div class="image-desc">El Dr. Juan Manuel Cota Abaroa agradeci&oacute; a su profesi&oacute;n y a su familia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-18.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-18-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Clausura</div>
									<div class="image-desc">Mensaje del Dr. Mario Garc&iacute;a Isais hacia a los homenajeados y clausura del evento</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-19.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-19-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Aplausos</div>
									<div class="image-desc">Aplausos y clausura del evento</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-20.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-20-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Felicitaciones</div>
									<div class="image-desc">Amigos, compa&ntilde;eros y familiares se acercaron a felicitar al Dr. Juan Manuel Cota Abaroa una vez finalizado el evento</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-21.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-21-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Felicitaciones</div>
									<div class="image-desc">Amigos, compa&ntilde;eros y familiares se acercaron a felicitar al Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez una vez finalizado el evento</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-22.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-22-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Felicitaciones</div>
									<div class="image-desc">Felicitaciones al Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-23.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-23-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Dr. Juan Manuel Cota Abaroa y familia</div>
									<div class="image-desc">Familia del Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-24.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-24-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez y dos grandes compa&ntilde;eras</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-25.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-25-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Entrevista</div>
									<div class="image-desc">Entrevista de los medios hacia el Dr. Carlos Arriola Isais, Director del Benem&eacute;rito Hospital General con Especialidades Juan Mar&iacute;a de Salvatierra</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-26.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-26-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Compa&ntilde;eros</div>
									<div class="image-desc">Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez y compa&ntilde;eros del hospital</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-27.JPG" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-27-thumb.JPG" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Compa&ntilde;eros</div>
									<div class="image-desc">Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez y compa&ntilde;eros</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.10.24/img-28.jpg" title="Reconocimiento">
									<img src="../img/galeria/2012.10.24/img-28-thumb.jpg" alt="Reconocimiento" />
								</a>
								<div class="caption">
									<div class="image-title">Homenajeados</div>
									<div class="image-desc">Los homenajeados el Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez y el Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>																																											
						</ul>
					</div>
					<!-- End Advanced Gallery Html Containers -->
					<div style="clear: both;"></div>
				</div>
			</div>						
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>