<?php 
include_once '../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../css/master.css" />
		<link rel="stylesheet" href="../css/menu.css"/>				
		<link rel="stylesheet" href="../galleriffic/css/galleriffic-3.css" type="text/css" />				
		<script src="../js/jquery-1.6.2.min.js"></script>
		<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../js/menu.js"></script>    
		<script src="../js/servicio-consulta-externa.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.history.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.opacityrollover.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '290px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 10,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            5,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Carrusel',
					pauseLinkText:             'Pause Carrusel',
					prevLinkText:              '&lsaquo; Anterior Imagen',
					nextLinkText:              'Siguiente Imagen &rsaquo;',
					nextPageLinkText:          'Sig &rsaquo;',
					prevPageLinkText:          '&lsaquo; Ant',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;
					
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("1");		
				$objCabecera->menu("../");			
			?>		
			<div id="content">
				<div class="barra-01" style="background-image: url('../img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="../img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">Jornadas Prot&eacute;sicas - Abril 2014</p><br/>	
				</div><br>		
				<div id="container">
					<!-- Start Advanced Gallery Html Containers -->
					<div id="gallery" class="content ">
						<div id="controls" class="controls"></div>
						<div class="slideshow-container">
							<div id="loading" class="loader"></div>
							<div id="slideshow" class="slideshow"></div>
						</div>
						<div id="caption" class="caption-container"></div>
					</div>
					<div id="thumbs" class="navigation">
						<ul class="thumbs noscript">
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-01.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-01-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title">Ceremonia de clausura de las I Jornadas Prot&eacute;sicas</div>
									<div class="image-desc">				
									 En la sala de ense&ntilde;anza del Hospital Salvatierra se realiz&oacute; la clausura de las I Jornadas Prot&eacute;sicas realizadas del 28 al 30 de abril.
									</div>
								</div>
							</li>						
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-02.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-02-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title">Pres&iacute;dium</div>
									<div class="image-desc">				
									 Lic. Mar&iacute;a del Socorro Garc&iacute;a Quiroz; Directora de la Administraci&oacute;n del Patrimonio de la Beneficencia P&uacute;blica
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-03.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-03-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									 Mensaje de la Directora de la Administraci&oacute;n del Patrimonio de la Beneficencia P&uacute;blica que fue la que dono las pr&oacute;tesis
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-04.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-04-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									 Entrega de reconocimientos al personal que participo en las jornadas
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-05.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-05-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									 Mensaje y clausura de las Jornadas Prot&eacute;sicas por el Dr. Mario
									</div>
								</div>
							</li>	
							
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-06.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-06-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									Equipo del Hospital que participo en las Jornadas Prot&eacute;sicas
									</div>
								</div>
							</li>							
							
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-07.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-07-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title">Se operaron 9 pacientes</div>
									<div class="image-desc">				
									 -Pr&oacute;tesis de rodilla<br>
									 -Pr&oacute;tesis de cadera
									
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-08.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-08-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title">Grupo de m&eacute;dicos de ortotrauma que realizaron las cirug&iacute;as</div>
									<div class="image-desc">				
									 Dr. Rafael Carrillo Jim&eacute;nez, Dr. Luis &aacute;ngel de la Toba Fonseca
									
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-09.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-09-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									 La evoluci&oacute;n de los pacientes fue satisfactoria
									</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2014.04.30/img-10.JPG" title="Jornadas Prot&eacute;sicas ">
									<img src="../img/galeria/2014.04.30/img-10-thumb.JPG" alt="Jornadas Prot&eacute;sicas" />
								</a>
								<div class="caption">
									<div class="image-title"></div>
									<div class="image-desc">				
									 La Directora de la Administraci&oacute;n del Patrimonio de la Beneficencia P&uacute;blica visito a los beneficiados
									</div>
								</div>
							</li>																																											
																						
							
						</ul>
					</div>
					<!-- End Advanced Gallery Html Containers -->
					<div style="clear: both;"></div>
				</div>
			</div>						
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>