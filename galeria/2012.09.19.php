<?php 
include_once '../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../css/master.css" />
		<link rel="stylesheet" href="../css/menu.css"/>				
		<link rel="stylesheet" href="../galleriffic/css/galleriffic-3.css" type="text/css" />				
		<script src="../js/jquery-1.6.2.min.js"></script>
		<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../js/menu.js"></script>    
		<script src="../js/servicio-consulta-externa.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.history.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.opacityrollover.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '250px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 10,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            5,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Carrusel',
					pauseLinkText:             'Pause Carrusel',
					prevLinkText:              '&lsaquo; Anterior Imagen',
					nextLinkText:              'Siguiente Imagen &rsaquo;',
					nextPageLinkText:          'Sig &rsaquo;',
					prevPageLinkText:          '&lsaquo; Ant',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;
					
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("1");		
				$objCabecera->menu("../");			
			?>	
			<div id="content">
				<div class="barra-01" style="background-image: url('../img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="../img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">Inauguraci&oacute;n de Cl&iacute;nica de Cat&eacute;teres Vasculares</p><br/>	
				</div><br>		
				<div id="container">
					<!-- Start Advanced Gallery Html Containers -->
					<div id="gallery" class="content ">
						<div id="controls" class="controls"></div>
						<div class="slideshow-container">
							<div id="loading" class="loader"></div>
							<div id="slideshow" class="slideshow"></div>
						</div>
						<div id="caption" class="caption-container"></div>
					</div>
					<div id="thumbs" class="navigation">
						<ul class="thumbs noscript">
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-24.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-24-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Las autoridades nacionales, estatales y locales.</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-14.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-14-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">La inauguraci&oacute;n se realizo en el aula de ense&ntilde;anza del Benemerito Hospital General con Especialidades &quot;Juan Mar&iacute;a de Salvatierra&quot;  </div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-28.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-28-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Asistencia de representantes del Sector Salud y personal de Hospital Salvatierra</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-23.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-23-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">En la ceremonia hubo la participaci&oacute;n de la Esc. de Enfermer&iacute;a del CONALEP entonando el &quot;Himno a la enfermera&quot;</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-25.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-25-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">La Maestra Teresa Camacho Barajas dio un mensaje alusivo a la Cl&iacute;nica de Cat&eacute;teres (Coordinadora Estatal de Enfermer&iacute;a del Estado de Baja California Sur)</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-32.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-32-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">La inauguraci&oacute;n de las diferentes Cl&iacute;nicas de Cat&eacute;teres del Estado por parte del Dr. Ruben Preza Castro en 
															representeci&oacute;n del Secretario de Salud Dr. Santiago Alan Cervantes Aldama.</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-34.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-34-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">La ceremonia fue muy emotiva y hubo mucha participaci&oacute;n</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-17.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-17-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Representaci&oacute;n simbolica de las cl&iacute;nicas cat&eacute;teres del Sector Salud de B.C.S que iniciaron sus actividades, en apoyo al programa.<strong>Bacteremia Cero</strong></div>
								</div>
							</li>
					<!-- 		<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-18.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-18-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Cl&iacute;nica de Cat&eacute;teres del Hospital de Cd. Constituci&oacute;n </div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-19.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-19-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Cl&iacute;nica de Cat&eacute;teres del Hospital de Cabo San Lucas</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-20.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-20-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Cl&iacute;nica de Cat&eacute;teres del ISSSTE</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-21.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-21-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Centro estatal de oncolog&iacute;a</div>
								</div>
							</li>
							 
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-22.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-22-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Cl&iacute;nica de Cat&eacute;teres del Hospital de San Jos&eacute; del Cabo</div>
								</div>
							</li>-->
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-37.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-37-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Inauguraci&oacute;n simbolica de la cl&iacute;nica de cat&eacute;teres del Hospital de Cd. Constituci&oacute;n </div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-38.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-38-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Inauguraci&oacute;n simbolica de la cl&iacute;nica de cat&eacute;teres del Hospital de Cabo San Lucas </div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-39.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-39-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Inauguraci&oacute;n simbolica de la cl&iacute;nica de cat&eacute;teres del ISSSTE</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-40.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-40-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Inauguraci&oacute;n simbolica de la cl&iacute;nica de cat&eacute;teres del Hospital de San Jos&eacute; del Cabo  </div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-46.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-46-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Las &aacute;reas involucradas en la coordinaci&oacute;n del evento fueron el Servicio de 
									Enfermer&iacute;a (L.E. Saul Edgar S&aacute;nchez y E.G. Judith G&oacute;mez Mendoza) y la Coordinaci&oacute;n 
									de Innovaci&oacute;n Calidad. Tambi&eacute;n asistieron representantes del Seguro Social.</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-54.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-54-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">Entrega de kit de capacitaci&oacute;n por las autoridades a cada una de las cl&iacute;nicas</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.09.19/img-47.jpg" title="Cl&iacute;nica de Cat&eacute;teres Vasculares">
									<img src="../img/galeria/2012.09.19/img-47-thumb.jpg" alt="Cl&iacute;nica de Cat&eacute;teres Vasculares" />
								</a>
								<div class="caption">
									<div class="image-title">Inauguraci&oacute;n Cl&iacute;nica de Cat&eacute;teres Vasculares</div>
									<div class="image-desc">La cl&iacute;nica del Hospital Salvatierra esta bien equipada con todos los insumos necesarios para su funcionamiento</div>
								</div>
							</li>
						</ul>
					</div>
					<!-- End Advanced Gallery Html Containers -->
					<div style="clear: both;"></div>
				</div>
			</div>						
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>