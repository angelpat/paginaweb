<?php 
include_once '../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../css/master.css" />
		<link rel="stylesheet" href="../css/menu.css"/>				
		<link rel="stylesheet" href="../galleriffic/css/galleriffic-3.css" type="text/css" />				
		<script src="../js/jquery-1.6.2.min.js"></script>
		<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../js/menu.js"></script>    
		<script src="../js/servicio-consulta-externa.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.history.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="../galleriffic/js/jquery.opacityrollover.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '290px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 10,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            5,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Carrusel',
					pauseLinkText:             'Pause Carrusel',
					prevLinkText:              '&lsaquo; Anterior Imagen',
					nextLinkText:              'Siguiente Imagen &rsaquo;',
					nextPageLinkText:          'Sig &rsaquo;',
					prevPageLinkText:          '&lsaquo; Ant',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;
					
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("1");		
				$objCabecera->menu("../");			
			?>		
			<div id="content">
				<div class="barra-01" style="background-image: url('../img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="../img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">D&iacute;a del Padre Salvatierra</p><br/>	
				</div><br>		
				<div id="container" style="min-height: 660px;">
					<!-- Start Advanced Gallery Html Containers -->
					<div id="gallery" class="content ">
						<div id="controls" class="controls"></div>
						<div class="slideshow-container">
							<div id="loading" class="loader"></div>
							<div id="slideshow" class="slideshow"></div>
						</div>
						<div id="caption" class="caption-container"></div>
					</div>
					<div id="thumbs" class="navigation">
						<ul class="thumbs noscript">
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-01.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-01-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Padre Juan Mar&iacute;a de Salvatierra</div>
									
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-02.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-02-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Presentaci&oacute;n del presidium</div>
									<div class="image-desc">Dr. Luis Cardoza L&oacute;pez, Subdirector M&eacute;dico y Maestro de Ceremonias</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-03.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-03-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Presidium</div>
									<div class="image-desc">
									Lic. Victor Nolasco Cota Administrador del Hospital, 
									Esthela de Jes&uacute;s Ponce Beltr&aacute;n Presidenta Municipal de La Paz, B.C.S.,
									Lic. Valent&iacute;n Moreno Soria Oficial Mayor, 
									Dr. Rub&eacute;n Preza Castro Director de Servicios de Salud en el Estado de Baja California Sur, 
									Dip. Ram&oacute;n Alvarado Higuera,
									Dr. Carlos Arriola Isais Director del Hospital, 
									Dr. Miguel Liera Ba&ntilde;uelos representante de la Sociedad Medica de B.C.S.
									</div>
								</div>
							</li>														
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-04.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-04-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Asistentes</div>
									<div class="image-desc">Invitados de honor, presidium e invitados en general</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-05.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-05-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Asistencia</div>
									<div class="image-desc">El evento fue muy emotivo</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-06.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-06-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Rese&ntilde;a biogr&aacute;fica del Padre Salvatierra</div>
									<div class="image-desc">Dr. Carlos Arriola Isais Director del hospital</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-07.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-07-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Fin de la rese&ntilde;a</div>
									<div class="image-desc">Dr. Carlos Arriola Isais Director del Hospital, Dr. Luis Cardoza L&oacute;pez Subdirector del hospital</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-08.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-08-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Develaci&oacute;n de la placa biogr&aacute;fica del Padre Salvatierra</div>
									<div class="image-desc">La placa fue develada por el Dr. Rub&eacute;n Preza Castro Director de Servicios de Salud en el Estado de Baja California Sur y por el Dr. Carlos Arriola Isais Director del Hospital</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-09.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-09-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Develaci&oacute;n de la placa biogr&aacute;fica del Padre Salvatierra</div>
									
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-10.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-10-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Placa biogr&aacute;fica del Padre Salvatierra y Momento Musical</div>
									<div class="image-desc">La Danza Sagrada de la Opera Salvatierra<br>
										Profesor Luis Pela&aacute;s Director de la Escuela de M&uacute;sica
									</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-11.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-11-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Invitados de Honor</div>
									<div class="image-desc">Invitados de Honor, Dr. Juan Manuel Cota Abaroa y familia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-12.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-12-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Entrega de reconocimiento del premio Salvatierra a el presidium por parte de la Dra. Judith Arciniega Torres</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-13.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-13-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>														
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-14.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-14-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Juan Manuel Cota Abaroa</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-15.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-15-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-16.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-16-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-17.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-17-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-18.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-18-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Otorgamiento del premio Salvatierra</div>
									<div class="image-desc">Recibimiento del premio Salvatierra por parte del Dr. Miguel Mondrag&oacute;n Gonz&aacute;lez</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-19.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-19-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Clausura</div>
									<div class="image-desc">La clausura se llevo a cabo por parte del Dr. Rub&eacute;n Preza Castro Director de Servicios de Salud en el Estado de Baja California Sur</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../img/galeria/2012.11.15/img-20.JPG" title="JMS">
									<img src="../img/galeria/2012.11.15/img-20-thumb.JPG" alt="JMS" />
								</a>
								<div class="caption">
									<div class="image-title">Placa biogr&aacute;fica del Padre Salvatierra</div>
									<div class="image-desc">Biograf&iacute;a del Padre Salvatierra</div>
								</div>
							</li>	
																																																								
						</ul>
					</div>
					<!-- End Advanced Gallery Html Containers -->
					<div style="clear: both;"></div>
				</div>
			</div>						
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>