﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="tiposUsuario.aspx.vb" Inherits="Aton.tiposUsuario" %>

<%@ Register Src="registro.ascx" TagName="registro" TagPrefix="uc2" %>
<%@ Register Src="menu.ascx" TagName="menu" TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
              <asp:ImageButton ID="ImageButton1" runat="server" Height="30px" 
                    ImageUrl="../img/back.png" Width="30px" BorderWidth="0" Visible="False" />
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" ImageUrl="../img/addArea.png" Width="30px" BorderWidth="0" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="center" style="height: 20px">
            <h2>Catalógo de Tipos de Usuario</h2>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <br />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
                <asp:GridView ID="gvListado" runat="server" DataKeyNames="id" AutoGenerateColumns="False" AllowPaging="True" Width="90%" CellPadding="4" ForeColor="#333333" GridLines="None">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="false" CommandName="Editar" CommandArgument='<%# (ctype(Container,GridViewRow)).RowIndex %>'
                                    Height="24px" ImageUrl="../img/Edit_Icon2.png" Width="24px" ToolTip="Editar este registro" />
                            </ItemTemplate>
                            <FooterStyle Width="30px" />
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Id" HeaderText="C&#243;digo" >
                            <FooterStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Tipo de Usuario" >
                            <FooterStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            
                        </asp:BoundField>
                    </Columns>
                    <RowStyle BackColor="#E3EAEB" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                &nbsp; &nbsp;
                &nbsp; &nbsp;<br />
            </td>
        </tr>
    </table>
</div>
<div id="divModal" runat="server" visible="false"></div>
<div id="divControl" runat="server" visible="false">
    <uc2:registro id="Registro1" runat="server"></uc2:registro>
</div>
</asp:Content>