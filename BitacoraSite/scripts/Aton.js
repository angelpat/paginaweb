    function validaCaracter(e, validos) 
    {
        // Obtener el caracter a partir del codigo
        var caracter;
        caracter = String.fromCharCode(event.keyCode);
        // Pasar a munisculas
        caracter = caracter.toLowerCase();
        validos = validos.toLowerCase();

        var cadena;
        if (e.value != null) {
            cadena = e.value;
            if (cadena.indexOf(".") != -1 && caracter == ".") {
                return false;
            }
        }

        // Comparar caracteres introducidos, con los unicos que son validos
        if (validos.indexOf(caracter) != -1) {
            // Si se encuentra entre los caracteres validos
            return true;
        }
        else 
        {
            // Control keys, para que se permitan teclas como backspace, enter, tab y demas
            if (event.keyCode == null || event.keyCode == 0 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 27) {
                return true;
            }
            else 
            {
                // De lo contrario regresa false para que no escriba dicho caracter
                return false;
            }
        }

    }
		
    function fncInputNumericValuesOnly() 
	{
        if (!(event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) {
            event.returnValue = false;
        }
    }