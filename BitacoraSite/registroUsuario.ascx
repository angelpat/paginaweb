<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="registroUsuario.ascx.vb" Inherits="Aton.edicionUsuario" %>
<table border="0" cellpadding="0" cellspacing="0" width="400px">
    <tr style="height: 20px;">
        <td style="width: 20px; height: 20px; background: url(img/esqVentIzq.png)"></td>
        
        <td align="center" valign="middle" style="width: 360px; height: 20px; background: url(img/fondoTituloVent.png)" colspan="2">
            <asp:Label ID="lblTitulo" runat="server">Registro de Usuarios</asp:Label>
        </td>
        
        <td style="width: 20px; height: 20px; background: url(img/esqVentDer.png)"></td>
    </tr>
    <tr style="background-color: #BBD8E1">
        <td colspan="4" align="center" valign="middle">
            <asp:Label ID="lblNombre" runat="server"></asp:Label>
        </td>
    </tr> 
    <tr id="renglonTipo" runat="server" style="background-color: #BBD8E1">
        <td style="height: 30px" valign="middle"></td>
        
        <td align="right" style="height: 30px">
            <asp:Label ID="lblTipo" runat="server" Text="Tipo:"></asp:Label>
        </td>
        
        <td align="center" style="height: 30px">
            <asp:DropDownList ID="cmbTipo" runat="server" Width="90%">
            </asp:DropDownList>
        </td>
        
        <td style="height: 30px"></td>
    </tr>
    <tr id="renglonActivo" runat="server" style="background-color: #BBD8E1">
        <td style="height: 30px" valign="middle"></td>
        
        <td align="right">
            <asp:Label ID="lblActivo" runat="server" Text="Activo:"></asp:Label>
        </td>
        
        <td align="left">
            &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkActivo" runat="server" />
        </td>
        
        <td></td>
    </tr>
    <tr style="height: 38px; background-color: #BBD8E1">
        <td align="right" valign="middle" colspan="4">
            <asp:ImageButton ID="btnAceptar" ImageUrl="../img/aceptar.png" runat="server" Height="30px" Width="30px" ToolTip="Registrar/Editar" />
            &nbsp;&nbsp;
            <asp:ImageButton ID="btnCanelar" ImageUrl="../img/cancelar.png" runat="server" Height="30px" Width="30px" ToolTip="Cancelar/Salir" />
        </td>
    </tr>
</table>