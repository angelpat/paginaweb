﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" Inherits="Aton.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head runat="server">
    <title>Página sin título</title>
     <style type="text/css">
          .columnas
          {
            width: 30px;
          }
     </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%;">
            <tr>
                <td style="<%-- background: url();--%>background-color:#5B755C; height: 55px;">
                    <img src="img/personal4.png" style="border:0px" /><%--<h1>Bitacora</h1>--%>
                </td>                            
            </tr>
            <tr>
                <td align="center" valign="middle">
                    <br />
                    <br />
                    <br />
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0" width="300">
                        <tr>
                            <td style="width: 20px; height: 20px">
                                <img height="100%" src="img/esqVentIzq.png" width="100%" />
                            </td>
                            <td align="center" style="background: url(img/fondoTituloVent.png); width: 260px;
                                height: 20px" valign="middle">
                                <asp:Label ID="lblTitulo" runat="server">Inicio de sesión</asp:Label>
                            </td>
                            <td style="width: 20px; height: 20px">
                                <img height="100%" src="img/esqVentDer.png" width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height: 80px; background-color: #bbd8e1" valign="middle">
                                <table cellpadding="1" cellspacing="1" border="0">
                                    <tr>
                                        <td align="left" style="height: 26px">
                                            <asp:Label ID="Label2" runat="server" Text="Usuario: "></asp:Label>
                                        </td>
                                        <td align="left" style="height: 26px">
                                            <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Contraseña: "></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtClave" runat="server" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="lblMensaje" runat="server" Font-Bold="False" ForeColor="DarkRed"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3" style="height: 38px; background-color: #bbd8e1" valign="middle">
                                <asp:Button ID="btnAcceder" runat="server" Text="Acceder" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>                                
            </tr>
            <tr>
                <td align="center" valign="middle" style="background-color:#1C5E55; ">
                    <br />
                    <font color="white"><b>Hospital con especialidades Juan María de Salvatierra</b></font>
                    <br />
                    <br />
                </td>                               
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
