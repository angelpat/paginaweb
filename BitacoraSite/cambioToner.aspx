﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="cambioToner.aspx.vb" Inherits="Aton.cambioToner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
              <asp:ImageButton ID="btnRegresar" runat="server" Height="30px" 
                    ImageUrl="img/back.png" Width="30px" BorderWidth="0" Visible="False" 
                    ToolTip="Regresar" onclientclick="javascript:return confirm('Desea regresar a la consulta de usuarios ?');" />
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" ImageUrl="img/addArea.png" Width="30px" BorderWidth="0" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                $(document).ready(function () {
                    Load();
                });

                function Load() {
                    $("#<%= txtFechaFin.ClientID %>").datepicker(
                        {
                            changeMonth: true,
                            monthNamesShort: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            changeYear: true,
                            hideIfNoPrevNext: true,
                            nextText: 'Sig',
                            prevText: 'Ant',
                            dateFormat: 'dd/mm/yy',
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
                        });

                    $("#<%= txtFechaInicio.ClientID %>").datepicker(
                        {
                            changeMonth: true,
                            monthNamesShort: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            changeYear: true,
                            hideIfNoPrevNext: true,
                            nextText: 'Sig',
                            prevText: 'Ant',
                            dateFormat: 'dd/mm/yy',
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
                        });
                }
	        </script>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
                <tr align="center" valign="middle">
                  <td align="center" style="height: 30px" valign="middle">                               
                    <h2>Cambio de Tonners</h2>
                  </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <%--<br />--%>
                        <table cellpadding="2" cellspacing="2" border="0" width="450">
                            <tr>
                                <td>Inicio:</td>
                                <td>
                                    <asp:TextBox ID="txtFechaInicio" runat="server" AutoPostBack="True" ></asp:TextBox>
                                </td>
                                <td>Fin:</td>
                                <td>
                                    <asp:TextBox ID="txtFechaFin" runat="server" AutoPostBack="True" ></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tonner:
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="cmbTonner" runat="server" Width="90%" AutoPostBack="True"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
                        <asp:GridView ID="gvListado" runat="server" 
                            AutoGenerateColumns="False" AllowPaging="True" Width="95%" CellPadding="4" 
                            ForeColor="#333333" GridLines="None" PageSize="16">
                            <Columns>
                                <asp:BoundField DataField="fecha" HeaderText="Fecha">
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="usuario" HeaderText="Usuario" >
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="tonner" HeaderText="Tonner" >
                                <ItemStyle HorizontalAlign="Left" Font-Bold="True" VerticalAlign="Middle" 
                                    Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="area" HeaderText="Area" >
                                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <br />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>
