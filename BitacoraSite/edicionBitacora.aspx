﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="edicionBitacora.aspx.vb" Inherits="Aton.edicionBitacora" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
                <asp:ImageButton ID="btnImprimir" runat="server" Height="30px" 
                    ImageUrl="~/img/printer_search.ico" Width="30px" BorderWidth="0" />
                <asp:ImageButton ID="ImageButton1" runat="server" Height="30px" 
                    ImageUrl="img/back.png" Width="30px" BorderWidth="0" 
                    onclientclick="javascript:return confirm('Desea regresar ?');" />
                <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" 
                    ImageUrl="img/addArea.png" Width="30px" BorderWidth="0" Visible="False" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr align="center" valign="middle">
          <td align="center" style="height: 30px" valign="middle">                               
            <h2>Edición Bitacoras</h2>
          </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <br />
                <table border="0" cellpadding="2" cellspacing="2" width="750px">
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="Inicio servicio:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblFechaInicio" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="renglonFechaFin" runat="server">
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Finalización:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblFechaFin" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width:20%;">
                            <asp:Label ID="lblTipo" runat="server" Text="Tipo de atención:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="cmbAtencion" runat="server" Width="50%"></asp:DropDownList><br />
                            <asp:Label ID="lblMensajeAtencion" runat="server" ForeColor="DarkRed"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" Text="Área de servicio:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="cmbAreas" runat="server" Width="100%"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label1" runat="server" Text="Observaciones:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtOberservaciones" runat="server" Width="99%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="renglonTonner" runat="server">
                        <td align="left">
                            <asp:CheckBox ID="chkCambio" runat="server" AutoPostBack="True" />
                            <asp:Label ID="lblTonners" runat="server" Text="Tonner:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="cmbTonner" runat="server" Width="50%"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="chkActivo" runat="server" AutoPostBack="True" Text="" />
                            <asp:Label ID="lblResultados" runat="server" Text="Resultados:"></asp:Label>                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtResultados" runat="server" Width="99%"></asp:TextBox>                            
                        </td>
                    </tr>
                </table>                             
                <br />
                <table border="0" cellpadding="0" cellspacing="0" width="250px">
                    <tr>
                        <td align="center" style="width:50%; height: 24px;">
                            <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" Width="80%" /></td>
                        <td align="center" style="height: 24px">
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="80%" 
                                onclientclick="javascript:return confirm('Seguro que desea cancelar ?');" /></td>
                    </tr>
                </table>
                <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
            </td>
        </tr>
    </table>
</div>
</asp:Content>