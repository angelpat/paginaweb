<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="menu.ascx.vb" Inherits="Aton.menu" %>
<ul id="panelbar">
    <li id="opcionCatalogos" runat="server">Catalogos
        <ul>
            <li>
                <%--<asp:LinkButton ID="lnkUsuarios" PostBackUrl="~/usuarios.aspx" runat="server">Autorización Usuarios</asp:LinkButton>--%>
                <a ID="lnkUsuarios" href="~/usuarios.aspx" runat="server">Autorización Usuarios</a>
            </li>
            <li>
                <%--<asp:LinkButton ID="lnkTipoUsuario" PostBackUrl="~/tiposUsuario.aspx" runat="server">Tipo de Usuario</asp:LinkButton>--%>
                <a ID="lnkTipoUsuario" href="~/tiposUsuario.aspx" runat="server">Tipo de Usuario</a>
            </li>
            <li>
                <%--<asp:LinkButton ID="lnkAtencion" PostBackUrl="~/tiposAtencion.aspx" runat="server">Atenciones</asp:LinkButton>--%>
                <a ID="lnkAtencion" href="~/tiposAtencion.aspx" runat="server">Atenciones</a>
            </li>
            <li>
                <%--<asp:LinkButton ID="lnkAreas" PostBackUrl="~/areas.aspx" runat="server">Áreas</asp:LinkButton>--%>
                <a ID="lnkAreas" href="~/areas.aspx" runat="server">Áreas</a>
            </li>
        </ul>
    </li>
    <li>Bitacora
        <ul>
            <li>
                <%--<asp:LinkButton ID="lnkBitacora" PostBackUrl="~/bitacora.aspx" runat="server">Registros</asp:LinkButton>--%>
                <a ID="lnkBitacora" href="~/bitacora.aspx" runat="server">Registros</a></li>
            <li>
                <%--<asp:LinkButton ID="lnkConsultaBitacora" PostBackUrl="~/usuariosBitacoras.aspx" runat="server">Usuarios</asp:LinkButton>--%>
                <a ID="lnkConsultaBitacora" href="~/usuariosBitacoras.aspx" runat="server">Usuarios</a>
            </li>
        </ul>
    </li>
    <li>Reportes
        <ul>
            <li>
                <a id="lnkCamcioTonners" href="cambioToner.aspx">Cambios de Tonner</a>
            </li>
            <li>
                <a id="lnkInventarioTonner" href="http://192.168.3.251:9000/toner/inventarios.php" target="_blank">Inventario Tonner</a>
            </li>
        </ul>
    </li>
</ul>