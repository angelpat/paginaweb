﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="reporteBitacora.aspx.vb" Inherits="Aton.reporteBitacora" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register Src="menu.ascx" TagName="menu" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
            
              <a href="edicionBitacora.aspx"><img width="30px" height="30px" src="img/back.png" style="border:0;" /></a>
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" 
                    ImageUrl="../img/addArea.png" Width="30px" BorderWidth="0" Enabled="False" 
                    Visible="False" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
        <center>
            <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
        </center>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
            AutoDataBind="True" GroupTreeImagesFolderUrl="" 
            HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" 
            Height="1202px" ReportSourceID="CrystalReportSource1" ToolbarImagesFolderUrl="" 
            ToolPanelView="None" ToolPanelWidth="200px" Width="868px" 
            HasSearchButton="False" HasZoomFactorList="False" PrintMode="ActiveX" />

        <%--<CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="registroBitacora.rpt">
            </Report>
        </CR:CrystalReportSource>--%>
</div>
</asp:Content>