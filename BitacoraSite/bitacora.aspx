﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="bitacora.aspx.vb" Inherits="Aton.bitacora" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
              <asp:ImageButton ID="btnRegresar" runat="server" Height="30px" 
                    ImageUrl="img/back.png" Width="30px" BorderWidth="0" Visible="False" 
                    ToolTip="Regresar" onclientclick="javascript:return confirm('Desea regresar a la consulta de usuarios ?');" />
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" ImageUrl="img/addArea.png" Width="30px" BorderWidth="0" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                $(document).ready(function () {
                    Load();
                });

                function Load() {
                        $("#<%= txtFechaFin.ClientID %>").datepicker(
                        {
                            changeMonth: true,
                            monthNamesShort: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            changeYear: true,
                            hideIfNoPrevNext: true,
                            nextText: 'Sig',
                            prevText: 'Ant',
                            dateFormat: 'dd/mm/yy',
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
                        });

                        $("#<%= txtFechaInicio.ClientID %>").datepicker(
                        {
                            changeMonth: true,
                            monthNamesShort: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            changeYear: true,
                            hideIfNoPrevNext: true,
                            nextText: 'Sig',
                            prevText: 'Ant',
                            dateFormat: 'dd/mm/yy',
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
                        });
                }
	        </script>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
                <tr align="center" valign="middle">
                  <td align="center" style="height: 30px" valign="middle">                               
                    <h2>Bitacoras Registradas</h2>
                  </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <br />
                        <table cellpadding="2" cellspacing="2" border="0">
                            <tr>
                                <td>Inicio:</td>
                                <td>
                                    <asp:TextBox ID="txtFechaInicio" runat="server" AutoPostBack="True" ></asp:TextBox>
                                </td>
                                <td>Fin:</td>
                                <td>
                                    <asp:TextBox ID="txtFechaFin" runat="server" AutoPostBack="True" ></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
                        <asp:GridView ID="gvListado" runat="server" DataKeyNames="id" 
                            AutoGenerateColumns="False" AllowPaging="True" Width="90%" CellPadding="4" 
                            ForeColor="#333333" GridLines="None" PageSize="12">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="false" CommandName="Editar" CommandArgument='<%# (ctype(Container,GridViewRow)).RowIndex %>'
                                            Height="24px" ImageUrl="img/Edit_Icon2.png" Width="24px" ToolTip="Editar este registro" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="id" HeaderText="id" Visible="False" >
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="inicio" HeaderText="Fecha">
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" 
                                    Font-Bold="True" />
                                </asp:BoundField>
                                <asp:BoundField DataField="observaciones" HeaderText="Observaciones" >
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="estado" HeaderText="Estado" >
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                </asp:BoundField>
                                <asp:TemplateField Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCambio" runat="server" Text='<%# Bind("cambio") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("cambio") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <br />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>