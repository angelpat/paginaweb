﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="usuariosBitacoras.aspx.vb" Inherits="Aton.bitacoras" %>
<%@ Register Src="menu.ascx" TagName="menu" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
              <asp:ImageButton ID="ImageButton2" runat="server" Height="30px" 
                    ImageUrl="../img/back.png" Width="30px" BorderWidth="0" Visible="False" />
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" 
                    ImageUrl="../img/addArea.png" Width="30px" BorderWidth="0" Enabled="False" 
                    Visible="False" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
                <tr align="center" valign="middle">
                  <td align="center" style="height: 30px" valign="middle">                               
                    <h2>
                        Bitacoras de Usuarios</h2>
                      <p>
                        Área: <asp:DropDownList ID="cmbAreas" runat="server" Width="50%" AutoPostBack="True"></asp:DropDownList>
                      </p>
                  </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <br />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
                        <asp:GridView ID="gvListado" runat="server" DataKeyNames="id,login" AutoGenerateColumns="False" AllowPaging="True" Width="90%" CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnConsulta" runat="server" CausesValidation="false" CommandName="Consulta" CommandArgument='<%# (ctype(Container,GridViewRow)).RowIndex %>'
                                            Height="28px" ImageUrl="../img/search_user.ico" Width="28px" ToolTip="Consultar bitacora" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="login" HeaderText="Login" >
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="nombre" HeaderText="Nombre" >
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                            </Columns>
                            <RowStyle BackColor="#E3EAEB" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#7C6F57" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div id="divModal" runat="server" visible="false"></div>
</asp:Content>