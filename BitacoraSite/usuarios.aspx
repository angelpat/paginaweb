﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="usuarios.aspx.vb" Inherits="Aton.usuarios" %>

<%@ Register Src="registroUsuario.ascx" TagName="registroUsuario" TagPrefix="uc2" %>
<%@ Register Src="menu.ascx" TagName="menu" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="navigation">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr>
            <td align="right">
              <asp:ImageButton ID="ImageButton2" runat="server" Height="30px" 
                    ImageUrl="../img/back.png" Width="30px" BorderWidth="0" Visible="False" />
              <asp:ImageButton ID="btnAgregar" runat="server" Height="30px" 
                    ImageUrl="../img/addArea.png" Width="30px" BorderWidth="0" Enabled="False" 
                    Visible="False" />
            </td>
        </tr>
    </table>
</div>
<div class="scroll">
    <table border="0" width="100%" cellpadding="0" cellspacing="0" style="height: 100%">
        <tr align="center" valign="middle">
            <td align="center" style="height: 30px" valign="middle">                               
            <h2>Autorización de Usuarios</h2>
                <p>
                Área: <asp:DropDownList ID="cmbAreas" runat="server" Width="50%" AutoPostBack="True"></asp:DropDownList>
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <br />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="DarkRed"></asp:Label>
                <asp:GridView ID="gvListado" runat="server" DataKeyNames="id, login" AutoGenerateColumns="False" AllowPaging="True" Width="90%" CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15">
                    <Columns>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="false" CommandName="Editar" CommandArgument='<%# (ctype(Container,GridViewRow)).RowIndex %>'
                                    Height="24px" ImageUrl="../img/Edit_Icon2.png" Text="Botón" Width="24px" ToolTip="Editar este registro" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="CURP" />
                        <asp:BoundField DataField="login" HeaderText="Login" />
                        <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="activo" HeaderText="Activo" />
                    </Columns>
                    <RowStyle BackColor="#E3EAEB" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
<div id="divModal" runat="server" visible="false"></div>
<div id="divControl" runat="server" visible="false">
    <uc2:registroUsuario ID="RegistroUsuario1" runat="server" />
</div>
</asp:Content>
