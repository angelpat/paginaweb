<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="registro.ascx.vb" Inherits="Aton.registro" %>
<table border="0" cellpadding="0" cellspacing="0" width="300px">
    <tr>
        <td style="width: 20px; height: 20px; background: url(img/esqVentIzq.png)"></td>
        <td align="center" valign="middle" style="width: 260px; height: 20px; background: url(img/fondoTituloVent.png)">
            <asp:Label ID="lblTitulo" runat="server">Registro de</asp:Label>
        </td>
        <td style="width: 20px; height: 20px; background: url(img/esqVentDer.png)"></td>
    </tr>
    <tr>
        <td align="center" valign="middle" colspan="3" style="height: 80px; background-color: #BBD8E1">
            <asp:Label ID="lblArea" runat="server"></asp:Label><br />
            Descripción:
            <br />
            <asp:TextBox ID="txtDescripcion" Width="80%" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
            <br />
        </td>
    </tr>
    <tr>
        <td align="right" valign="middle" style="height: 38px; background-color: #BBD8E1" colspan="3">
            <asp:ImageButton ID="btnAceptar" ImageUrl="img/aceptar.png" runat="server" Height="30px" Width="30px" ToolTip="Registrar/Editar" />
            &nbsp;&nbsp;
            <asp:ImageButton ID="btnCanelar" ImageUrl="img/cancelar.png" runat="server" Height="30px" Width="30px" ToolTip="Cancelar/Salir" />
        </td>
    </tr>
</table>