<?php

$type = $_GET['type'];

/**
 * Type 1: Muestra la ubicacion geografica
 */
switch ($type) {
    case 1:
?>
<iframe width="750" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.mx/?ie=UTF8&amp;t=h&amp;vpsrc=0&amp;ll=24.112797,-110.31828&amp;spn=0.004896,0.008047&amp;z=17&amp;output=embed"></iframe>

<?php
    break;
 
    case 2:
?>

<header>
	<img src="img/content/orla-bienvenida.png" width="600" height="96">
</header>
<article class="bienvenida">
	<div style="float: right; padding-left: 10px;">
		<img alt="Dr. Carlos Arriola Isais" src="img/personal/dr-arriola.png">
		<br /> <br />
		<p class="titulo-director">
			Dr. Carlos Arriola Isais<br /> Director del Hospital
		</p>
	</div>
	<p class="texto-bienvenida">El Benem&eacute;rito Hospital General "Juan
		Mar&iacute;a de Salvatierra" es una instituci&oacute;n que funciona
		como Hospital de concentraci&oacute;n Estatal y ofrece atenci&oacute;n
		M&eacute;dica de Segundo Nivel, con especialidades agregadas de mayor
		nivel como Cirug&iacute;a Pedi&aacute;trica, Nefrolog&iacute;a,
		Cirug&iacute;a Vascular, Odontopediatr&iacute;a, Gineco Obstetricia,
		entre otras; al laborar con lineamientos de humanismo y calidad.</p>
	<br /> <br />
	<p class="texto-bienvenida">Al ser el primer Hospital que se
		instal&oacute; en Baja California Sur, cuenta con un historial de
		servicio y prestigio que enorgullece a los que actualmente tenemos el
		honor de laborar en el; y nos esforzamos por continuar enalteciendo su
		nombre.</p>
	<br /> <br /> <br /> <br /> <br />
	<p class="titulo-bienvenida">
		BIENVENIDOS<br /> A ESTA INSTITUCI&Oacute;N DE SALUD
	</p>
</article>



<?php
    break;
    case 3:
?>
<object style="height: 390px; width: 640px">
	<param name="movie"
		value="http://www.youtube.com/v/keV6T24nBG0?version=3&feature=player_detailpage">
	<param name="allowFullScreen" value="true">
	<param name="allowScriptAccess" value="always">
	<embed
		src="http://www.youtube.com/v/keV6T24nBG0?version=3&feature=player_detailpage"
		type="application/x-shockwave-flash" allowfullscreen="true"
		allowScriptAccess="always" width="640" height="360">

</object>

<?php    	
	break;    
    default:
    break;
?>			
<?php      
}