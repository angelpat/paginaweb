<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css"/>
		<link rel="stylesheet" href="css/master.css"/>
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>	
		<script src="js/menu.js"></script>
 		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");

			$(function(){					
				$( "#modal-contacto" ).dialog({
					autoOpen: false,
					show: "blind",
					hide: "explode"
				});

				$("#menu-contact").click(function(){
					$( "#modal-contacto" ).dialog( "open" );
					return false;		
				});		
			});
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>   		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>	       
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>	
			<section style="padding: 10px;">	
				<article>	
					<header class="quienes-somos">							
						<p>Misi&oacute;n</p>			
					</header>
					<section class="quienes-somos">
						&quot;Somos un Hospital General con Especialidades que proporciona atenci&oacute;n m&eacute;dica a la poblaci&oacute;n del Estado de Baja California Sur, con calidad y eficiencia, sin distinci&oacute;n, privilegiando el desarrollo de la docencia e investigaci&oacute;n en salud&quot;.
						     					
					</section>	
					<aside class="quienes-somos"><img src="img/content/mision.png" width="284" alt="Misi�n"></aside>					
				</article>						
				<article>	
					<header class="quienes-somos">							
						<p>Visi&oacute;n</p>			
					</header>
					<section class="quienes-somos">
						&quot;Seremos un Hospital de Referencia y Concentraci&oacute;n Estatal, acreditado y certificado, con amplia capacidad resolutiva que prestara servicios de salud basados en la mejor tecnolog&iacute;a y avances cient&iacute;ficos disponibles, en un clima laboral de respeto, con calidad y humanismo en la atenci&oacute;n a todos los usuarios.&quot;
					</section>									
					<aside class="quienes-somos" style="height: 110px"><img src="img/content/vision.png" width="284" alt="Visi�n"></aside>	
				</article>				
				<article>	
					<header class="quienes-somos">							
						<p>Valores</p>			
					</header>	
					<section class="quienes-somos">
						<p class="bold">RESPETO A LA INTEGRIDAD HUMANA.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="texto">Garantizamos los derechos universales de la persona y la comunidad para mejorar su calidad de vida y desarrollo individual.</p>				
						<p class="bold">UNIVERSALIDAD.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>
						<p class="desc-valores">Atendemos a todas las personas que soliciten nuestros servicios sin ning&uacute;n tipo de discriminaci&oacute;n.</p>
						<p class="bold">INTEGRIDAD.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="desc-valores">Brindamos atenci&oacute;n continua a la comunidad con &oacute;ptima calidad humana, cientifica y t&eacute;cnica dentro de todos los servicios del Hospital.</p>
						<p class="bold">EFICIENCIA.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="desc-valores">Buscamos una &oacute;ptima aplicaci&oacute;n de los recursos con criterios de beneficio social.</p>
						<p class="bold">SOLIDARIDAD.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="desc-valores">Ampliamos la cobertura de nuestros servicios a la poblaci&oacute;n con mayores limitaciones de acceso y menores recursos econ&oacute;micos, mediante el sistema de cuotas de recuperaci&oacute;n seg&uacute;n nivel socioecon&oacute;mico y a beneficiarios del Seguro Popular.</p>	
						<p class="bold">CALIDAD.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="desc-valores">Buscamos mejorar permanentemente los procesos de atenci&oacute;n a nuestros usuarios logrando la excelencia en la calidad de los servicos.</p>
						<p class="bold">EQUIDAD.</p>
						<span class="ui-icon ui-icon-check" style="float: left; margin-right: .3em;"></span>						
						<p class="desc-valores">Atendemos a todas las personas con los mismos principios de calidad y oportunidad a los servicios de salud.</p>				
					</section>			
					<aside class="quienes-somos"><img src="img/content/valores.png" width="284" alt="Valores"> </aside>	
				</article>
			</section>	
			<?php 
				$objCabecera->pie();
			?>				
		</div>             	 
    </body>
</html>
