<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/servicio-urgencias-triage.js"></script>															
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>	
	<section id="modal-diptico"></section>		
    	<section id="modal-ingreso"></section>    	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<div id="modal-tabla-triage"></div>
			<div id="dialog-form" title="Evaluaci&oacute;n de Pacientes">
				<p class="validateTips">Todos los campos son requeridos.</p>
				<form>
					<fieldset style="border: 1px solid #C9D9E7; padding-left: 10px;">
						<legend>
							<b class="titulo">Valoraci&oacute;n inicial urgencias</b>
						</legend>
						<table style="height: 80px;">
							<thead>
								<tr>
									<th width="250"></th>
									<th width="150"></th>
									<th width=auto style="margin-top: 10px;"></th>
								</tr>
							</thead>
							<tr>
								<td><label for="nombre">Nombre: </label> <input type="text"
									name="nombre" id="nombre"
									class="text ui-widget-content ui-corner-all" maxlength="15" />
								</td>
	
								<td><label for="edad">Edad: </label> <input type="number"
									name="edad" id="edad" value=""
									class="text ui-widget-content ui-corner-all" maxlength="2"
									size="2" />
								</td>
	
								<td><label for="sexo">Sexo</label> <select name="sexo" id="sexo"
									class="text ui-widget-content ui-corner-all">
										<option value="Masculino">Masculino</option>
										<option value="Femenino">Femenino</option>
								</select>
								</td>
							</tr>
						</table>
					</fieldset>
	
					<fieldset
						style="margin-top: 50px; border: 1px solid #C9D9E7; padding-left: 10px;">
						<legend>
							<b class="titulo">Signos vitales</b>
						</legend>
						<table style="height: 80px;">
							<thead>
								<tr>
									<th width="250"></th>
									<th width="150"></th>
									<th width=auto style="margin-top: 10px;"></th>
								</tr>
							</thead>
	
							<tr>
								<td><label for="presion">Presi&oacute;n art. </label> <input
									type="text" name="presion" id="presion"
									class="text ui-widget-content ui-corner-all" maxlength="3"
									size="3" /> <input type="text" name="presion2" id="presion2"
									class="text ui-widget-content ui-corner-all" maxlength="3"
									size="2" /> mm/hg</td>
	
								<td><label for="temp">Temp. </label> <input type="text"
									name="temp" id="temp" value=""
									class="text ui-widget-content ui-corner-all" maxlength="4"
									size="4" /> &deg;C</td>
	
								<td><label for="estado">Estado alerta</label> &nbsp; 
									<select name="estado" id="estado"
										class="text ui-widget-content ui-corner-all">
											<option value="Orientado">Orientado</option>
											<option value="Desorientado">Desorientado</option>
											<option value="Estuporoso">Estuporoso</option>
									</select>
								</td>
							</tr>
	
							<tr>
							
								<td><label for="resp">Respiraci&oacute;n</label> <input
									type="text" name="resp" id="resp" value=""
									class="text ui-widget-content ui-corner-all" maxlength="2"
									size="8" />
								</td>
	
								<td><label for="pulso">Pulso</label> <input type="text"
									name="pulso" id="pulso" value=""
									class="text ui-widget-content ui-corner-all" maxlength="3"
									size="4"/> F.C
								</td>
							</tr>	
						</table>
					</fieldset>
				</form>
			</div>			
			<div id="content">
				<div class="triage-izq">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; ">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Materiales</p><br/>	
					</div >										
						
					<!-- ############################## Material de valoracion ###################### -->

					<div class="ui-widget-content aparato">
						<p class="aparato">Esfigmoman&oacute;metro</p>						
						<img src="../../img/servicios/urgencias/presion.jpg" class="aparato" alt="Presion">
						<div class="aparato-desc">
							<p>Un esfigmoman&oacute;metro es un instrumento m&eacute;dico empleado para la medici&oacute;n indirecta de la presi&oacute;n arterial,...</p>
							<div class="link-leer">
								<strong> <a href="#" class="esfigmomanómetro">Leer m&aacute;s</a></strong>
							</div>
						</div>					
					</div>
					
					<div class="ui-widget-content aparato">
						<p class="aparato">Termometro</p>						
						<img src="../../img/servicios/urgencias/termo.jpg" class="aparato" alt="Termometro">
						<div class="aparato-desc">
							<p>Es un tubo de vidrio sellado que contiene mercurio...</p>
							<div class="link-leer">
								<strong> <a href="#" class="termometro">Leer m&aacute;s</a></strong>
							</div>
						</div>					
					</div>
					<div class="ui-widget-content aparato">
						<p class="aparato"> Medidor de Flujo M&aacute;ximo Pulmonar</p>						
						<img src="../../img/servicios/urgencias/respiracion.jpg" class="aparato" alt="Respiracion">
						<div class="aparato-desc">
							<p>
								El flujo espiratorio m&aacute;ximo es la velocidad m&aacute;s r&aacute;pida en la que los pulmones expulsan aire hacia... 
							</p>
							<div class="link-leer">
								<strong> <a href="#" class="Medidor_de_Flujo_Pulmonar">Leer m&aacute;s</a></strong>
							</div>
						</div>					
					</div>
					<div class="ui-widget-content aparato">
						<p class="aparato">Medidor de Pulso</p>						
						<img src="../../img/servicios/urgencias/pulso.jpg" class="aparato" alt="Pulso">
						<div class="aparato-desc">
							<p>
								Permite escuchar y medir de forma sencilla la frecuencia del pulso card&iacute;aco. 								
							</p>
							<div class="link-leer">
								<strong> <a href="#" class="Medidor_pulso">Leer m&aacute;s</a></strong>
							</div>
						</div>					
					</div>
				
				</div>				

				<div class="triage-der">	
					<div class="barra-01" 
						style="background-image: url('../../img/content/barra-01-l.png'); margin: auto; margin-bottom: 10px;">
		
						<img alt="" src="../../img/content/barra-01-r.png"
							style="float: right; z-index: 0">
						<p class="titulo-barra-01" style="text-align: center;">SIMULADOR DE TRIAGE</p>
					</div>
					
					<p class="texto sangria">
					Triaje (del franc&eacute;s triage) es un m&eacute;todo de la medicina de emergencias y desastres para la selecci&oacute;n y clasificaci&oacute;n de los pacientes bas&aacute;ndose en las prioridades de atenci&oacute;n, privilegiando la posibilidad de supervivencia, de acuerdo a las necesidades terap&eacute;uticas y los recursos disponibles. 
					Trata por tanto de evitar que se retrase la atenci&oacute;n del paciente que empeorar&iacute;a su pron&oacute;stico por la demora en su atenci&oacute;n. Un nivel que implique que el paciente puede ser demorado, no quiere decir que el diagn&oacute;stico final no pueda ser una enfermedad grave.
					</p>
							
					<!-- ############################### Tabla de Resultados del Test ############### -->
					<div style="float: left;">	
						<p class="titulo-triage" >
							Resultados del test
						</p>
						<br>
						<table id="tabla-triage" class="ui-widget ui-widget-content">
							<thead>
								<tr class="ui-widget-header ">
									<th width="60">Prioridad</th>
									<th width="120">Nombre</th>
									<th width="60">Sexo</th>
									<th width="40">Edad</th>
									<th width="80">Presi&oacute;n art.</th>
									<th width="85">Temperatura</th>
									<th width="">Respiraci&oacute;n</th>
									<th width="">Pulso (F.C.)</th>
									<th width="">Estado alerta</th>
								</tr>
							</thead>
							<tbody class="1">
								<tr>
									<td bgcolor="#ff0000">1</td>
									<td>Juan Perez</td>
									<td>Masculino</td>
									<td>23</td>
									<td>100-150</td>
									<td>39.0 &deg;C</td>
									<td>50</td>
									<td>180</td>
									<td>Estuporoso</td>
								</tr>								
							</tbody>							
							<tbody class="2">
							</tbody>
							<tbody class="3">							
							</tbody>
							<tbody class="4">
							</tbody>
						</table>
					</div>	
					<br>
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 10px; padding: 0.7em;  float: left; width: 695px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p>
						<div>
							<p style="margin-left: 1.6em;">* Para iniciar la simulaci&oacute;n haz click sobre el boton <strong>Iniciar Simulaci&oacute;n</strong></p>
							<p style="margin-left: 1.6em;">* El test es solo didactico.</p>												
						</div>
					</div>
					
					<div align="center" style="float: left; width: 100%; padding-top: 10px;">
						<button id="simulacion-boton" >Iniciar simulaci&oacute;n</button>
					</div>
					<br><br>
					<p class="titulo-triage">
						Leyenda
					</p>
					<!-- ############################### Leyenda #################################### -->
					
					<table>
						<thead>	
							<tr>
								<th width="40"></th>
								<th width="500"></th>
							</tr>
							<tr>
								<td><img src="../../img/servicios/urgencias/triage-rojo.png" width="40"></td>
								<td>
									Atenci&oacute;n inmediata - Pasa a sala.
								</td>
							</tr>
							<tr>
								<td><img src="../../img/servicios/urgencias/triage-amarillo.png" width="40"></td>
								<td>
									Enfermedades que pueden llegar a ser gravez si no se atienden pronto. <br>
									Deben ser atendidas en un m&aacute;ximo de 40 min.
								</td>
							</tr>
							<tr>
								<td><img src="../../img/servicios/urgencias/triage-verde.png" width="40"></td>
								<td>
									Enfermedades que pueden ser atendidas dentro de las siguientes 2 horas.
								</td>
							</tr>
							<tr>
								<td><img src="../../img/servicios/urgencias/triage-azul.png" width="40"></td>
								<td>
									Enfermedades que no son urgencias, pueden ser atendidas dentro de las siguientes 6 horas.
								</td>
							</tr>																					
						</thead>
					</table>
		
				</div>
				
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>