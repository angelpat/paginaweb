<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>		
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/tooltip.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/servicio-urgencias.js"></script>															
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-diptico"></section>		
    	<section id="modal-ingreso"></section>    	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-urgencias">
				<section class="urgencias-izq" >	
				 	<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Urgencias</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dr. Miguel Liera Ba&ntilde;uelos &nbsp; &nbsp; &nbsp;Jefe de Urgencias</p>
				 	</div>					 					 		
				</section>
				<section class="urgencias-der">
					<img src="../../img/servicios/urgencias/header-urgencias.png" width="340" height="96" alt="Urgencias">
				</section>	 	
			</header>
			<div id="content">
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Atender con calidad, calidez y respeto, de manera oportuna e inmediata a toda persona que se encuentre con un padecimiento que ponga en riesgo la vida o la funci&oacute;n de un &oacute;rgano, sin distinci&oacute;n de raza, nacionalidad, estado socioecon&oacute;mico, religi&oacute;n o g&eacute;nero.</p>							
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/urgencias/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p> Ser reconocidos como un servicio de urgencias que brinde atenci&oacute;n calificada y oportuna, vanguardia en la atenci&oacute;n m&eacute;dica de urgencias con personal que brinde un trato amable y respetuoso, con tecnolog&iacute;a de punta y comprometido con la educaci&oacute;n m&eacute;dica continua para alcanzar la excelencia</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/urgencias/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>			
				<section class="urgencias-izq">									
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Servicio de Urgencias</p><br/>	
					</div>	
					<section style=" padding: 10px 0px; float: left; ">																
						<p class="texto sangria">
							El servicio de urgencias funciona las 24 horas, los 365 d&iacute;as del a&ntilde;o con la finalidad de atender a las personas que acudan con una enfermedad que a consideraci&oacute;n de ellos amerite una atenci&oacute;n m&eacute;dica inmediata.
							Toda persona que acuda a nuestra unidad deber&aacute; de ser valorada a los primeros 10 minutos de su llegada como m&aacute;ximo y dependiendo del padecimiento se le asignar&aacute; un &aacute;rea donde deber&aacute; de ser atendido. ES IMPORTANTE RECALCAR QUE SOLO ATENDEMOS URGENCIAS CALIFICADAS es decir, enfermedades que pongan en riesgo la vida o la funci&oacute;n de un &oacute;rgano.
						</p>
					</section>
					<br/><br/>		
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 23px;">&iquest;C&oacute;mo Solicitar una Consulta en Urgencias?</p><br/>	
					</div>
							 
					<a href="#" class="tooltip" title="Paso No.1" >							 
			 	 		<img alt="" class ="paso-lab paso-1-ingreso" src="../../img/servicios/urgencias/paso-1-thumb.png" width="176" />
			 	 	</a>
			 	 	<a href="#" class="tooltip" title="Paso No.2" >	
						<img alt="" class ="paso-lab paso-2-ingreso" src="../../img/servicios/urgencias/paso-2-thumb.png" width="176" />
					</a>
					<a href="#" class="tooltip" title="Paso No.3" >																	
						<img alt="" class ="paso-lab paso-3-ingreso" src="../../img/servicios/urgencias/paso-3-thumb.png" width="176" />
					</a><br/>
					<a href="#" class="tooltip" title="Paso No.4" >	
						<img alt="" class ="paso-lab paso-4-ingreso" src="../../img/servicios/urgencias/paso-4-thumb.png" width="176" />
					</a>
					<a href="#" class="tooltip" title="Paso No.5" >							
						<img alt="" class ="paso-lab paso-5-ingreso" src="../../img/servicios/urgencias/paso-5-thumb.png" width="176" />
					</a>
					<a href="#" class="tooltip" title="Paso No.6" >	
						<img alt="" class ="paso-lab paso-6-ingreso" src="../../img/servicios/urgencias/paso-6-thumb.png" width="176" />					
					</a>
					<p class="paso-desc paso-1-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						1.- Solicita atenci&oacute;n m&eacute;dica en recepci&oacute;n.
					</p><br/>
					<p class="paso-desc paso-2-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						2.- Es valorado en TRIAGE por la enfemera, en un m&aacute;ximo de 10 minutos de su llegada.
					</p><br/>
					<p class="paso-desc paso-3-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						3.- Se le asigna un color y dependiendo de ello pasar&aacute; a consulta o a internamiento inmediato.
					</p><br/>
					<p class="paso-desc paso-4-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						4.- Pasar a pagar a Caja la consulta.
					</p><br/>
					<p class="paso-desc paso-5-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						5.- Se otorga la consulta y se emite la receta.
					</p><br/>
					<p class="paso-desc paso-6-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						6.- Acude a Farmacia a surtir la receta.
					</p><br/>

				</section>																		
				<section class="urgencias-der">							
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Plantilla de M&eacute;dicos</p><br/>	
					</div>			
					<section style=" padding-top: 10px; float: left; width: 340px;">									
						<div id="accordion" style="float: left;">
							<h3><a href="#">Turno Matutino</a></h3>
							<div>
								<p class="titulo-dr">Dr. Antonio Ram&iacute;rez Mart&iacute;nez</p>
								<p class="titulo-dr">Dr. Joaqu&iacute;n Ayala Cardoza</p>
								<p class="titulo-dr">Dra. Karla Villa S&aacute;nchez</p>
								<p class="titulo-dr">Dra. Viridiana Olim&oacute;n Aguilar</p>
								<p class="titulo-dr">Dra. Lilian Sotelo P&eacute;rez</p>
							</div>		
							<h3><a href="#">Turno Vespertino</a></h3>
							<div>
								<p class="titulo-dr">Dra. Mar&iacute;a del Carmen D&iacute;az Cer&oacute;n</p>
								<p class="titulo-dr">Dra. Nuria Josefina Rodr&iacute;guez</p>
								<p class="titulo-dr">Dra. Selene Tapia Maga&ntilde;a</p>
								<p class="titulo-dr">Dra. Alba Espinoza Bernal</p>
							</div>
							<h3><a href="#">Nocturno A</a></h3>
							<div>
								<p class="titulo-dr">Dr. Yamil Ju&aacute;rez Cese&ntilde;a</p>
								<p class="titulo-dr">Dr. Esau Trujillo Barcel&oacute;</p>
								<p class="titulo-dr">Dra. Cinthya Rodr&iacute;guez Sandoval</p>
							</div>
							<h3><a href="#">Nocturno B</a></h3>
							<div>
								<p class="titulo-dr">Dr. Catarino Colon Velarde</p>
								<p class="titulo-dr">Dra. Martha V&aacute;zquez Campos</p>
								<p class="titulo-dr">Dr. Valent&iacute;n Rafael Moreno Sicairos</p>
							</div>
							<h3><a href="#">Especial Diurno</a></h3>
							<div>
								<p class="titulo-dr">Dr. V&iacute;ctor Hugo Castro Aguilar</p>
								<p class="titulo-dr">Dra. Karina Bull&oacute;n Z&uacute;&ntilde;iga</p>
								<p class="titulo-dr">Dr. Nelson Cota Sandoval</p>
								<p class="titulo-dr">Dr. Oscar Garc&iacute;a Moreno</p>
								<p class="titulo-dr">Dra. Yessica Jahel Tintor&eacute; Flores</p>
							</div>
							<h3><a href="#">Especial Nocturno</a></h3>
							<div>
								<p class="titulo-dr">Dra. Mar&iacute;a Esperanza Landa Hern&aacute;ndez</p>
								<p class="titulo-dr">Dr. F&eacute;lix V&aacute;zquez V&aacute;zquez</p>
								<p class="titulo-dr">Dr. Mario Alberto Garibay Castro</p>	
							</div>																														
						</div>	
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; margin-top: 10px;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">TRIAGE</p><br/>	
						</div>											
						<!-- <img src="../../img/servicios/urgencias/doctor.jpg" alt="" width="250" style="float: left;" /> -->								
						<img src="../../img/servicios/urgencias/diptico-urgencias-1-thumb.png" class="paso-urg paso-1" alt="" width="170" style="float: left;" />
						<img src="../../img/servicios/urgencias/diptico-urgencias-2-thumb.png" class="paso-urg paso-2" alt="" width="170" style="float: left;" />
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:320px; float: right;" > 
							<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
							<strong>Nota!</strong></p><br/>
							<div>
								* Si no has valorado en los primero 10 minutos de tu registro acude a la ventanilla para informarnos. Cualquier duda, pregunta o incomodidad pide hablar con el Jefe de Servicio o Asesor M&eacute;dico en turno
							</div>
						</div>					
						<br><br>				
						<p style="float: left; font-size: 14px;font-weight: bold; color: #005077;">Recuerda que estamos para servirte y tu bienestar es lo mas importante</p><br>
						
<!-- 						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; margin-top: 10px;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Simulador TRIAGE</p><br/>	
						</div>
						<p>Prueba nuestro simulador de TRIAGE</p><br>
						<a href="triage.php" class="btn">Simulador Triage</a> -->							
					</section>				 
    			</section>	 
    			 		 
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>