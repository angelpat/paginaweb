<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>								
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>
		<script src="../../js/tooltip.js"></script> 
		<script src="../../js/servicio-unidad-quirurgica.js"></script> 

		<script>		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");					
		</script>	
		
<style>


</style>	
				
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	
    	<div id="contador"></div>
    	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<div id="modal-img"></div>
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<div id="content">			
				<section class="unidad-qui-izq">
									
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 20px;">Sistema de Seguimiento de Cirug&iacute;as Programadas</p><br/>	
					</div>
					<table class="table-siscipro texto" style="margin-top: 10px;">					
						<tr>
							<td class="sangria-tabla"></td>
							<td class="sangria-tabla"></td>
							<td></td>
						</tr>
						<tr>
							<td class="sangria-tabla">1.-</td>
							<td colspan="2">El m&eacute;dico determina que el paciente necesita ser sometido a un procedimiento quir&uacute;rgico; se emite una SOLICITUD DE CIRUG&Iacute;A, y se indica una fecha tentativa sujeta a la ocupaci&oacute;n de las salas en la semana correspondiente. El paciente recibe informaci&oacute;n sobre los pasos a seguir para asegurar su espacio en quir&oacute;fano.</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>		
						<tr>
							<td></td>
							<td class="sangria-tabla">1a.-</td>
							<td>Si se requiere de valoraci&oacute;n cardiovascular, el m&eacute;dico realizar&aacute; una INTERCONSULTA AL SERVICIO DE MEDICINA INTERNA</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td></td>
							<td class="sangria-tabla">1b.-</td>
							<td>Si se requiere la presencia durante el procedimiento quir&uacute;rgico de personal de Imagenolog&iacute;a, el m&eacute;dico realizar&aacute; una SOLICITUD DE IMAGENOLOG&Iacute;A</td>
						</tr>	
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td></td>
							<td class="sangria-tabla">1c.-</td>
							<td>Si se requiere de equipo o material especial, el m&eacute;dico har&aacute; la indicaci&oacute;n del mismo en el Sistema de Seguimiento de Cirug&iacute;as Programadas (SiSCiPro)</td>
						</tr>	
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">2.-</td>
							<td colspan="2">El paciente acudir&aacute; a laboratorio a realizar las pruebas indicadas por el m&eacute;dico</td>		
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">3.-</td>
							<td colspan="2">En caso de haber sido requerido, el paciente deber&aacute; acudir a consulta con Medicina Interna para su valoraci&oacute;n cardiovascular</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">4.-</td>
							<td colspan="2">El paciente deber&aacute; acudir al Servicio de Transfusi&oacute;n Sangu&iacute;nea, para platicar sobre la labor de donaci&oacute;n de sangre. El Servicio de Transfusi&oacute;n Sangu&iacute;nea indicar&aacute; en SiSCiPro cuando el paciente cuente con la reserva necesaria de unidades de sangre</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">5.-</td>
							<td colspan="2">El Servicio de CEyE indicar&aacute; en SiSCiPro si se cuenta o no con el material b&aacute;sico y especial</td>
						</tr>		
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">6.-</td>
							<td colspan="2">El jueves en la Reuni&oacute;n del Consejo de Cirug&iacute;a se genera la s&aacute;bana quir&uacute;rgica, en la cu&aacute;l se confirmar&aacute; la fecha, sala y turno de la cirug&iacute;a. Aqu&iacute; se programa una cita con el Servicio de Anestesia para la valoraci&oacute;n pre anest&eacute;sica.</td>	
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td></td>
							<td class="sangria-tabla">6a.-</td>
							<td>El Servicio de Imagenolog&iacute;a ser&aacute; notificado a trav&eacute;s de SiSCiPro si se requerir&aacute; personal de dicha &aacute;rea, y se responder&aacute; de enterado mediante el mismo sistema</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">7.-</td>
							<td colspan="2">El m&eacute;dico anestesi&oacute;logo genera la SOLICITUD DE INTERNAMIENTO, con la cu&aacute;l el paciente acude al Servicio de Admisi&oacute;n</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">8.-</td>
							<td colspan="2">El Servicio de Admisi&oacute;n da el INGRESO HOSPITALARIO al paciente y asigna una cama.</td>		
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">9.-</td>
							<td colspan="2">El Servicio de Enfermer&iacute;a indica mediante SiSCiPro que el paciente es trasladado entre Piso, Recuperaci&oacute;n y Quir&oacute;fano, esto para determinar los tiempos de cirug&iacute;a y desplegar, en pantalla, el estado en tiempo real de cada cirug&iacute;a</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td colspan="3"> <b> Consideraciones:</b></td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">a)</td>
							<td colspan="2">El seguimiento iniciar&aacute; en el momento en que un m&eacute;dico 
											realice una Solicitud de Cirug&iacute;a en 
											<a href="../../departamentos/informatica/sigho.php" class="tooltip link"
												title="Sistema de Informaci&oacute;n para la Gerencia Hospitalaria">
											SIGHO</a>
							</td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">b)</td>
							<td colspan="2">La Solicitud de Cirug&iacute;a deber&aacute; generarse dentro de una consulta o mientras el paciente se encuentra hospitalizado</td>	
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">c)</td>
							<td colspan="2">Se deber&aacute; realizar la Solicitud de Interconsulta al Servicio de Medicina Interna, si es que se requiere de Valoraci&oacute;n Cardiovascular</td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">d)</td>
							<td colspan="2">Si el paciente no se encuentra hospitalizado, las Solicitudes de Laboratorio (e Imagenolog&iacute;a, en caso de ser requeridos estudios transoperatorios), deber&aacute;n de ser realizadas dentro de la misma Consulta en que se realiz&oacute; la Solicitud de Cirug&iacute;a</td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">e)</td>
							<td colspan="2">El m&eacute;dico que lleve a cabo la Valoraci&oacute;n Pre Anest&eacute;sica, deber&aacute; realizar una Orden de Internamiento</td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">f)</td>
							<td colspan="2">Permitir que personal directivo pueda indicar en SiSCiPro cuando un paciente deba ser internado, a&uacute;n sin contar con todos los requisitos establecidos</td>
						</tr>				
						<tr><td colspan="3">&nbsp;</td></tr>	
						<tr>
							<td class="sangria-tabla">g)</td>
							<td colspan="2">Mostrar por servicio las solicitudes realizadas para cada semana</td>
						</tr>					
					</table>	
					<br>
						
					</section>																		
			<section class="unidad-qui-der">
				<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
					<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="font-size: 20px;">Solicitud de Cirug&iacute;a</p><br/>	
				</div>	
				<a href="#" class="tooltip" title="Proceso Quirurgico">
					<img alt="aaa" id="pro-img-01" src="../../img/servicios/unidad.quirurgica/proceso-thumb.png" class="imgshadow" style="width: 330px; margin-top: 10px;">
				</a>    				
    			<a href="#" class="tooltip" title="Solicitud de Cirug&iacute;a">
    				<img alt="" id="pro-img-02" src="../../img/servicios/unidad.quirurgica/solicitud-cirugia-thumb.png" class="imgshadow" style="width: 330px; margin-top: 10px;">
    			</a>
				<a href="#" class="tooltip" title="Programacion Quirurgica">
					<img alt="" id="pro-img-03" src="../../img/servicios/unidad.quirurgica/junta-thumb.JPG" class="imgshadow" style="width: 330px; margin-top: 10px;">
				</a>    				
				<a href="#" class="tooltip" title="SiSCiPro Agenda Semanal">
					<img alt="" id="pro-img-04" src="../../img/servicios/unidad.quirurgica/SiSCiPro-Agenda-Semanal-thumb.png" class="imgshadow" style="width: 330px; margin-top: 10px;">
				</a>    				
    			<a href="#" class="tooltip" title="SiSCiPro-Programaci&oacute;n-Semanal">
    				<img alt="" id="pro-img-05" src="../../img/servicios/unidad.quirurgica/SiSCiPro-Programacion-Semanal-thumb.png" class="imgshadow" style="width: 330px; margin-top: 10px;">
    			</a>
			</section>	
	    			
	    			
	    		
	    			
			</div>
			<?php 
				$objCabecera->pie();
			?>
		</div>               	 
    </body>
</html>