<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../galleriffic/css/galleriffic-3.css" type="text/css" />				
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/servicio-consulta-externa.js"></script>
		<script type="text/javascript" src="../../galleriffic/js/jquery.history.js"></script>
		<script type="text/javascript" src="../../galleriffic/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="../../galleriffic/js/jquery.opacityrollover.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '250px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 12,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            5,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Carrusel',
					pauseLinkText:             'Pause Carrusel',
					prevLinkText:              '&lsaquo; Anterior Imagen',
					nextLinkText:              'Siguiente Imagen &rsaquo;',
					nextPageLinkText:          'Sig &rsaquo;',
					prevPageLinkText:          '&lsaquo; Ant',
					enableHistory:             true,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;
					
					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	

			<div id="content">
				<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
					<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">Galer&iacute;a - Unidad Quir&uacute;rgica</p><br/>	
				</div><br>		
				<div id="container">
					<!-- Start Advanced Gallery Html Containers -->
					<div id="gallery" class="content ">
						<div id="controls" class="controls"></div>
						<div class="slideshow-container">
							<div id="loading" class="loader"></div>
							<div id="slideshow" class="slideshow"></div>
						</div>
						<div id="caption" class="caption-container "></div>
					</div>
					<div id="thumbs" class="navigation">
						<ul class="thumbs noscript">
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1468.JPG" title="Team Quir&uacute;rgico">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1468-thumb.JPG" alt="Team Quir&uacute;rgico" />
								</a>
								<div class="caption">
									<div class="image-title">Team Quir&uacute;rgico</div>
									<div class="image-desc">Personal quir&uacute;rgico en cirug&iacute;a (Sala 1)</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1469.JPG" title="Sala de Quir&oacute;fano No. 2">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1469-thumb.JPG" alt="Sala de Quir&oacute;fano No. 2" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 2</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 2</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1470.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1470-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Maquina de anestesia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1472.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1472-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Maquina de anestesia</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1473.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1473-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Torre de almientaci&oacute;n (oxigeno y aire comprimido)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1474.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1474-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Torre de alimentaci&oacute;n (toma de energia el&eacute;ctrica)</div>
								</div>
							</li>		
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1479.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1479-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Dispensador de material</div>
								</div>
							</li>			
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1480.JPG" title="Sala de Quir&oacute;fano No. 1 (Corta estancia)">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1480-thumb.JPG" alt="Sala de Quir&oacute;fano No. 1 (Corta estancia)" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 1 (Corta estancia)</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 1 (Corta estancia)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1485.JPG" title="Sala de Quir&oacute;fano No. 2 (Corta estancia)">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1485-thumb.JPG" alt="Sala de Quir&oacute;fano No. 2 (Corta estancia)" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 2 (Corta estancia)</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 2 (Corta estancia)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1486.JPG" title="Sala de Quir&oacute;fano No. 3">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1486-thumb.JPG" alt="Sala de Quir&oacute;fano No. 3" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 3</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 3</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1488.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1488-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Maquina de anestesia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1489.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1489-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernas lamparas de techo</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1490.JPG" title="Sala de Quir&oacute;fano No. 3">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1490-thumb.JPG" alt="Sala de Quir&oacute;fano No. 3" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 3</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 3 (vista a&eacute;rea)</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1491.JPG" title="Sala de Quir&oacute;fano No. 2">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1491-thumb.JPG" alt="Sala de Quir&oacute;fano No. 2" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 2</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 2 (vista a&eacute;rea)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1494.JPG" title="Sala de Quir&oacute;fano No. 2">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1494-thumb.JPG" alt="Sala de Quir&oacute;fano No. 2" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 2</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 2 (vista panor&aacute;mica)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1495.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1495-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Dispensador de material</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1502.JPG" title="Sala de Quir&oacute;fano No. 1">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1502-thumb.JPG" alt="Sala de Quir&oacute;fano No. 1" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 1</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 1</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1504.JPG" title="Sala de Quir&oacute;fano No. 1">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1504-thumb.JPG" alt="Sala de Quir&oacute;fano No. 1" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de Quir&oacute;fano No. 1</div>
									<div class="image-desc">Sala de Quir&oacute;fano No. 1 (vista a&eacute;rea)</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1505.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1505-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernas lamparas de techo</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1507.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1507-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Maquina de anestesia y torre de alimentac&oacute;n (vista trasera)</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1514.JPG" title="Sala de recuperaci&oacute;n">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1514-thumb.JPG" alt="Sala de recuperaci&oacute;n" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de recuperaci&oacute;n</div>
									<div class="image-desc">(UCPA)</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1515.JPG" title="Sala de recuperaci&oacute;n">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1515-thumb.JPG" alt="Sala de recuperaci&oacute;n" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de recuperaci&oacute;n</div>
									<div class="image-desc">(UCPA)</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1516.JPG" title="Sala de recuperaci&oacute;n">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1516-thumb.JPG" alt="Sala de recuperaci&oacute;n" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de recuperaci&oacute;n</div>
									<div class="image-desc">(UCPA)</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1517.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1517-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Cuna t&eacute;rmica</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1518.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1518-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Sala de recuperaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1519.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1519-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Sala de recuperaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1520.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1520-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Sala de recuperaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1522.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1522-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Sala de recuperaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1524.JPG" title="Sala de endoscopia">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1524-thumb.JPG" alt="Sala de endoscopia" />
								</a>
								<div class="caption">
									<div class="image-title">Sala de endoscopia</div>
									<div class="image-desc">Descripcion</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1525.JPG" title="Central de enfermer&iacute;a">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1525-thumb.JPG" alt="Central de enfermer&iacute;a" />
								</a>
								<div class="caption">
									<div class="image-title">Central de enfermer&iacute;a</div>
									<div class="image-desc">Salas de endoscopia</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1526.JPG" title="Acceso">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1526-thumb.JPG" alt="Acceso" />
								</a>
								<div class="caption">
									<div class="image-title">Acceso</div>
									<div class="image-desc">Salas de endoscopia</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1527.JPG" title="Pasillo de endoscopia">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1527-thumb.JPG" alt="Pasillo de endoscopia" />
								</a>
								<div class="caption">
									<div class="image-title">Pasillo de endoscopia</div>
									<div class="image-desc">Jefe de servicio</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1528.JPG" title="Pasillo de endoscopia">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1528-thumb.JPG" alt="Pasillo de endoscopia" />
								</a>
								<div class="caption">
									<div class="image-title">Pasillo de endoscopia</div>
									<div class="image-desc">Pasillo de endoscopia</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1531.JPG" title="Oficina">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1531-thumb.JPG" alt="Oficina" />
								</a>
								<div class="caption">
									<div class="image-title">Oficina</div>
									<div class="image-desc">Jefe de Cirug&iacute;a de Corta Estancia y Endoscopia</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1549.JPG" title="Recepci&oacute;n">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1549-thumb.JPG" alt="Recepci&oacute;n" />
								</a>
								<div class="caption">
									<div class="image-title">Recepci&oacute;n</div>
									<div class="image-desc">Vestidores</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1551.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1551-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1553.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1553-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1554.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1554-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1555.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1555-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1556.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1556-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Autoclave</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1558.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1558-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n (Personal)</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1560.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1560-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n (Personal)</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1560.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1560-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n (Persoanl)</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1564.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1564-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>	
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1565.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1565-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n (Personal)</div>
								</div>
							</li>								
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1567.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1567-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1568.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1568-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n (Personal)</div>
								</div>
							</li>															
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1569.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1569-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">CEyE</div>
									<div class="image-desc">Central de Equipo y Esterilizaci&oacute;n</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1594.JPG" title="CEyE">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1594-thumb.JPG" alt="CEyE" />
								</a>
								<div class="caption">
									<div class="image-title">Transfer</div>
									<div class="image-desc">NECESARIO ENTRAR CON TRAJE QUIRURGICO, GORRO, BOTAS, CUBREBOCAS.</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1595.JPG" title="CENDI">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1595-thumb.JPG" alt="CENDI" />
								</a>
								<div class="caption">
									<div class="image-title">CENDI</div>
									<div class="image-desc">CENDI</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1607.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1607-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernos Electrocauterios</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1608.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1608-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernos Electrocauterios</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1609.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1609-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Torre de endoscopias</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1611.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1611-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Torre de endoscopias</div>
								</div>
							</li>														
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1615.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1615-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Sierra el&eacute;ctrica para el retiro de yeso</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1619.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1619-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Torre de endoscopias</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1620.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1620-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Uroendoscopia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1623.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1623-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Uroendoscopia</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1624.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1624-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernas lamparas y monitores</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1626.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1626-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernas maquinas de anestesia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1627.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1627-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernas maquinas de anestesia</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1633.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1633-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Arco en C</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1634.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1634-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Arco en C</div>
								</div>
							</li>							
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1638.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1638-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Facoemulsificador</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1639.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1639-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Facoemulsificador</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1641.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1641-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernos microscopios</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1642.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1642-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Modernos microscopios</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1648.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1648-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Endoscopias</div>
								</div>
							</li>			
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1649.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1649-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Endoscopias</div>
								</div>
							</li>
							<li>
								<a class="thumb" href="../../img/servicios/unidad.quirurgica/galeria/1650.JPG" title="Equipamiento">
									<img src="../../img/servicios/unidad.quirurgica/galeria/1650-thumb.JPG" alt="Equipamiento" />
								</a>
								<div class="caption">
									<div class="image-title">Equipamiento</div>
									<div class="image-desc">Moderno microscopio</div>
								</div>
							</li>																																																	
						</ul>
					</div>
					<!-- End Advanced Gallery Html Containers -->
					<div style="clear: both;"></div>
				</div>
			</div>						
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>