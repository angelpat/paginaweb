<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>  
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/tooltip.js"></script>
		<script src="../../js/servicio-unidad-quirurgica.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>    
  
    <body>              
    	<div id="contador"></div>
    	<section id="modal-croquis"></section>	
    	<section id="modal-organigrama"></section>	
    	<section id="modal-cv"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");							
			?>	
			<header class="header-unidad-qui" style="display: table;">
				<section class="unidad-qui-izq resenia-cv">

					<div class="foto">
						<a class="cv" href="#">
							<img src="../../img/personal/foto/dr-miguel-angel-azua-elias.png" alt="Dr Miguel A. Azua Elias" width="58"/>
						</a>
					</div>
					
					<div class="nombre">
						<a href="#" class="cv">Dr. Miguel Angel Azua Elias</a>
					</div>

					<div class="area">
						<p>Unidad Quir&uacute;rgica y <br> Cirug&iacute;a de Corta Estancia</p>
					</div>
							 				 					 		
				</section>
				
				<section class="unidad-qui-der">
					<img src="../../img/servicios/unidad.quirurgica/header-unidad-q.png" width="340" height="96" alt="Unidad quirurgica">
				</section>	 	
			</header>
			<div id="content">							 
				<div id="features">							
					<div>
						<div class="features-izq">
							<div>
								<h1>Quir&oacute;fano</h1>	
								<p>El quir&oacute;fano es una estructura independiente en la cual se practican intervenciones quir&uacute;rgicas y procedimientos de anestesia y reanimaci&oacute;n necesarias para el buen desarrollo de las mismas y de sus consecuencias</p>								
							</div>							
						</div>
						<div class="features-der" >
							<div>
								<img src="../../img/servicios/unidad.quirurgica/quirofano.png" alt="Quirofano" />
							</div>	
						</div>
					</div>				
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Su misi&oacute;n es ofrecer un  marco de seguridad y confiabilidad a todas las intervenciones, quir&uacute;rgicas electivas o urgentes, para los pacientes con alguna patolog&iacute;a quir&uacute;rgica</p>							
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/unidad.quirurgica/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Ser reconocidos como un servicio de vanguardia que brinde atenci&oacute;n medica quir&uacute;rgica, calificada y oportuna en un marco de seguridad e higiene con la m&aacute;s alta tecnolog&iacute;a y comprometidos con la ense&ntilde;anza m&eacute;dica continua que permita alcanzar la excelencia.</p>														
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/unidad.quirurgica/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>

				<div>				
					<section class="unidad-qui-izq">									
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Sistema de Seguimiento de Cirug&iacute;as Programadas</p><br/>	
						</div><br>							
						<p class="texto">El Sistema de Seguimiento de Cirug&iacute;as Programadas (SiSCiPro) es un programa desarrollado por el 
						                 departamento de Tecnolog&iacute;as de la Informaci&iacute;n  en colaboraci&oacute;n con el servicio de 
						                 Unidad Quir&iacute;rgica y Corta Estancia el cual sirve para programar las solicitudes de 
						                 cirug&iacute;as realizadas por el m&eacute;dico.
						</p>
						
						<a href="siscipro.php" class="link-descarga" style="padding-left: 10px; float: right;" >Leer mas...</a><br>
						<br>
					</section>																		
					<section class="unidad-qui-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 20px;">Solicitud de Cirug&iacute;a</p><br/>	
					</div><br>					
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width:325px;" >
    					<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
    					<strong>Nota!</strong></p>
	    				<div >* Para verificar el estado de su cirug&iacute;a programada presione el bot&oacute;n <strong>Solicitud Cirug&iacute;a</strong></div>	    				
    				</div><br/>    			    
    				<a href="../cirugia/proceso.quirurgico.php" class="btn">Solicitud Cirug&iacute;a</a>
    				<br/>	    			
	    			</section>	  									
				</div>
					
				<div>
				
					<section class="unidad-qui-izq">
					
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Unidad Quir&uacute;rgica y Cirug&iacute;a de Corta Estancia</p><br/>	
						</div><br>							
						
						<img src="../../img/servicios/unidad.quirurgica/croquis.png" usemap="#map-croquis" alt="">
																
						<map name="map-croquis">
							<area shape="rect" coords="17,4,37,213" href="#" onmouseover="opcion(0)" alt="Unidad Quirurgica" />
							<area shape="rect" coords="17,214,237,230" href="#" onmouseover="opcion(1)" alt="Unidad Quirurgica" />
						<!-- 	<area shape="rect" coords="37,5,371,24" href="#" onmouseover="opcion(2)" alt="Unidad Quirurgica"/> -->
							<area shape="rect" coords="37,138,84,159" href="#" onmouseover="opcion(3)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="85,138,99,159" href="#" onmouseover="opcion(4)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="100,138,160,159" href="#" onmouseover="opcion(5)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="161,138,219,159" href="#" onmouseover="opcion(6)" alt="Unidad Quirurgica"/>							
							
							<area shape="rect" coords="220,139,278,159" href="#" onmouseover="opcion(7)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="279,139,338,159" href="#" onmouseover="opcion(8)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="339,138,509,160" href="#" onmouseover="opcion(9)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="323,3,391,42" href="#" onmouseover="opcion(10)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="392,3,408,42" href="#" onmouseover="opcion(11)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="410,3,434,42" href="#" onmouseover="opcion(12)" alt="Unidad Quirurgica"/>
						<!-- 	<area shape="rect" coords="435,3,488,24" href="#" onmouseover="opcion(13)" /> -->
							<area shape="rect" coords="489,3,510,96" href="#" onmouseover="opcion(14)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="535,105,552,200" href="#" onmouseover="opcion(15)" alt="Unidad Quirurgica"/>							
							<area shape="rect" coords="491,268,515,280" href="#" onmouseover="opcion(16)" alt="Unidad Quirurgica"/>							
							<area shape="rect" coords="471,268,490,280" href="#" onmouseover="opcion(17)" alt="Unidad Quirurgica"/>							
							<area shape="rect" coords="359,268,416,281" href="#" onmouseover="opcion(18)" alt="Unidad Quirurgica"/>							
							<area shape="rect" coords="287,162,308,176" href="#" onmouseover="opcion(19)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="223,162,285,176" href="#" onmouseover="opcion(20)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="38,77,99,137" href="#" onmouseover="opcion(21)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="100,78,160,137" href="#" onmouseover="opcion(22)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="161,78,219,138" href="#" onmouseover="opcion(23)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="220,78,278,138" href="#" onmouseover="opcion(24)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="279,78,338,138" href="#" onmouseover="opcion(25)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="237,177,298,313" href="#" onmouseover="opcion(26)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="299,177,358,313" href="#" onmouseover="opcion(27)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="359,282,416,315" href="#" onmouseover="opcion(28)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="418,281,490,315" href="#" onmouseover="opcion(29)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="491,281,515,315" href="#" onmouseover="opcion(30)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="511,3,546,104" href="#" onmouseover="opcion(31)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="547,4,584,104" href="#" onmouseover="opcion(32)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="373,162,415,195" href="#" onmouseover="opcion(33)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="373,196,415,233" href="#" onmouseover="opcion(34)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="416,179,428,185" href="#" onmouseover="opcion(35)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="430,179,451,185" href="#" onmouseover="opcion(36)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="416,186,461,206" href="#" onmouseover="opcion(37)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="462,186,489,206" href="#" onmouseover="opcion(38)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="417,207,461,233" href="#" onmouseover="opcion(39)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="462,207,489,233" href="#" onmouseover="opcion(40)" alt="Unidad Quirurgica"/>														

							<area shape="rect" coords="161,25,180,77" href="#" onmouseover="opcion(41)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="209,25,252,57" href="#" onmouseover="opcion(42)" alt="Unidad Quirurgica"/>							
							<area shape="rect" coords="269,25,307,58" href="#" onmouseover="opcion(43)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="252,64,269,77" href="#" onmouseover="opcion(44)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="270,59,316,77" href="#" onmouseover="opcion(45)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="317,64,388,77" href="#" onmouseover="opcion(46)" alt="Unidad Quirurgica"/>
							
							<area shape="rect" coords="397,43,434,85" href="#" onmouseover="opcion(47)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="435,25,489,42" href="#" onmouseover="opcion(48)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="435,43,488,121" href="#" onmouseover="opcion(49)" alt="Unidad Quirurgica"/>
							<area shape="rect" coords="417,122,481,137" href="#" onmouseover="opcion(50)" alt="Unidad Quirurgica"/>
						</map>
		
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
							<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
							<strong>Nota!</strong> </p>
							<p>* La Unidad Quir&uacute;rgica se encuentra ubicada en el primer piso. 
								<a class="btn" href="#" id="btn-croquis" style="position: relative; left: 80px;">Zoom</a>
							</p> 
							<p style="margin-left: 1.5em;">* El acceso es por medio del elevedor o por las escaleras</p>
							 																
						</div>
					</section>							
																
					<section class="unidad-qui-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Croquis</p><br/>	
						</div><br>    				
						<div class="ui-widget-content ui-corner-all"
							style="margin-top: 0px; padding: 0.7em; width: 320px; height: 400px;">
							<p id="desc-croquis"></p>
						</div>	    				    			    			
						<br/>
	    			</section>	  					
				</div>

				<div>				
					<section class="unidad-qui-izq">
					
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Estructura Organica</p><br/>	
						</div><br>							
	
						<p class="bullet texto">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>I.	La organizaci&oacute;n del &aacute;rea quir&uacute;rgica se basa en el concepto de unidad, entendido como el  conjunto de &aacute;reas, espacios y locales en los que personal de diversas disciplinas de la salud y pertenecientes a diferentes departamentos del Hospital, labora en  forma armoniosa y coordinada.
						</p><br>					
						<p class="bullet texto">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>II.	Al frente de la Unidad hay un Coordinador nombrado por el titular de la unidad, quien   acuerda con las subdirecciones M&eacute;dica y Administrativa para solventar las necesidades del servicio de acuerdo con la normatividad aplicable y los recursos disponibles. Es indispensable la comunicaci&oacute;n con los servicios de Cirug&iacute;a y Medicina Interna, auxiliares de diagn&oacute;stico y tratamiento y de actividades param&eacute;dicas.
						</p><br>										
						<br>
					</section>																		
					<section class="unidad-qui-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Organigrama</p><br/>	
						</div>	    				 	    				
	    				<img alt="" src="../../img/servicios/unidad.quirurgica/organigrama-thumb.png"  class="imgshadow" style="width: 330px; margin-top: 10px;">
	    				<a id="btn-zoom" class="btn" style="margin: 10px 0;">Zoom</a>
	    				
	    			</section>	  									
				</div>
				
				<div>				
					<section class="unidad-qui-izq">					
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">&Aacute;rea de admisi&oacute;n programados</p><br/>	
						</div>							
						<div class="texto">
							<p class="bullet">
								<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
								Confirma a los usuarios con 7 d&iacute;as naturales de anticipaci&oacute;n que ser&aacute;n intervenidos quir&uacute;rgicamente y les da indicaciones de ingreso personalmente, informa de valoraci&oacute;n preanest&eacute;sica, donaci&oacute;n de sangre y horarios de ingreso.							 
							</p><br>						
							<p class="bullet">
								<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza tr&aacute;mite de ingreso, explica los mecanismos de pacientes particulares y seguro popular y asigna la cama en el sistema.
							</p><br>	
							<p class="bullet">
								<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza el tr&aacute;mite de egresos del paciente, como arreglo de expediente  f&iacute;sico, alta en el sistema e informe de los mismos.
							</p><br>
							<p class="bullet">
								<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Para cualquier procedimiento quir&uacute;rgico el tramite de pago se realiza en caja.
							</p><br>							
						</div>
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
							<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
							<strong>Nota!</strong> </p>
							<p>* Para asesor&iacute;a e informaci&oacute;n sobre conceptos de cobro acudir a Trabajo Social. 								
							</p> 
						</div>						
						
					</section>																		
					<section class="unidad-qui-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01" style="font-size: 20px;">Galer&iacute;a de Imagenes</p><br/>	
						</div>	    					   
	    				<a href="galeria.php"> 
	    					<img alt="" src="../../img/servicios/unidad.quirurgica/galeria.jpg"  class="imgshadow" style="width: 330px; margin: 10px 0;">
	    				</a>	    				    					  	    				 				    	
	    			</section>	  									
				</div>
				

			</div>
			<?php 
				$objCabecera->pie();
			?>
		</div>               	 
    </body>
</html>