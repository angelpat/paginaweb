<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>  
		<script src="../../js/tooltip.js"></script>  
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/servicio-imagenologia.js"></script>															
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-ingreso"></section>    	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-imagenologia">
				<section class="urgencias-izq" >
				 	<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Imagenolog&iacute;a</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dr. Fidencio Ba&ntilde;uelos Morales &nbsp; &nbsp; &nbsp;Jefe de Servicio</p>
				 	</div>				 		
				</section>
				<section class="imagenologia-der">
					<img src="../../img/servicios/imagenologia/header-imagenologia.png" width="340" height="96" alt="Imagenologia">
				</section>	 	
			</header>
			<div id="content">
			

				<div id="features">							
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Nuestro compromiso es dar  resultados confiables y oportunos que sean de apoyo al m&eacute;dico y al paciente en el diagn&oacute;stico, utilizando m&eacute;todos certificados con equipo especificado de avanzada tecnolog&iacute;a y con personal capacitado en constante actualizaci&oacute;n. Comprometidos con la salud y satisfacci&oacute;n de nuestros usuarios, ofrecemos un servicio basado en un sistema de calidad que cumple con requisitos normalizados.</p>															
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/imagenologia/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Ser un Servicio  con la capacidad de cubrir eficientemente las necesidades actuales y futuras de nuestros usuarios y con la garant&iacute;a de ofrecer siempre validez y confiabilidad en los resultados y distinguirnos por la aplicaci&oacute;n constante de principios de calidad en nuestro trabajo.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/imagenologia/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>			
				
				<section class="imagenologia-izq">			
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 16px;">&iquest;C&oacute;mo solicita una placa de RX, Tomograf&iacute;a y Resonancia?</p><br/>	
					</div>
							 
					<a href="#" class="tooltip" title="Paso No.1" >							 
			 	 		<img alt="" class ="paso-lab paso-1-ingreso" src="../../img/servicios/imagenologia/paso-1-thumb.png" width="176" />
			 	 	</a>
			 	 	<a href="#" class="tooltip" title="Paso No.2" >
						<img alt="" class ="paso-lab paso-2-ingreso" src="../../img/servicios/imagenologia/paso-2-thumb.png" width="176" />
					</a>
					<a href="#" class="tooltip" title="Paso No.3" >												
						<img alt="" class ="paso-lab paso-3-ingreso" src="../../img/servicios/imagenologia/paso-3-thumb.png" width="176" /><br/>
					</a>
					<a href="#" class="tooltip" title="Paso No.4" >
						<img alt="" class ="paso-lab paso-4-ingreso" src="../../img/servicios/imagenologia/paso-4-thumb.png" width="176" />
					</a>						
				
				
					<p class="paso-desc paso-1-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						1.- Presentarse al &aacute;rea de control de Imagenolog&iacute;a con solicitud del m&eacute;dico para el estudio solicitado y darle fecha  para la cita.
					</p><br/>
					<p class="paso-desc paso-2-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-bottom: 20px;"></span>
						2.- Entregar Solicitud M&eacute;dica de:<br>
							<strong>Seguro popular</strong> (Sellos del Seguro Popular y Caja)<br>
							<strong>Poblaci&oacute;n General</strong> (Recibo de Caja)
					</p><br/>
					<p class="paso-desc paso-3-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-bottom: 20px;"></span>
						3.- Presentarse el <strong>D&Iacute;A DE LA CITA</strong> a las 08:30 ser puntual y entregar la solicitud a la encargado(a) de Control y posteriormente ser&aacute; llamada por la enfermera del &aacute;rea para su estudio.
					</p><br/>
					<p class="paso-desc paso-4-ingreso">
						<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
						4.- Entrega de resultados
							Los resultados de los Estudios de <strong>Flouroscopia, Tomograf&iacute;a y Resonancia</strong> ser&aacute;n <strong>entregados 2 d&iacute;as posteriores al estudio tomado</strong>. 
					</p><br/>

				</section>																		
				<section class="imagenologia-der">							
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Plantilla de M&eacute;dicos</p><br/>	
					</div>			
					<section style=" padding-top: 10px; float: left; width: 340px;">									
						<div id="accordion" style="float: left;">
							<h3><a href="#">Turno Matutino</a></h3>
							<div>
								<p class="titulo-dr">Dr. Fidencio Ba&ntilde;uelos Morales</p>
								<p class="titulo-dr">C. Cynthia D. Vallarino Cese&ntilde;a</p>
								<p class="titulo-dr">Dr. Fidencio Ba&ntilde;uelos Morales</p>
								<p class="titulo-dr">Dr. Carlos A. Lopez Calvillo</p>
								<p class="titulo-dr">Dr. Miguel A. Monta&ntilde;o Mendez</p>
								<p class="titulo-dr">Dra. Andrea Orantes</p>
								<p class="titulo-dr">Dr. Miguel A. Monta&ntilde;o Mendez</p>
								<p class="titulo-dr">T.R. Beatriz Jimenez Barrera</p>
								<p class="titulo-dr">T.R. Juan Antonio Rosas Olachea</p>
								<p class="titulo-dr">T.R. Jes&uacute;s Omar Garza</p>
								<p class="titulo-dr">T.R. Ernesto Alvarado P&eacute;rez</p>
								<p class="titulo-dr">T.R. Jorge l. Cervantes Ahumada</p>
								<p class="titulo-dr">T.R. Neri A. Rosas Olachea</p>
								<p class="titulo-dr">C. Ma. Graciela Amador Amador</p>
								<p class="titulo-dr">C. Camerina Mendoza Verdugo</p>
							</div>		
							<h3><a href="#">Turno Vespertino</a></h3>
							<div>
								<p class="titulo-dr">Dr. Carlos Cornejo Monroy</p>
								<p class="titulo-dr">T.R. Pedro E. Santana Morera</p>
								<p class="titulo-dr">C. Daniel Plasencia urias</p>
							</div>
							<h3><a href="#">Nocturno A</a></h3>
							<div>
								<p class="titulo-dr">T.R. Miriam Mendoza Medina</p>
							</div>
							<h3><a href="#">Nocturno B</a></h3>
							<div>
								<p class="titulo-dr">T.R. Adrian Minchaca Salas</p>
							</div>
							<h3><a href="#">Especial Diurno</a></h3>
							<div>								
								<p class="titulo-dr">Dr. Carlos Eusebio Pozo Juarez</p>
								<p class="titulo-dr">T.R. Karina Ortega Amador</p>
								<p class="titulo-dr">T.R. Bulmaro Geraldo Romero</p>								
							</div>
							<h3><a href="#">Especial Nocturno</a></h3>
							<div>							
								<p class="titulo-dr">T.R. Ma. de los Angeles Dominguez</p>
								<p class="titulo-dr">T.R. Ignacio Juarez Gonzalez</p>
							</div>																														
						</div>	
					
					
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; margin-top: 10px;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Servicios que ofrece</p><br/>	
						</div>											
						<section style="padding-left: 10px; padding-top: 10px; float: left;">					
							<p class="titulo-horario">Ultrasonido</p>
							<p class="personal">08:00 a 20:00 hrs.</p><br>
							<p class="titulo-horario">Flouroscopia</p>
							<p class="personal">08:00 a 20:00 hrs.</p><br>
							<p class="titulo-horario">Ortopantomograf&iacute;a</p>
							<p class="personal">08:00 a 14:30 hrs. </p><br>
							<p class="titulo-horario">Densitometria</p>
							<p class="personal">08:00 a 18:00 hrs.</p><br>
							<p class="titulo-horario">Resonancia</p>
							<p class="personal">08:00 a 14:00 hrs.</p><br>
							<p class="titulo-horario">Tomograf&iacute;a y RX</p>
							<p class="personal">Las 24 hrs.</p><br>							
							
						</section>											
						
					</section>				 
    			</section>	 
    			 		 
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>