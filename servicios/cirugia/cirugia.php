<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/servicio-cirugia.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>           
    	<div id="contador"></div>   
    	<section id="modal-cirugia"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-cirugia">
				<section class="cirugia-izq" >	
				 	<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Cirug&iacute;a</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dr. Jos&eacute; Juan Agundez Meza &nbsp; &nbsp; &nbsp;Jefe de Cirug&iacute;a</p>
				 	</div>					 					 		
				</section>
				<section class="cirugia-der">
					<img src="../../img/servicios/cirugia/header-cirugia.png" width="340" height="96" alt="Cirugia">
				</section>	 	
			</header>
			<div id="content">
			
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>"Somos un servicio del Hospital que proporciona atenci&oacute;n m&eacute;dica y quir&uacute;rgica a la poblaci&oacute;n del Estado de Baja California Sur con buena calidad y eficiencia, con los avances tecnol&oacute;gicos de vanguardia sin distinci&oacute;n de su nivel socioen&oacute;mico o condici&oacute;n de aseguramiento, privilegiando el desarrollo de la docencia e invesgaci&oacute;n en Salud".</p>							
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/cirugia/mision.png" alt="Mision" />
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Ser el mejor Centro Hospitalario de atenci&oacute;n quir&uacute;rgica, del Estado de Baja California Sur, con car&aacute;cter de hospital universitario y participaci&oacute;n estrecha en la docencia e invesgaci&oacute;n para la formaci&oacute;n de recursos para la atenci&oacute;n de la salud, desde estudiantes, becarios de pre y postgrado.</p>															
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/cirugia/vision.png" alt="Vision" />
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Valores</h1>							
								<p>&Eacute;tica, Reponsabilidad, Calidad, Seguridad, Innovaci&oacute;n y Liderazgo, mejor ambiente laboral.</p>															
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/cirugia/valores.png" alt="Valores" />
							</div>
						</div>
					</div>															
				</div>										
				<section class="cirugia-izq">
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Servicio de Cirug&iacute;a</p><br/>	
					</div><br>				
					<p class="texto sangria">
						La actividad del servicio de Cirug&iacute;a es proporcionada por los servicios de Urgencias, Admisi&oacute;n Hospitalaria, Hospitalizaci&oacute;n y la Consulta Externa de Especialidades, a trav&eacute;s de la valoraci&oacute;n realizada por el m&eacute;dico que indica la necesidad de que el paciente sea intervenido quir&uacute;rgicamente. En los servicios de urgencias y admisi&oacute;n, ingresan los paciente que pesentan un sindreome de abdomen agudo, traumatismos, herida por arma de fuego o blanca, accidentes automovil&iacute;sticos, etc. que requieren la atenci&oacute;n m&eacute;dica y quir&uacute;rgica urgente, y que posteriormente ingresan a Cirug&iacute;a.
					</p>
					<br/>
					<p class="texto sangria">
						En caso de Consulta Externa se ingresan pacientes con padecimientos que ameritan de atenci&oacute;n no urgente y rara vez urgente, pero que necesariamente deber&aacute;n ingresar al servicio de Cirug&iacute;a, previo ingreso a hospitalizaci&oacute;n para valoraci&oacute;n del especialista, completar ayuno o estudios. En el caso de hospitalizaci&oacute;n, paciente hospitalizado con un diagn&oacute;stico que no requiera tratamiento quir&uacute;rgico de inicio, por presentar complicaciones post-quir&uacute;rgicas o como aquellos pacientes que presentan alguna complicaci&oacute;n pos-quir&uacute;rgica inmediata (estado de choque por hermorragia activa del &aacute;rea previamente intervenida, dehiscencia de herida quir&uacute;rgica, entre otras) estando a&uacute;n en la sala de operaciones o en la de recuperaci&oacute;n.
					</p>					
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Proceso Quir&uacute;rgico</p><br/>	
					</div><br>					
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width:580px;" >
    					<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
    					<strong>Nota!</strong></p>
	    				<div >* Para verificar el estado de su cir&uacute;gia programada presione el boton <strong>Solicitud Cirug&iacute;a</strong></div>	    				
    				</div><br/>    			    
    				<a href="proceso.quirurgico.php" class="btn">Solicitud Cirug&iacute;a</a>
    				<br/>										
				</section>																		
				<section class="cirugia-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>
    				<img alt="" src="../../img/servicios/cirugia/organigrama-thumb.png" width="328" class="imgshadow" style="margin: 10px 0">
    				<a id="btn-zoom" class="btn">Zoom</a> <br/>
					<br/>					
    				
    			</section>	  		    				
			</div>
			<?php 
				$objCabecera->pie();
			?>	
		</div>               	 
    </body>
</html>