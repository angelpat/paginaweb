<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css"/>
		<link rel="stylesheet" href="../../css/master.css"/>
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>        
		<script src="../../js/menu.js"></script>		
		<script src="../../js/proceso-quirurgico.js"></script>        
 		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
		<div id="modal-map"></div>		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div> 				
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<div id="content">			
				<section id="pro-quirurgico-izq">		
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); width: 209px;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >							
					</div>		
					<div id="pro-quirurgico-login"></div>							
				</section>
				<section id="pro-quirurgico-der">	
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); width: 505px;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Solicitud de Cirug&iacute;a</p><br/>	
					</div>					
					<div id="pro-quirurgico-info"></div>					
				</section>
			</div>
			<?php 
				$objCabecera->pie();
			?>	
		</div>
    </body>
</html>
