<?php 
include_once '../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="../css/jquery-ui-1.8.16.custom.css"/>
		<link rel="stylesheet" href="../css/master.css"/>
		<link rel="stylesheet" href="../css/menu.css" type="text/css" media="screen" />			
		<script src="../js/jquery-1.6.2.min.js"></script>
		<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../js/menu.js"></script>
 		<script>

		$(function(){
				
			$( "#modal-contacto" ).dialog({
				autoOpen: false,
				show: "blind",
				hide: "explode"
			});
				
			$("#menu-contact").click(function(){
				$( "#modal-contacto" ).dialog( "open" );
				return false;		
			});	
			
			$( ".link-nivel" ).hover(function() {		
				$(this).find("span").removeClass( "ui-icon-check" );
				$(this).find("span").addClass( "ui-icon-pin-s" );
				
			}, function(){
				$(this).find("span").removeClass( "ui-icon-pin-s" );
				$(this).find("span").addClass( "ui-icon-check" );
			});
		
			$(document).ready(function(){
				$('#modal-tip').load('../ajax_contador.php', 'url=' + location.href, '');
			});
			
		});
 		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");						
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
		<div id="modal-tip"></div>	
		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>			
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("1");		
				$objCabecera->menu("../");			
			?>	 	
			<div id="content">				
			 	<section class="servicios-izq">	
					<div class="barra-01" style="background-image: url('../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">SERVICIOS</p><br/>	
					</div>

				</section>					
				<section class="servicios-der">
					<div class="barra-02" style="background-image: url('../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">DEPARTAMENTOS</p><br/>	
					</div>																	
				</section>				
			 		
			 					 		
					<table id="tabla-menu-nivel">

						<tr>
							<td class="area"></td>
							<td class="area"></td>
							<td class="espacio"></td>
							<td class="area"></td>
							<td class="area"></td>
						</tr>						
						<tr>
					 		<td colspan="5" class="titulo-nivel">PLANTA BAJA</td>					 		
					 	</tr>	
					 	<tr>
					 		<td><a class="link-nivel menu-archivo" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Archivo Cl&iacute;nico</a></td>
					 		<td>
					 			<a class="link-nivel menu-laboratorio" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Laboratorio</a>					 		
					 		</td>
					 		<td></td>
					 		<td><a class="link-nivel menu-nutricion" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Nutrici&oacute;n, Diet&eacute;tica y Cocina</a></td>
					 		<td><a class="link-nivel">Delegacion Sindical</a></td>
					 	</tr>
					 	<tr>
					 		<td><a class="link-nivel menu-consulta" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Consulta Externa</a></td>
					 		<td><a class="link-nivel">Unidad de Salud Mental</a></td>
					 		<td></td>
					 		<td><a class="link-nivel">Intendencia</a></td>
					 		<td><a class="link-nivel">Infraestructura y Servicios Generales</a>
					 		</td>
					 	</tr>				 	
					 	<tr>				 	
					 		<td><a class="link-nivel">Farmacia</a></td>
					 		<td>
					 			<a class="link-nivel menu-urgencias" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Urgencias</a>
					 		</td>
					 		<td></td>
					 		<td><a class="link-nivel">Patologia</a></td>
					 		<td></td>
					 	</tr>
					 	<tr>
					 		<td>
					 			<a class="link-nivel menu-imagenologia" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Imagenologia (Rayos X)</a>
					 		</td>
					 		<td><a>Transfucion Sanguinea</a></td>
					 		<td></td>
					 		<td><a class="link-nivel">Abastecimiento</a></td>
					 		<td></td>
					 	</tr>
					 	<tr>
					 		<td>&nbsp;</td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 	</tr>		
					 	<tr>
					 		<td colspan="5" class="titulo-nivel">PRIMER PISO</td>					 		
					 	</tr>	
					 	<tr>
					 		<td><a class="link-nivel menu-ucin-galeria" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Pediatr&iacute;a UCIN</a></td>
					 		<td><a class="link-nivel">Patolog&iacute;a</a></td>
					 		<td></td>
					 		<td><a class="link-nivel menu-calidad" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</a></td>
					 		<td><a class="link-nivel">Medicina Preventiva y Promocion de la Salud</a></td>
					 	</tr>
					 	<tr>
					 		<td><a class="link-nivel">Terapia</a></td>
					 		<td><a class="link-nivel">Hemodinamia</a></td>
					 		<td></td>
					 		<td><a class="link-nivel">Direcci&oacute;n General</a></td>
					 		<td><a class="link-nivel">Recursos Humanos</a></td>
					 	</tr>				 	
					 	<tr>				 	
					 		<td><a class="link-nivel">Trasplante</a></td>
					 		<td><a class="link-nivel menu-unidad-quirurgica" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Unidad Quir&uacute;rgica</a></td>
					 		<td></td>
					 		<td>
					 			<a class="link-nivel menu-ensenanza" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Ense&ntilde;anza e Investigacion</a>
					 		</td>
					 		<td>
								<a class="link-nivel menu-informatica" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Ingenieria en Sistemas de Informaci&oacute;n</a>
					 		</td>
					 	</tr>
					 	<tr>				 	
					 		<td><a class="link-nivel">Vigilancia Epidemiol&oacute;gica</a></td>
					 		<td></td>
					 		<td></td>
					 		<td><a class="link-nivel">Jefatura de Enfermer&iacute;a</a></td>
					 		<td><a class="link-nivel">Vigilacia Epidemiol&oacute;gica</a></td>
					 	</tr>	
					 	<tr>				 	
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 		<td><a class="link-nivel menu-ts" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;"></span>Jefatura de Trabajo Social</a></td>
					 		<td></td>
					 	</tr>					 		
					 	<tr>				 	
					 		<td>&nbsp;</td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 	</tr>			 	
					 	<tr>
					 		<td colspan="5" class="titulo-nivel">SEGUNDO PISO</td>
					 	</tr>	
					 	<tr>
					 		<td>
					 			<a class="link-nivel menu-cirugia" href="#"><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em; margin-left: 10px;" ></span>Cirug&iacute;a</a>
					 		</td>
					 		<td><a class="link-nivel">Medicina Interna</a></td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 	</tr>			 	
					 	<tr>				 	
					 		<td><a class="link-nivel">Toco-Cirug&iacute;a</a></td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 		<td></td>
					 	</tr>						 	
					</table>				
						
			</div>
			<?php 
				$objCabecera->pie();			
			?>		
		</div>                	 
    </body>
</html>
