<?php
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
<link rel="stylesheet" href="../../css/master.css" />
<link rel="stylesheet" href="../../css/menu.css"/>		
<link rel="stylesheet" href="../../css/jshowoff.css" />
<script src="../../js/jquery-1.6.2.min.js"></script>
<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
<script src="../../js/jquery.jshowoff.js"></script>
<script src="../../js/menu.js"></script>
<script src="../../js/servicio-consulta-externa.js"></script>
<script>
	document.createElement("nav");
	document.createElement("header");
	document.createElement("footer");
	document.createElement("section");
	document.createElement("article");
	document.createElement("aside");
	document.createElement("hgroup");

	$(function(){	
					
		$( "#modal-contacto" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "explode"
		});
			
		$("#menu-contact").click(function(){
			$( "#modal-contacto" ).dialog( "open" );
			return false;		
		});			
					
	});
	
</script>

<title>HOSPITAL SALVATIERRA</title>
<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
</head>
<body>
	<div id="contador"></div>
	<section id="modal-organigrama"></section>
	<div id="modal-contacto" title="P&aacute;gina de Contacto">
		<br> <br> <br>
		<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en
			l&iacute;nea</p>
	</div>
	<div id="wrapper">
		<?php
		$objCabecera->cabecera("");
		$objCabecera->menu("../../");
		?>
		<header class="header-consulta">
			<section class="consulta-izq">
				<div
					style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat;">
					<br>
					<p class="titulo-header">Consulta Externa</p>
					<br> <br> <br>
					<p class="jefe-servicio">Dr. Jos&eacute; Grajales Montiel&nbsp; &nbsp; &nbsp;Consulta Externa</p>
				</div>
			</section>
			<section class="consulta-der">
				<img src="../../img/servicios/archivo.clinico/header-archivo.png" width="340" height="96" alt="Consulta Externa">
			</section>
		</header>
		<div id="content">

				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>
								<p>Proporcionar atenci&oacute;n m&eacute;dica con recursos
									suficientes y capacitados, con l&iacute;neas jer&aacute;rquicas
									bien definidas acordes con los principios bio&eacute;ticos de la
									pr&aacute;ctica m&eacute;dica y con procedimientos t&eacute;cnico
									administrativos que garanticen su aplicaci&oacute;n y efectividad
									para lograr la satisfacci&oacute;n de los usuarios, el bienestar
									f&iacute;sico y mental del paciente, prolongar y mejorar su
									calidad de vida, as&iacute; como fomentar el crecimiento
									profesional de los trabajadores.
								</p>
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/archivo.clinico/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>
								<p>Brindar la mejor atenci&oacute;n a los pacientes, en forma
									oportuna y adecuada con la eficiencia requerida, obteniendo el
									m&aacute;ximo provecho de los recursos asignados, en el
									cumplimiento con los indicadores de trato digno, basados en los
									c&oacute;digos &eacute;ticos y legales acordes con los
									lineamientos establecidos por la Instituci&oacute;n.
								</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/archivo.clinico/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>																	
				</div>			
			<section class="consulta-izq">
				<div class="barra-01"
					style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-01-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Servicio de Consulta Externa</p>
					<br />
				</div>
				<br>
				<p class="texto sangria">Se hace necesario que la Jefatura del
					Servicio cuente con un instrumento administrativo que sirva como
					documento dise&ntilde;o para ilustrar la estructura org&aacute;nica
					del servicio y formalizar su organizaci&oacute;n, asignando niveles
					jer&aacute;rquicos, funciones y actividades para lograr una mejora
					continua en beneficio de la poblaci&oacute;n que acude a este
					Centro Hospitalario, ya que las funciones curativas tienen como fin
					efectuar un diagn&oacute;stico temprano y tratamiento oportuno,
					siendo la Consulta Externa el primer contacto que tiene la
					poblaci&oacute;n con la Instituci&oacute;n.</p>
				<br>
				<div class="barra-01"
					style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-01-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Consultorios</p>
					<br />
				</div>
				<br>

				<table class="consulta-externa">
					<tr>
						<th>Consulta externa 1</th>
						<th>Consulta externa 2</th>
					</tr>
					<tr>
						<td>1.- Trabajo social</td>
						<td>20.- Jefatura de enfermer&iacute;a</td>
					</tr>
					<tr>
						<td>2.- Somatometria</td>
						<td>21.- Somatometria</td>
					</tr>
					<tr>
						<td>3.- Medicina preventiva y juridico</td>
						<td>22.- Ginecolog&iacute;a</td>
					</tr>
					<tr>
						<td>4.- Pediatr&iacute;a</td>
						<td>23.- Ginecolog&iacute;a</td>
					</tr>
					<tr>
						<td>5.- Oftalmolog&iacute;a</td>
						<td>24.- Ginecolog&iacute;a</td>
					</tr>
					<tr>
						<td>6.- Psicolog&iacute;a</td>
						<td>25.- Odontopediatr&iacute;a</td>
					</tr>
					<tr>
						<td>7.- Neurocirug&iacute;a</td>
						<td>26.- Planificaci&oacute;n familiar</td>
					</tr>
					<tr>
						<td>8.- Tamiz neonatal</td>
						<td>27.- Vacunas</td>					
					</tr>
					<tr>
						<td>9.- Otorrinolaringolog&iacute;a</td>
						<td>28.- Medicina preventiva</td>
					</tr>
					<tr>
						<td>10.- Cardiolog&iacute;a</td>
						<td>29.- Tamiz</td>
					</tr>
					<tr>
						<td>11.- Angiolog&iacute;a</td>
						<td>30.- Dermatolog&iacute;a</td>
					</tr>
					<tr>
						<td>12.- Electrocardiograma</td>
						<td>31.- Endocrinolog&iacute;a</td>
					</tr>
					<tr>
						<td>13.- Neumolog&iacute;a / Gen&eacute;tica</td>
						<td>32.- Medicina interna</td>
					</tr>
					<tr>
						<td>14.- Labio y paladar hendido</td>
						<td>33.- Medicina interna</td>
					</tr>
					<tr>
						<td>15.- Traumatolog&iacute;a</td>
						<td>34.- Medicina interna</td>
					</tr>
					<tr>
						<td>16.- Cirug&iacute;a reconstructiva</td>
						<td>35.- Nefrolog&iacute;a</td>
					</tr>
					<tr>
						<td>17.- Cirug&iacute;a general</td>
						<td>36.- Dermatolog&iacute;a</td>
					</tr>
					<tr>
						<td>18.- Cirug&iacute;a general</td>
						<td>37.- Urolog&iacute;a</td>
					</tr>
					<tr>
						<td colspan="2">19.- Oncolog&iacute;a m&eacute;dica quir&uacute;rgica / Estimulaci&oacute;n temprana</td>
					</tr>
				</table>

			</section>
			<section class="consulta-der">
				<div class="barra-02"
					style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-02-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Organigrama</p>
					<br />
				</div>
				<img alt=""
					src="../../img/servicios/consulta.externa/organigrama-thumb.png"
					width="328" class="imgshadow" style="margin: 10px 0" > <a id="btn-zoom" class="btn">Zoom</a> <br />
				<br />
				<div class="barra-02"
					style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-02-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">C&oacute;digo de &Eacute;tica</p>
					<br />
				</div>
				<br>
				<p class="bullet">
					<span class="ui-icon ui-icon-radio-on"
						style="float: left; margin-right: .3em;"></span><a
						href="codigo.etica.php">C&oacute;digo de &Eacute;tica de los
						Servidores P&uacute;blicos</a>
				</p>
				<br>
				<div class="ui-state-highlight ui-corner-all"
					style="margin-top: 0px; padding: 0.7em; width: 320px;">
					<p>
						<span class="ui-icon ui-icon-info"
							style="float: left; margin-right: .3em;"></span> <strong>Nota!</strong>
					</p>
					<br /> * Publicado en el Diario Oficial de la Federaci&oacute;n el
					31 de Julio de 2002.
				</div>
				<br>
				<div class="barra-02"
					style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-02-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Galer&iacute;a de Imagenes</p>
					<br />
				</div>
			 	<a href="galeria.php"><img
					src="../../img/servicios/consulta.externa/galeria.jpg" width="328" class="imgshadow" style="margin: 10px 0"
					title="Ir a la galeria" /> </a>
			</section>
		</div>

<!-- 
		<div style="float: left; margin-left: 15px;">
			<div style="float: left">
				<div class="barra-01"
					style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-01-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Planta Baja</p>
				</div>
				<br> <img src="../../img/servicios/consulta.externa/planta baja.png"
					width="600" height="419" alt="planta" usemap="#ConsultasPlantaBaja" />

				<map name="ConsultasPlantaBaja">
					<area shape="rect" coords="502,28,547,170" href="#"
						onmouseover="opcion(1,0)" onMouseOut="recepcion()" />
					<area shape="rect" coords="500,300,547,410" href="#"
						onmouseover="opcion(1,1)" onMouseOut="recepcion()" />
				</map>
			</div>
			<div style="float: right; margin-left: 10px;">
				<div id="planta-Baja">
					<div class="barra-02"
						style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
						<img alt="" src="../../img/content/barra-02-r.png"
							style="float: right; z-index: 0">
						<p class="titulo-barra-01">Informaci&oacute;n</p>
						<br />
					</div>
					<br>
				</div>
				<div>
					<div class="ui-state-highlight ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; width: 320px; height: 395px;">
						<p id="textoPlanta-baja"></p>
					</div>
				</div>
			</div>
		</div>
	 -->	
		
		<!-- ###################################################Consulta externa 1 informacion ##############################################################################-->
				
		<div style="float: left; margin-left: 15px; margin-top: 0px;">
			<div style="float: left">
				<div class="barra-01"
					style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-01-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Consulta Externa 1</p>
					<br />
				</div>
				<br> <img src="../../img/servicios/consulta.externa/cons-ext-1.png"
					width="593" height="419" alt="exter-1" usemap="#ConsultasExterna-1" />

				<map name="ConsultasExterna-1">
					<area shape="rect" coords="282,67,315,106" href="#"
						onmouseover="opcion(1,0)" onMouseOut="recepcion()" alt="Consulta Externa 1" />
					<area shape="rect" coords="319,20,360,106" href="#"
						onmouseover="opcion(1,1)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="361,20,402,106" href="#"
						onmouseover="opcion(1,2)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="404,20,445,106" href="#"
						onmouseover="opcion(1,3)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="447,20,488,106" href="#"
						onmouseover="opcion(1,4)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="490,57,525,106" href="#"
						onmouseover="opcion(1,5)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="poly"
						coords="492,20,532,19,532,43,568,44,568,117,528,117,529,55,492,55"
						href="#" onmouseover="opcion(1,6)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="528,121,567,149" href="#"
						onmouseover="opcion(1,7)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="528,153,567,236" href="#"
						onmouseover="opcion(1,8)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="528,238,567,306" href="#"
						onmouseover="opcion(1,9)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="528,304,567,393" href="#"
						onmouseover="opcion(1,10)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="473,309,524,393" href="#"
						onmouseover="opcion(1,11)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="429,309,470,393" href="#"
						onmouseover="opcion(1,12)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="376,309,426,393" href="#"
						onmouseover="opcion(1,13)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="323,309,373,393" href="#"
						onmouseover="opcion(1,14)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="282,309,320,393" href="#"
						onmouseover="opcion(1,15)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="236,309,278,392" href="#"
						onmouseover="opcion(1,16)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="193,309,234,392" href="#"
						onmouseover="opcion(1,17)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>
					<area shape="rect" coords="148,309,190,392" href="#"
						onmouseover="opcion(1,18)" onMouseOut="recepcion()" alt="Consulta Externa 1"/>						
				</map>
			</div>

			<div style="float: right; margin-left: 10px;">
				<div id="info-Consulta-Exter-1" style="margin-top: 2px;">
					<div class="barra-02"
						style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
						<img alt="" src="../../img/content/barra-02-r.png"
							style="float: right; z-index: 0">
						<p class="titulo-barra-01">Informaci&oacute;n</p>
						<br />
					</div>
				</div>
				<br>
				<div>
					<div class="ui-widget-content ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; width: 320px; height: 395px;">
						<p id="textoExterno-1"></p>
					</div>
				</div>
			</div>
		</div>
		<!-- ###################################################Consulta externa 2 informacion ##############################################################################-->
		<div style="float: left; margin-left: 15px; margin-top: 10px;">
			<div style="float: left">
				<div class="barra-01"
					style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">
					<img alt="" src="../../img/content/barra-01-r.png"
						style="float: right; z-index: 0">
					<p class="titulo-barra-01">Consulta Externa 2</p>
					<br />
				</div>
				<br> <img src="../../img/servicios/consulta.externa/cons-ext-2.png"
					width="600" height="419" alt="Planets" usemap="#ConsultasExterna-2" />

				<map name="ConsultasExterna-2">
					<area shape="rect" coords="277,68,327,109" href="#"
						onmouseover="opcion(2,0)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="237,61,276,110" href="#"
						onmouseover="opcion(2,1)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="194,26,234,110" href="#"
						onmouseover="opcion(2,2)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="146,26,191,110" href="#"
						onmouseover="opcion(2,3)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="101,25,143,112" href="#"
						onmouseover="opcion(2,4)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="37,26,99,110" href="#"
						onmouseover="opcion(2,5)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="37,113,75,195" href="#"
						onmouseover="opcion(2,6)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="37,198,75,287" href="#"
						onmouseover="opcion(2,7)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="37,290,75,361" href="#"
						onmouseover="opcion(2,8)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="78,310,107,392" href="#"
						onmouseover="opcion(2,9)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="110,310,149,392" href="#"
						onmouseover="opcion(2,10)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="152,310,193,392" href="#"
						onmouseover="opcion(2,11)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="195,310,234,392" href="#"
						onmouseover="opcion(2,12)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="237,310,277,392" href="#"
						onmouseover="opcion(2,13)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="281,310,318,392" href="#"
						onmouseover="opcion(2,14)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="322,310,363,392" href="#"
						onmouseover="opcion(2,15)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="366,310,406,392" href="#"
						onmouseover="opcion(2,16)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="409,310,451,392" href="#"
						onmouseover="opcion(2,17)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
					<area shape="rect" coords="235,26,275,60" href="#"
						onmouseover="opcion(2,18)" onMouseOut="recepcion()" alt="Consulta Externa 2"/>
				</map>
			</div>

			<div style="float: right; margin-left: 10px;">
				<div id="info-Consulta-Exter-2" style="margin-top: 2px;">
					<div class="barra-02"
						style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">
						<img alt="" src="../../img/content/barra-02-r.png"
							style="float: right; z-index: 0">
						<p class="titulo-barra-01">Informaci&oacute;n</p>
						<br />
					</div>
				</div>
				<br>
				<div>
					<div class="ui-widget-content ui-corner-all"
						style="margin-top: 0px; padding: 0.7em; width: 320px; height: 395px;">
						<p id="textoExterno-2"></p>
					</div>
				</div>
			</div>
		</div>
				
			<?php 
				$objCabecera->pie();
			?>		
		</div>
</body>
</html>
