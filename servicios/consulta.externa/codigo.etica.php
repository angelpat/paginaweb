<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/servicio-consulta-externa.js"></script>
		<script src="../../js/tip-salud.js"></script>		
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>		
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    
		<div id="modal-tip"></div>	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<div id="content">			
				<section class="consulta-izq">

					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">C&oacute;digo de &Eacute;tica de los Servidores P&uacute;blicos</p><br/>	
					</div><br>	

					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* Publicado en el Diario Oficial de la Federaci&oacute;n el 31 de Julio de 2002.																
					</div><br>

					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Bien Com&uacute;n</p><br/>	
					</div><br>	
					<p class="texto sangria">
						Todas las decisiones y acciones del servidor p&uacute;blico deben estar dirigidas a la satisfacci&oacute;n de las necesidades e intereses de la sociedad, por encima de intereses particulares ajenos al bienestar de la colectividad. 
						El servidor p&uacute;blico no debe permitir que influyan en sus juicios y conducta, intereses que puedan perjudicar o beneficiar a personas o grupos en detrimento del bienestar de la sociedad. El compromiso con el bien com&uacute;n 
						implica que el servidor p&uacute;blico est&eacute; consciente de que el servicio p&uacute;blico es un patrimonio que pertenece a todos los mexicanos y que representa una misi&oacute;n que s&oacute;lo adquiere legitimidad cuando busca 
						satisfacer las demandas sociales y no cuando se persiguen beneficios individuales.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Integridad</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe actuar con honestidad, atendiendo siempre a la verdad. Conduci&eacute;ndose de esta manera, el servidor p&uacute;blico fomentar&aacute; la credibilidad de la sociedad en las instituciones p&uacute;blicas 
						y contribuir&aacute; a generar una cultura de confianza y de apego a la verdad.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Honradez</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico no deber&aacute; utilizar su cargo p&uacute;blico para obtener alg&uacute;n provecho o ventaja personal o a favor de terceros. Tampoco deber&aacute; buscar o aceptar compensaciones o prestaciones de cualquier 
						persona u organizaci&oacute;n que puedan comprometer su desempe&ntilde;o como servidor p&uacute;blico.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Imparcialidad</p><br/>	
					</div><br>	 
					<p class="texto sangria">
						El servidor p&uacute;blico actuar&aacute; sin conceder preferencias o privilegios indebidos a organizaci&oacute;n o persona alguna. Su compromiso es tomar decisiones y ejercer sus funciones de manera objetiva, sin prejuicios personales 
						y sin permitir la influencia indebida de otras personas.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Justicia</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe conducirse invariablemente con apego a las normas jur&iacute;dicas inherentes a la funci&oacute;n que desempe&ntilde;a. Respetar el Estado de Derecho es una responsabilidad que, m&aacute;s que nadie, debe asumir 
						y cumplir el servidor p&uacute;blico. Para ello, es su obligaci&oacute;n conocer, cumplir y hacer cumplir las disposiciones jur&iacute;dicas que regulen el ejercicio de sus funciones.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Transparencia</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe permitir y garantizar el acceso a la informaci&oacute;n gubernamental, sin m&aacute;s l&iacute;mite que el que imponga el inter&eacute;s p&uacute;blico y los derechos de privacidad de los particulares 
						establecidos por la ley. La transparencia en el servicio p&uacute;blico tambi&eacute;n implica que el servidor p&uacute;blico haga un uso responsable y claro de los recursos p&uacute;blicos, eliminando cualquier discrecionalidad indebida en su aplicaci&oacute;n.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Rendici&oacute;n de Cuentas</p><br/>	
					</div><br>	
					<p class="texto sangria">
						Para el servidor p&uacute;blico rendir cuentas significa asumir plenamente ante la sociedad, la responsabilidad de desempe&ntilde;ar sus funciones en forma adecuada y sujetarse a la evaluaci&oacute;n de la propia sociedad. Ello lo obliga a realizar sus funciones con 
						eficacia y calidad, as&iacute; como a contar permanentemente con la disposici&oacute;n para desarrollar procesos de mejora continua, de modernizaci&oacute;n y de optimizaci&oacute;n de recursos p&uacute;blicos.
					</p><br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Entorno Cultural y Ecol&oacute;gico</p><br/>	
					</div><br>	
					<p class="texto sangria">
						Al realizar sus actividades, el servidor p&uacute;blico debe evitar la afectaci&oacute;n de nuestro patrimonio cultural y del ecosistema donde vivimos, asumiendo una f&eacute;rrea voluntad de respeto, defensa y preservaci&oacute;n de la cultura y del medio 
						ambiente de nuestro pa&iacute;s, que se refleje en sus decisiones y actos. Nuestra cultura y el entorno ambiental son nuestro principal legado para las generaciones futuras, por lo que los servidores p&uacute;blicos tambi&eacute;n tienen 
						la responsabilidad de promover en la sociedad su protecci&oacute;n y conservaci&oacute;n.
					</p><br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Generosidad</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe conducirse con una actitud sensible y solidaria, de respeto y apoyo hacia la sociedad y los servidores p&uacute;blicos con quienes interact&uacute;a. Esta conducta debe ofrecerse con especial atenci&oacute;n hacia las personas 
						o grupos sociales que carecen de los elementos suficientes para alcanzar su desarrollo integral, como los adultos en plenitud, los ni&ntilde;os, las personas con capacidades especiales, los miembros de nuestras etnias y quienes menos tienen.
					</p><br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Igualdad</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe prestar los servicios que se le han encomendado a todos los miembros de la sociedad que tengan derecho a recibirlos, sin importar su sexo, edad, raza, credo, religi&oacute;n o preferencia pol&iacute;tica. No debe permitir que 
						influyan en su actuaci&oacute;n, circunstancias ajenas que propicien el incumplimiento de la responsabilidad que tiene para brindar a quien le corresponde los servicios p&uacute;blicos a su cargo.
					</p><br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Respeto</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe dar a las personas un trato digno, cort&eacute;s, cordial y tolerante. Est&aacute; obligado a reconocer y considerar en todo momento los derechos, libertades y cualidades inherentes a la condici&oacute;n humana.
					</p><br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Liderazgo</p><br/>	
					</div><br>	
					<p class="texto sangria">
						El servidor p&uacute;blico debe convertirse en un decidido promotor de valores y principios en la sociedad, partiendo de su ejemplo personal al aplicar cabalmente en el desempe&ntilde;o de su cargo p&uacute;blico este C&oacute;digo de &eacute;tica y el 
						C&oacute;digo de Conducta de la instituci&oacute;n p&uacute;blica a la que est&eacute; adscrito. El liderazgo tambi&eacute;n debe asumirlo dentro de la instituci&oacute;n p&uacute;blica en que se desempe&ntilde;e, fomentando aquellas conductas que promuevan 
						una cultura &eacute;tica y de calidad en el servicio p&uacute;blico. El servidor p&uacute;blico tiene una responsabilidad especial, ya que a trav&eacute;s de su actitud, actuaci&oacute;n y desempe&ntilde;o se construye la confianza de los ciudadanos en sus instituciones.
					</p><br>
	
				</section>																		
				<section class="consulta-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>
    				<img alt="" src="../../img/servicios/consulta.externa/organigrama-thumb.png" width="328" class="imgshadow" style="margin: 10px 0" >
    				<br/>
    				<a id="btn-zoom" class="btn">Zoom</a> <br/>    			    			
					<br/>	
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Galer&iacute;a de Imagenes</p><br/>	
					</div>
					<a href="galeria.php"><img src="../../img/servicios/consulta.externa/galeria.jpg" width="328" class="imgshadow" style="margin: 10px 0" title="Ir a la galeria"/></a>
					<br>
					<?php echo $objCabecera->tipSalud("../../") ?>
    			</section>	  		    				
			</div>
			<?php 
				$objCabecera->pie();
			?>	
		</div>               	 
    </body>
</html>