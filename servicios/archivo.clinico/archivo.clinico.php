<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/servicio-archivo-clinico.js"></script>
		<script src="../../js/tip-salud.js"></script>				
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<div id="modal-tip"></div>	
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-archivo">
				<section class="archivo-izq" >	
				 	<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Archivo Cl&iacute;nico</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">C. Ver&oacute;nica Herrera Ch&aacute;vez &nbsp; &nbsp; &nbsp;Archivo Cl&iacute;nico</p>
				 	</div>					 					 		
				</section>
				<section class="archivo-der">
					<img src="../../img/servicios/archivo.clinico/header-archivo.png" width="340" height="96" alt="Archivo">
				</section>	 	
			</header>
			
			<div id="content">
			
				<div id="features">			
					<div>
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Somos un servicio que proporciona atenci&oacute;n administrativa de calidad y eficiencia a los usuarios internos y externos del hospital general con especialidades &quot;Juan Maria de Salvatierra&quot; </p>							
							</div>							
						</div>
						<div class="features-der" >
							<div>
								<img src="../../img/servicios/archivo.clinico/mision.png" alt="Mision" />
							</div>	
						</div>
					</div>				
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Seremos un servicio acreditado del hospital, con amplia capacidad resolutiva, que prestar&aacute; sus servicios administrativos con la mejor tecnolog&iacute;a en un clima laboral de respeto y calidad en la atenci&oacute;n de los usuarios.</p>														
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/archivo.clinico/vision.png" alt="Vision" />
							</div>
						</div>
					</div>							
				</div>									
				<section class="archivo-izq">
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">&Aacute;rea de recepci&oacute;n</p><br/>	
					</div><br>							

					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Se encargan de programar cita con el especialista con previa interconsulta solicitada de esta instituci&oacute;n  o referencia otorgada del primer nivel y  da toda la informaci&oacute;n correspondiente a su cita.
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Atiende solicitudes de  usuarios for&aacute;neos  telef&oacute;nicamente y les programa cita.
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Programa citas para usuarios que solicitan v&iacute;a correo electr&oacute;nico.	
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>As&iacute; mismo  lleva el control de la programaci&oacute;n de electrocardiogramas, pruebas de esfuerzo y eco cardiogramas.
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Cabe mencionar  que  todo es por  sistemas  electr&oacute;nicos.
					</p><br>										
					 
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* El &aacute;rea de recepci&oacute;n, labora de 07:30 a 20:00 hrs. de lunes a viernes <br> * S&aacute;bados y domingos de 08:00 a 14:00 hrs.
					</div>
					<br>					
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0">
						<p class="titulo-barra-01">&Aacute;rea de archivo cl&iacute;nico</p><br/>	
					</div><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Otorga atenci&oacute;n al usuario  en cuanto a informaci&oacute;n general.
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Es el encargado de registrar a todo paciente en el sistema para que este cuente con un expediente electr&oacute;nico.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Recibe solicitudes por correo electr&oacute;nico  para especialistas y de igual manera se encarga de capturar datos de los usuarios de primera vez y as&iacute; mismo les env&iacute;a respuesta.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Organiza y distribuye las fichas  solicitadas por los usuarios, por m&eacute;dico y horarios.
					</p><br>										
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Rastrea resultados de patolog&iacute;a, certificados de nacimiento y todo documento relacionado  con la atenci&oacute;n del usuario.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Atiende solicitudes de personal administrativo, m&eacute;dico y autoridades  del hospital para realizaci&oacute;n de tesis, res&uacute;menes cl&iacute;nicos u otros documentos oficiales.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Resguarda los documentos relacionados con la atenci&oacute;n del usuario.
					</p><br>								
	
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* El personal de archivo cl&iacute;nico labora de 07:00 a 20:00 hrs.
						<div > 
						* Con turno matutino, vespertino y el personal de consulta externa cubre tambien el &aacute;rea de archivo en el turno diurno de s&aacute;bado y domingo.
						</div>																
					</div>
					<br>

					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">&Aacute;rea de admisi&oacute;n programados</p><br/>	
					</div><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Informa a los usuarios que ser&aacute;n intervenidos quir&uacute;rgicamente, si tienen programaci&oacute;n y les da indicaciones de ingreso, como d&iacute;a y hora de valoraci&oacute;n preanestesica, donaci&oacute;n de sangre y horarios de ingreso. Ya sea telef&oacute;nicamente o personalmente.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza todo el tr&aacute;mite para la realizaci&oacute;n del ingreso y explica los mecanismos en cuanto a pacientes particulares y seguro popular y asigna la cama en el sistema,
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Localiza cama para los pacientes.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Informa a caja de los ingresos realizados ya sea interno o externo para el registros del mismo.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza el tr&aacute;mite de egresos del paciente, como arreglo de expediente  f&iacute;sico, alta en el sistema e informe de los mismos.
					</p><br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Admisi&oacute;n urgencias</p><br/>	
					</div><br>	
					<p class="texto sangria">	
					En esta &aacute;rea de realizan los ingresos  de pacientes que permanecieron en el &aacute;rea de urgencias y se decide que ser&aacute; ingresado,  los ni&ntilde;os que nacen y presentan problemas por lo cual requieren el ingreso a la unidad y los traslados previa valoraci&oacute;n de los m&eacute;dicos de urgencias.
					</p><br>										
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza toda la papeler&iacute;a necesaria para el ingreso explic&aacute;ndole al  usuario lo que corresponde a cada una de ellas.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Le corresponde simplificar los tr&aacute;mites, brindarle el apoyo administrativo necesario a la indicaciones m&eacute;dicas y crear una buena imagen de atenci&oacute;n.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Le busca cama y lo registra en el sistema para informaci&oacute;n de  las dem&aacute;s a&eacute;reas.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Se encarga de las pertenencias en caso que el usuario no cuente con familiar en el momento del ingreso y se encarga de localizarlos de inmediato para la entrega de las mismas.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Es el encargado de realizar todos los tramites en caso de defunci&oacute;n, como elaboraci&oacute;n del certificado de defunci&oacute;n y le da seguimiento hasta entregar el cad&aacute;ver a la funeraria o a quien corresponda (ministerio publico si se trata de caso legal)
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza el censo f&iacute;sico al termino del d&iacute;a para que el sistema otorgue informaci&oacute;n veraz y ver&iacute;dica a las dem&aacute;s a&eacute;reas en cuanto a la poblaci&oacute;n hospitalaria
					</p><br>	
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* En los turnos nocturnos, realiza las funciones de trabajo social (de 20:00 a 08:00 hrs.)																
					</div>
					<br>

					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Admisi&oacute;n toco/cirug&iacute;a.</p><br/>	
					</div><br>	

					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>De igual manera es el primer contacto con el usuario.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Registra y lleva el control de las consultas, trazos etc. Que se otorgan en esta &aacute;rea.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza todos los tr&aacute;mites de ingreso al momento que el m&eacute;dico decide el ingreso.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Tiene el control de camas ocupadas mediante el sistema electr&oacute;nico.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realiza los cambios del &aacute;rea toco a pisos y asigna la cama.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>En los turnos nocturnos realiza funciones de trabajo social de 20.00 a 08:00 hrs.
					</p><br>					

					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* Cabe mencionar que el departamento de admisi&oacute;n hospitalaria, urgencias y toco cirug&iacute;a realizan las funciones de trabajo social en los turnos nocturno.																
					</div>
					<br>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Estad&iacute;stica</p><br/>	
					</div><br>	

					<p class="texto sangria">
						Son los encargados de mantener en forma oportuna, completa y veraz la informaci&oacute;n estad&iacute;stica de recursos, servicios y de morbi-mortalidad  hospitalaria  que sirvan de base para la toma de decisiones.
						<br><br>
						En sus actividades espec&iacute;ficas:
					</p>
					<br>


					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Mandar en tiempo y forma todos los informes solicitados a nivel nacional, como el sis, sinac, saeh hospital, saeh urgencias, etc.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Recaba, tabula y analiza la informaci&oacute;n estad&iacute;stica sobre la productividad por m&eacute;dico, turno y servicio.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Analiza las actividades llevadas a cabo en los diferentes  servicios del hospital, y confrontarlas con las metas establecidas con el fin de conocer los avances y productividad  de la unidad.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elabora y proporciona material estad&iacute;stico solicitado para el desarrollo de auditor&iacute;as m&eacute;dicas, protocolos de investigaci&oacute;n, previa  autorizaci&oacute;n.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elabora tablas comparativas con los &iacute;ndices reconocidos, para evaluar el desempe&ntilde;o de los servicios.
					</p><br>

					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* El &aacute;rea de estad&iacute;stica labora solo en el turno matutino de 08:00 a 14:30 hrs.																
					</div>
					<br>					
					
					
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">&Aacute;rea de triage administrativo</p><br/>	
					</div><br>	

					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Es el primer contacto del usuario en busca de una consulta en el servicio de urgencias.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Registra datos en el sistema ya establecido y  asigna los turnos de la consulta.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Registra las atenciones  en el sistema saeh de urgencias.
					</p><br>	
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Este servicio labora las 24 horas.
					</p><br>

					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* El &aacute;rea del triage administrativo, labora  las 24 horas del d&iacute;a y est&aacute; ubicado en el &aacute;rea de urgencias m&eacute;dica y es el encargado de realizar el saeh (sistema automatizado hospitalizaci&oacute;n )-urgencias																
					</div>
				</section>																		
				<section class="archivo-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0">
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div> 
    				<img alt="Organigrama" src="../../img/servicios/archivo.clinico/organigrama.png" width="328" class="imgshadow" style="margin: 10px 0;">    				
    				<a id="btn-zoom" class="btn">Zoom</a><br><br> 			    			
					<?php echo $objCabecera->tipSalud("../../") ?>
    			</section>	  		    				
			</div>
			<?php 
				$objCabecera->pie();
			?>
		</div>               	 
    </body>
</html>