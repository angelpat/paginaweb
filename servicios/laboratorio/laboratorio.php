<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/tooltip.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/servicio-laboratorio.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-laboratorio"></section>
    	<section id="modal-ingreso"></section>
    	<section id="modal-donacion"></section>		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-laboratorio">
				<section class="laboratorio-izq" >				 
				 	<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Laboratorio de An&aacute;lisis Cl&iacute;nicos</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Q.F.B. Maria Maricela Zaragoza Nu&ntilde;ez &nbsp; &nbsp; &nbsp;Jefa de Laboratorio</p>
				 	</div>					 				 	 			
				</section>
				<section class="laboratorio-der">
					<img src="../../img/servicios/laboratorio/header-laboratorio.png" width="340" height="96" alt="Laboratorio">
				</section>	 	
			</header>
			<div id="content">
				<div id="features">							
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p> Nuestro compromiso es obtener resultados confiables y oportunos que sean de apoyo al m&eacute;dico y al paciente en el diagn&oacute;stico, utilizando m&eacute;todos validados y certificados con equipo especificado de avanzada tecnolog&iacute;a y con personal capacitado en constante actualizaci&oacute;n.
									Comprometidos con la salud y satisfacci&oacute;n de nuestros usuarios, ofrecemos un servicio basado en un sistema de calidad que cumple con requisitos normalizados.
								</p>	
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/laboratorio/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Ser un Laboratorio de An&aacute;lisis Cl&iacute;nico l&iacute;der en el Estado de Baja California Sur, con la capacidad de cubrir eficientemente las necesidades actuales y futuras de nuestros usuarios y con la garant&iacute;a de ofrecer siempre validez y confiabilidad en los resultados y distinguirnos por la aplicaci&oacute;n constante de principios de calidad en nuestro trabajo.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/servicios/laboratorio/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>					
				
				<section class="laboratorio-izq">
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Servicio de Laboratorio</p><br/>	
					</div><br>
					<p class="texto sangria">
						El objetivo fundamental del Laboratorio de An&aacute;lisis Cl&iacute;nicos del Hospital Juan Mar&iacute;a de Salvatierra para el 2012 es Adecuar y completar el sistema de gesti&oacute;n de la calidad establecida para el cumplimiento de las normas oficiales y de acreditaci&oacute;n NOM 166-SSA-2002 para la organizaci&oacute;n y funcionamiento de Laboratorio Cl&iacute;nico NMX EC- 15189 Laboratorio Cl&iacute;nicos requisitos particulares para la calidad y competencia.						
					</p>
					<br/>
					<p class="texto sangria">
						Nuestro sistema de gesti&oacute;n de calidad esta apoyado b&aacute;sicamente en el manual de calidad en la elaboraci&oacute;n de los procedimientos normalizados de trabajo en el cumplimiento del programa de calidad establecido.
					</p><br>
					 <div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">&iquest;C&oacute;mo solicitar el servicio de laboratorio?</p><br/>	
					</div>
					 
					 	<a href="#" class="tooltip" title="Paso No.1" >
					 		<img alt="" class="paso-lab paso-1" src="../../img/servicios/laboratorio/paso-1-thumb.jpg" width="176"/>
					 	</a>				 	 	
				 	 	<a href="#" class="tooltip" title="Paso No.2" >
							<img alt="" class="paso-lab paso-2" src="../../img/servicios/laboratorio/paso-2-thumb.jpg" width="176" />
						</a>
						<a href="#" class="tooltip" title="Paso No.3" >												
							<img alt="" class="paso-lab paso-3" src="../../img/servicios/laboratorio/paso-3-thumb.jpg" width="176" />
						</a><br/>	
						<a href="#" class="tooltip" title="Paso No.4" >						
							<img alt="" class="paso-lab paso-4" src="../../img/servicios/laboratorio/paso-4-thumb.jpg" width="176" />
						</a>
						<a href="#" class="tooltip" title="Paso No.5" >						
							<img alt="" class="paso-lab paso-5" src="../../img/servicios/laboratorio/paso-5-thumb.jpg" width="176" />
						</a>
						<a href="#" class="tooltip" title="Paso No.6" >
							<img alt="" class="paso-lab paso-6" src="../../img/servicios/laboratorio/paso-6-thumb.jpg" width="176" />
						</a>	

						<p class="paso-desc paso-1">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							1.- Presentarse en recepci&oacute;n de Laboratorio.
						</p><br/>
						<p class="paso-desc paso-2">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							2.- Entregar tu solicitud m&eacute;dica.
						</p><br/>
						<p class="paso-desc paso-3">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							3.- Entregar tus frascos para etiquetarlos.
						</p><br/>
						<p class="paso-desc paso-4">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							4.- Presentarse a las 7:15 AM para tomar turno.
						</p><br/>
						<p class="paso-desc paso-5">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							5.- Entrega de resultados.
						</p><br/>
						<p class="paso-desc paso-6">
							<span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							6.- Programaci&oacute;n de citas.
						</p><br/>
						<!-- 
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
							<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
							<strong>Nota!</strong></p><br/>
							* Para una descripci&oacute;n mas detallada haga click sobre las imagenes																		
						</div> -->										
				</section>																		
				<section class="laboratorio-der">
				
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>
    				<br>  
    				<img alt="" src="../../img/servicios/laboratorio/organigrama-thumb.png" class="imgshadow" style="width: 330; margin-top: 10px;">
    				<br/><br/>
    				<a id="btn-zoom" class="btn">Zoom</a> <br/>	<br/>				
					
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">																				
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Cat&aacute;logo de estudios</p><br/>	
					</div>
					<br>
					<p class="texto sangria">
						Este hospital cuenta con un amplio cat&aacute;logo de estudios que se realizan en el &Aacute;rea de Laboratorio, a continuaci&oacute;n se muestra un archivo que cuenta con lo siguiente:						
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Hoja 1 - Estudios Generales.
					</p><br>
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Hoja 2 - Cat&aacute;logo de estudios completo.
					</p><br>					
					<p class="bullet">
						<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Hoja 3 - Perfiles.
					</p>					
					<br>
					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:320px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* Descargar archivo
					<a href="../../resources/servicios/CatalogoV6.xls" target="#" style="text-decoration: none;">					
					<img src="../../img/icon/xls.png" alt="CatalogoV6.xls" width="16"> CatalogoV6.xls</a>																		
					</div>
					
					<br>
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Donaci&oacute;n de Sangre</p><br/>	
					</div>
					<br>
					<img alt="" class ="donacion-1 requisitos-1 donacion-lab" src="../../img/servicios/laboratorio/requisitos-1-thumb.png" width="162" />	
					
					<img alt="" class ="donacion-2 requisitos-2 donacion-lab" src="../../img/servicios/laboratorio/requisitos-2-thumb.png" width="162" style="padding-left: 10px;" />
					<br><br>
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; width:320px;" > 
					<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Nota!</strong></p>
					<div >
						<p style="margin-left: 1.6em;">* Para obtener una copia el archivo haga clic en 
							<strong><a target="_blank" href="../../resources/servicios/Requisitos-Productos-Sanguineos.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="Requisitos-Productos-Sanguineos.pdf" title="Requisitos-Productos-Sanguineos.pdf" width="16" height="16"> descargar</a></strong>
						</p>
						<p style="margin-left: 1.6em;">* Documento en hoja oficio
						</p>												
					</div>
				</div>								    											    			
    			</section>
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>