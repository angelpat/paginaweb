<?php 
include_once 'cls/clsContadorVisitas.php';
include_once 'cls/clsCabecera.php';
$objContador = new Contador();
$objCabecera = new Cabecera();
$dominio = $_SERVER['SERVER_NAME'];
$pagina = $_SERVER['REQUEST_URI'];
$url = "http://" . "$dominio" . "$pagina";
$objContador->insertContadorVisitas($url);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />			
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script src="js/menu.js"></script>   		    
        <script src="js/jquery.imageLens.js"></script>     
		<script>
			$(function(){
				$(document).ready(function() {	 
				    $('#organigrama').imageLens({ lensSize: 300 }); // set lens size
				});
					
				$( "#modal-contacto" ).dialog({
					autoOpen: false,
					show: "blind",
					hide: "explode"
				});					
					
				$( "#menu-contact" ).click(function(){
					$( "#modal-contacto" ).dialog( "open" );
					return false;		
				});	
				
			});		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>      
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>      
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("0");		
				$objCabecera->menu("");			
			?>				
			<div id="content">	
				<div class="barra-01" style="background-image: url('img/content/barra-01-l.png'); margin: auto; width: 470px;">													
					<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
					<p class="titulo-barra-01" style="text-align: center;">ORGANIGRAMA</p><br/>	
				</div><br/>			
				<p class="organigrama-hgjms">BENEM&Eacute;RITO HOSPITAL GENERAL CON ESPECIALIDADES "JUAN MARIA DE SALVATIERRA"</p><br/>			
				<img id="organigrama" src="img/content/organigrama-2012.png" style="width:950px;" alt="Organigrama"/>
				<br/><br/>				
				<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em; font-size: 95%; width:600px;" > 
					<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
					<strong>Nota!</strong></p>
					<div >* Ultima actualizaci&oacute;n Abril de 2012
						<p style="margin-left: 1.6em;">* Para obtener una copia el archivo haga clic en 
							<strong><a target="_blank" href="resources/content/Organigrama-2012-04-18.pdf" class="organigrama"><img src="img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> descargar</a></strong>
						</p>												
					</div>
				</div>									
			</div>
			<?php 
				$objCabecera->pie();
			?>	
		</div>               	 
    </body>
</html>