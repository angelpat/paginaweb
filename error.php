<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="css/master.css" />
		<script src="js/jquery-1.6.2.min.js"></script>
		<script src="js/jquery-ui-1.8.16.custom.min.js"></script>     
		<script>		
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");
		</script>	
 		<title>HOSPITAL SALVATIERRA</title>
 		<link rel='shortcut icon' href='img/icon/shgjms.ico' type='image/x-icon' />    </head>
    <body>           
		<div id="wrapper">
			<header class="hgjms">
				<a href="http://saludbcs.gob.mx/"><img src="img/content/logo-secretaria-salud.png" class="link-secretaria-salud" border="none"></a>
				<img src="img/content/logo-bcs.png" class="link-bcs">
			</header>			
			<nav class="hgjms">	 
				 <table id="tabla-menu">
				 	<tr>
				 		<td id="menu-home"><a href="index.php">INICIO</a></td>
				 		<td id="menu-about-us"><a href="quienes.somos.php">QUIENES SOMOS</a></td>
				 		<td id="menu-services"><a href="servicios/servicios.php">SERVICIOS</a></td>
				 		<td id="menu-organization"><a href="organigrama.php" >ORGANIGRAMA</a></td>
				 		<td id="menu-mailbox"><a href="buzon.usuario.php">BUZ&Oacute;N</a></td>
				 		<td id="menu-location"><a href="localizacion.php">LOCALIZACI&Oacute;N</a></td>	
				 		<td id="menu-commutator"><a href="conmutador.php" style="color: #053B64;">CONMUTADOR</a></td>				 		
				 		<td id="menu-contact">CONTACTO</td>			 					 					 					 					 					 					 		
				 	</tr>
				 </table>
			</nav>			
			<section id="content" style="padding-top: 0px">	
				<section class="conmutador-izq">
					<div class="barra-01" style="background-image: url('img/content/barra-01-l.png');">													
						<img alt="" src="img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;">ERROR NO ESPERADO</p><br/>	
					</div>						
				</section>				
				<section class="conmutador-der">
					<div class="barra-02" style="background-image: url('img/content/barra-02-l.png');">													
						<img alt="" src="img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="text-align: center;"> </p>
						<!-- <span class="ui-icon ui-icon-search" style="float: left; margin-right: .3em; margin-left: 10px;" ></span> -->				 		
					</div>							
				</section>								
			 	<section  style="padding-top: 10px; float: left;">
			 	
					Disculpe las molestias.
					
			 	</section>													
			</section>
			<footer class="hgjms">
				<section class="footer-izq">
					Avenida Paseo de los Deportistas&nbsp;<br/>
					e/ Misioneros Combonianos y Carabineros&nbsp;<br/>
					Col. 8 de Octubre 2da Secci&oacute;n CP 23085&nbsp;
				</section>
				<section class="footer-der">
					&nbsp;Tel. 612 17 5 05 00<br />
					&nbsp;Lada sin costo: 01 800 837 6640<br />
					&nbsp;Correo: hgjms@prodigy.net.mx							
				</section>
				<section class="footer-cen">
					La Paz, Baja California Sur, M&eacute;xico
				</section>
			</footer>	
		</div>               	 
    </body>
</html>