<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/depto-calidad.js"></script>
		<style type="text/css">
			.slide	{ padding: 20px 30px; width: 900px; float: left; position: relative; };
		</style>																
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-nutricion-cocina">
				<section class="urgencias-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dra. Judith Arciniega Torres&nbsp; &nbsp; &nbsp;Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</p>
				 	</div>			
				</section>
				<section class="nutricion-cocina-der">
					<img src="../../img/departamentos/nutricion.cocina/header-nutricion-cocina.png" width="340" height="96">
				</section>	 	
			</header>
			<div id="content">					
				<section class="nutricion-cocina-izq">																				
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Sistema Integral de Calidad (SICALIDAD)</p><br/>	
					</div>	
					<section style="padding-left: 10px; padding-top: 10px; float: left;">	
						<p class="texto">				
							El Sistema Integral de Calidad (SICALIDAD), que ha venido siendo presentado en la diferentes entidades 
							federativas, incluye proyectos en los tres componentes de la calidad: <strong style="color: #339966;"> calidad percibida, calidad 
							t&eacute;cnica y seguridad del paciente y calidad en la gesti&oacute;n de los servicios de salud. </strong><br><br>
							Se trata de proyectos que han logrado notorios avances y dedicaci&oacute;n de muchos profesionales de la 
							salud a favor de la calidad y que sin perjuicio de su necesaria innovaci&oacute;n forman parte de SICALIDAD.
						</p><br>
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">L&iacute;neas de acci&oacute;n</p><br/>	
						</div>							
						<div class="texto">						
							<table>
								<tr>
									<td width="20px;"></td>
									<td width="30px;"></td>
									<td></td>
								</tr>
								<tr>																		
									<td><p class="cargo">1.</p></td>
									<td colspan="2"><p class="cargo">Calidad percibida por los usuarios</p></td>
								</tr>
								<tr>
									<td></td>
									<td>1.1.</td>
									<td>Construir ciudadan&iacute;a en salud: Aval ciudadano</td>
								</tr>
								<tr>
									<td></td>
									<td style="text-align:">1.2.</td>
									<td>Medici&oacute;n de la satisfacci&oacute;n de usuarios: calidad percibida y entrega de medicamentos</td>
								</tr>	
								<tr>
									<td></td>
									<td>1.3.</td>
									<td>Percepci&oacute;n de los profesionales de la salud. Caminando con los trabajadores de la salud</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>								
									<td><p class="cargo">2.</p></td>
									<td colspan="2"><p class="cargo">Calidad t&eacute;cnica y seguridad del paciente</p></td>
								</tr>
								<tr>
									<td></td>
									<td>2.1.</td>
									<td>Prevenci&oacute;n y reducci&oacute;n de la infecci&oacute;n nosocomial (PREREIN)</td>
								</tr>
								<tr>
									<td></td>
									<td>2.2.</td>
									<td>Expediente cl&iacute;nico integrado y de calidad (ECIC)</td>
								</tr>
								<tr>
									<td></td>
									<td>2.3.</td>
									<td>Prevenci&oacute;n de muerte materna</td>
								</tr>
								<tr>
									<td></td>
									<td>2.4.</td>
									<td>Seguridad del paciente</td>
								</tr>
								<tr>
									<td></td>
									<td>2.5.</td>
									<td>Alternativas a la hospitalizaci&oacute;n: Cirug&iacute;a de D&iacute;a, Atenci&oacute;n Domiciliaria y Cuidados Paliativos</td>
								</tr>
								<tr>
									<td></td>
									<td>2.6.</td>
									<td>Uso racional de medicamentos (URM)</td>
								</tr>
								<tr>
									<td></td>
									<td>2.7.</td>
									<td>Calidad en la atenci&oacute;n de urgencias (SUMAR)</td>
								</tr>
								<tr>
									<td></td>
									<td>2.8.</td>
									<td>Medicina basada en la evidencia gu&iacute;as de practica cl&iacute;nica y plan de cuidados de enfermer&iacute;a</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td><p class="cargo">3.</p></td>
									<td colspan="2"><p class="cargo">Calidad en la gesti&oacute;n de los servicios de salud: Institucionalizaci&oacute;n de la calidad</p></td>
								</tr>
								<tr>
									<td></td>
									<td>3.1.</td>
									<td>INDICAS. Sistema Nacional de Indicadores en Salud</td>
								</tr>
								<tr>
									<td></td>
									<td>3.2.</td>
									<td>Comit&eacute; Nacional por la Calidad en Salud</td>
								</tr>		
								<tr>
									<td></td>
									<td>3.3.</td>
									<td>Comit&eacute;s Estatales de Calidad</td>
								</tr>
								<tr>
									<td></td>
									<td>3.4.</td>
									<td>Comit&eacute; de calidad y seguridad del paciente (COCASEP)</td>
								</tr>	
								<tr>
									<td></td>
									<td>3.5.</td>
									<td>Acreditaci&oacute;n y garant&iacute;a de calidad</td>
								</tr>
								<tr>
									<td></td>
									<td>3.6.</td>
									<td>Componente de calidad en Convenios de Gesti&oacute;n del SPS </td>
								</tr>	
								<tr>
									<td></td>
									<td>3.7.</td>
									<td>Foro y reuniones nacionales de SICALIDAD</td>
								</tr>
								<tr>
									<td></td>
									<td>3.8.</td>
									<td>Red SICALIDAD: Bolet&iacute;n</td>
								</tr>								
								<tr>
									<td></td>
									<td>3.9.</td>
									<td>Modelo de gesti&oacute;n para la calidad total</td>
								</tr>						
							</table>						 			
						</div>
					</section>		
				</section>																		
				<section class="nutricion-cocina-der">			
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>  
    				<img alt="" src="../../img/departamentos/calidad/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin: 10px 0;">    				
    				<a id="btn-zoom" class="btn">Zoom</a><br/><br/>
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Personal</p><br/>	
					</div>	

			 		<section style=" padding:10px; margin-top: 10px; float: left; width: 320px;" class="ui-widget-content ui-corner-all">						
						<p class="cargo">Gestor de Calidad:</p>
						<p class="personal">Dra. Judith Arciniega Torres</p>	
						<br>
						<p class="cargo">Responsable de Calidad en Enfermer&iacute;a:</p>
						<p class="personal">Lic. en Enfermer&iacute;a Cleotilde G&oacute;mez Aripez</p>
						<br>
						<p class="cargo">Apoyo Administrativo:</p>
						<p class="personal">C. Alicia Garc&iacute;a Ortega</p>														
					</section>	
					<img alt="" src="../../img/departamentos/calidad/buzon-thumb.jpg" class="imgshadow" style="width: 330px; margin: 10px 0;">				
    			</section>	         		
    						 		 
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>