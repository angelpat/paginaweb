<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/tooltip.js"></script> 
		<script src="../../js/depto-calidad.js"></script>																
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-nutricion-cocina">
				<section class="urgencias-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dra. Judith Arciniega Torres&nbsp; &nbsp; &nbsp;Coordinaci&oacute;n de Innovaci&oacute;n y Calidad</p>
				 	</div>			
				</section>
				<section class="nutricion-cocina-der">
					<img src="../../img/departamentos/calidad/header.png" width="340" height="96">
				</section>	 	
			</header>
			<div id="content">
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>&iquest;Qui&eacute;nes somos?</h1>							
								<p>La Coordinaci&oacute;n de Innovaci&oacute;n y Calidad del Benem&eacute;rito Hospital General &quot;Juan Mar&iacute;a de Salvatierra&quot; es el enlace para operacionalizar las estrategias y l&iacute;neas de acci&oacute;n del Programa Nacional de Salud, en las diferentes &aacute;reas y servicios del Hospital. Es reciente creaci&oacute;n, considerado un &oacute;rgano de staff de la Direcci&oacute;n, pues contribuye al dise&ntilde;o organizacional y a la ejecuci&oacute;n de procesos de mejora continua de la calidad, esta integrado con personal calificado y son responsables de asesorar, capacitar y llevar a cabo los Programas Institucionales dentro del marco del Sistema Integral de Calidad (SIcalidad) de los Servicios de Salud.</p>						
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/calidad/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Como parte integral del Hospital General con Especialidades &quot;Juan Mar&iacute;a de Salvatierra&quot; que proporciona atenci&oacute;n m&eacute;dica a la poblaci&oacute;n del Estado de Baja California Sur, tiene la misi&oacute;n de implementar un sistema integral de calidad que contribuya a elevar la calidad de los servicios y la seguridad del paciente.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/calidad/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>								
				<section class="nutricion-cocina-izq">			
						
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Funciones</p><br/>	
					</div>										
					<section style="padding-left: 10px; padding-top: 10px; float: left;" >					
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaborar y operar el Programa de Trabajo de la Coordinaci&oacute;n.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Integrar las Coordinaciones de Calidad dentro de los servicios, para evaluar y analizar los procesos.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Coordinar el trabajo en equipo con personal m&eacute;dico y administrativo que maneja Programas de Calidad, en el seguimiento y logro de metas.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Asegurar que se cumpla con lo establecido en las normas que se fundamenta el Sistema Integral de Calidad.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Promover la implementaci&oacute;n y el seguimiento de las l&iacute;neas de acci&oacute;n del sistema integral de calidad.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Dar seguimiento a evaluaciones externas que propongan comentarios  en la soluci&oacute;n de: Quejas y Sugerencias de Contralor&iacute;a, Contralores Ciudadanos y Aval Ciudadano, a fin de conocer las expectativas y proporcionar la satisfacci&oacute;n de usuarios y personal.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Trabajar en equipo para elaborar y/o revisar los Programas de Mejora Continua que se lleven a cabo en el &aacute;rea m&eacute;dica y administrativa.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Monitorear y dar seguimiento a los Indicadores del Sistema Integral de Calidad (INDICA) y de los indicadores internos del hospital.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Coordinar  el Comit&eacute; de Calidad y seguridad del paciente (COCASEP).
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaborar el informe y reporte de actividades realizadas en la Coordinaci&oacute;n.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Participar en actividades docentes destinadas a la difusi&oacute;n de la calidad de la atenci&oacute;n, tanto para el personal del &aacute;rea m&eacute;dica como administrativa.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaborar, aplicar y mantener actualizado el Manual de Procedimientos de la Coordinaci&oacute;n.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Participar en la implantaci&oacute;n y mejora continua del Sistema Integral de Calidad.
						</p><br>
					
					</section>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Sistema Integral de Calidad (SICALIDAD)</p><br/>	
					</div>	
					<section style="padding-left: 10px; padding-top: 10px; float: left;">	
						<p class="texto">				
							El Sistema Integral de Calidad (SICALIDAD), que ha venido siendo presentado en la diferentes entidades 
							federativas, incluye proyectos en los tres componentes de la calidad: <strong style="color: #339966;"> calidad percibida, calidad 
							t&eacute;cnica y seguridad del paciente y calidad en la gesti&oacute;n de los servicios de salud. </strong><br><br>
							Se trata de proyectos que han logrado notorios avances y dedicaci&oacute;n de muchos profesionales de la 
							salud a favor de la calidad y que sin perjuicio de su necesaria innovaci&oacute;n forman parte de SICALIDAD.
						</p>
						<a href="si.calidad.php" class="link-descarga tooltip" style="padding-left: 10px; float: right;" title="Ver detalles" >Leer mas...</a><br>
					</section>										
				</section>																		
				<section class="nutricion-cocina-der">			
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>  
    				<img alt="" src="../../img/departamentos/calidad/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin: 10px 0;">    				
    				<a id="btn-zoom" class="btn">Zoom</a><br/><br/>
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Personal</p><br/>	
					</div>	

			 		<section style=" padding:10px; margin-top: 10px; float: left; width: 320px;" class="ui-widget-content ui-corner-all">						
						<p class="cargo">Gestor de Calidad:</p>
						<p class="personal">Dra. Judith Arciniega Torres</p>	
						<br>
						<p class="cargo">Responsable de Calidad en Enfermer&iacute;a:</p>
						<p class="personal">Lic. en Enfermer&iacute;a Cleotilde G&oacute;mez Aripez</p>
						<br>
						<p class="cargo">Apoyo Administrativo:</p>
						<p class="personal">C. Alicia Garc&iacute;a Ortega</p>														
					</section>		
					
					<img alt="" src="../../img/departamentos/calidad/buzon-thumb.jpg" class="imgshadow" style="width: 330px; margin: 10px 0;">			
    			</section>	     
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>