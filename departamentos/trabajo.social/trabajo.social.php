<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/tooltip.js"></script> 
		<script src="../../js/depto-trabajo-social.js"></script>																
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-nutricion-cocina">
				<section class="urgencias-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Trabajo Social</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Lic. T.S. Melissa de la Cruz Rodr&iacute;guez&nbsp; &nbsp; &nbsp;Jefa del Depto. de Trabajo Social</p>
				 	</div>			
				</section>
				<section class="nutricion-cocina-der">
					<img src="../../img/departamentos/trabajo-social/header.png" width="340" height="96">
				</section>	 	
			</header>
			<div id="content">
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Somos parte del equipo multidisciplinario que proporciona atenci&oacute;n, informaci&oacute;n y orientaci&oacute;n con calidad y calidez, en respuesta a las necesidades de salud y social del usuario, mediante acciones de investigaci&oacute;n, planeaci&oacute;n, canalizaci&oacute;n con apoyo de programas de salud establecidos, dirigido a la poblaci&oacute;n que demanda un servicio de salud y as&iacute; garantizar la reintegraci&oacute;n del individuo a su entorno.</p>						
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/trabajo-social/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Valores</h1>							
								<p>Prudencia, actuamos correctamente ante cualquier circunstancia, mediante la reflexi&oacute;n y razonamiento de los efectos que pueden producir nuestras palabras y acciones. Respeto, reconocemos, aceptamos, apreciamos y valoramos las cualidades del pr&oacute;jimo y sus derechos.  Es decir, el respeto es el reconocimiento del valor propio y de los derechos de los individuos y de la sociedad.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/trabajo-social/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>						
				</div>								
				<section class="trabajo-social-izq">			
						
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Servicios que ofrece</p><br/>	
					</div>										
					<section style="padding-left: 10px; padding-top: 10px; float: left;" >		
												
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Realizaci&oacute;n de estudios socioecon&oacute;micos 
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Resumen cl&iacute;nico y constancias de pacientes que se encuentren internados o lleven seguimiento en esta unidad
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Apoyo con el tr&aacute;mite de ingreso al albergue asistencial (s&oacute;lo con familiares internados o consultas proximas)
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Reposici&oacute;n de Unidades Sangu&iacute;neas
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Tramites de traslados a otro hospital o de tercer nivel
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Apoyo con estudios externos al hospital que sean solicitados por su m&eacute;dico tratante
						</p><br>		
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Orientaci&oacute;n e informaci&oacute;n, reglamento, tramites de egreso, defunci&oacute;n, horarios de visitas, entre otros
						</p><br>																													
					
					</section>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01" style="font-size: 21px;">Gu&iacute;a para el familiar del paciente hospitalizado</p><br/>	
					</div>	
					<section style="padding-top: 10px; float: left;">					
					
						<img alt="" src="../../img/departamentos/trabajo-social/guia-fam-01.png" class="imgshadow" style="width: 590px; ">
						<img alt="" src="../../img/departamentos/trabajo-social/guia-fam-02.png" class="imgshadow" style="width: 590px; margin: 10px 0;">
						

						
						<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  " > 
							<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
							<strong>Descargar diptico</strong></p><br/>
							<div>
								<a target="_blank" href="../../resources/departamentos/trabajo-social/GuiaParaElFamiliarDelPacienteHospitalizado.pdf" 
									class="link-descarga tooltip" title="Descargar en PDF">
									<img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Guia para el familiar del paciente hospitalizado
								</a><br>
							</div>
						</div>
						
						
					</section>										
				</section>																		
				<section class="trabajo-social-der">			
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>  
    				<img alt="" src="../../img/departamentos/trabajo-social/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin: 10px 0;">    				
    				<a id="btn-zoom" class="btn">Zoom</a><br/><br/>
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Personal</p><br/>	
					</div>	


					
				
						<div id="accordion" style="float: left; margin-top: 10px;">
							<h3><a href="#">Turno Matutino</a></h3>
							<div>
								<p class="titulo-dr">T.S. Lydia Amanda Rosas Nevarez</p>
								<p class="titulo-dr">Lic. T.S. Mar&iacute;a Juana de la Toba Ojeda</p>
								<p class="titulo-dr">T.S. Alma Patricia Zavala</p>
								<p class="titulo-dr">T.S. P. Ma. Del Refugio Alonso Rayo</p>
								<p class="titulo-dr">Lic. T.S. Dania Elizabeth Cervera Fuentes</p>
								<p class="titulo-dr">T.S. Alma Elisa de la Pe&ntilde;a Angulo</p>
								<p class="titulo-dr">Lic. T.S. Karina G. Sep&uacute;lveda Lucero</p>
								<p class="titulo-dr">T.S. Alma Rosa de la Pe&ntilde;a &aacute;viles</p>
								<p class="titulo-dr">T.S. Patricia Tirado Saucedo</p>
								<p class="titulo-dr">T.S. Nidia del Rosario Carballo Pe&ntilde;a</p>
								<p class="titulo-dr">Lic. T.S. Juana del Rosario Talamantes Espinoza</p>
								<p class="titulo-dr">T.S. Maricela &aacute;viles Ju&aacute;rez</p>
								 
							</div>		
							<h3><a href="#">Turno Vespertino</a></h3>
							<div>
								<p class="titulo-dr">T.S. Alma Lilia Mart&iacute;nez L&oacute;pez</p>  
								<p class="titulo-dr">T.S. Mar&iacute;a del Rosario N&uacute;&ntilde;ez Castillo</p> 
								<p class="titulo-dr">Lic. T.S. Rosa Ochoa Alonso</p> 
								<p class="titulo-dr">T.S. Amalia Guadalupe Tirado Saucedo</p>
								<p class="titulo-dr">T.S. Mar&iacute;a Dolores Guerrero Monta&ntilde;o</p>
								<p class="titulo-dr">Lic. T.S. Iris Monserrat Murillo Covarrubias</p> 
								<p class="titulo-dr">Lic. T.S. Ana Laura Mendoza Galindo</p>								
								<p class="titulo-dr">Lic. T.S. Mar&iacute;a Guadalupe Beltr&aacute;n Romero</p>								
							</div>

							<h3><a href="#">Diurno</a></h3>
							<div>
								<p class="titulo-dr">T.S. Mar&iacute;a Lourdes &aacute;viles Verdugo</p>
								<p class="titulo-dr">Lic. T.S. Miriam Guadalupe Romero M&aacute;rquez</p>  
								<p class="titulo-dr">Lic. T.S. Lucia Beltr&aacute;n Castro</p>
								<p class="titulo-dr">T.S. Maricela Gonz&aacute;lez Carl&oacute;n</p>
								<p class="titulo-dr">T.S. Patricia &aacute;viles Verdugo</p>
								<p class="titulo-dr">T.S. Norma Leticia Cosi&oacute; Rodr&iacute;guez</p>
								<p class="titulo-dr">Lic. T.S. Elvia Eduviges Urbina Lugo</p>
							</div>																													
						</div>
					
					
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; margin: 10px 0;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Horarios de atenci&oacute;n</p><br/>	
					</div>	
										
					<table class="hor-zebra-verde" style="width: 340px;">
						<thead >
							<tr>								
								<th colspan="3" style="text-align: center;">Horarios</th>																
							</tr>
						</thead>
						<tr>
							<td>Matutino</td>
							<td>8:00 am - 2:30 pm</td>
							<td>Lunes a viernes</td>
						</tr>	
						<tr>
							<td>Vespertino</td>
							<td>2:00 pm - 8:30 pm</td>
							<td>Lunes a viernes</td>
						</tr>	
						<tr>
							<td>Diurno</td>
							<td>8:00 am - 8:00 pm</td>
							<td>Fin de semana</td>
						</tr>	
					</table>
					
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left; margin-top: 10px;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Galer&iacute;a de imagenes</p><br/>	
					</div>						
 					<a href="galeria.php" class="tooltip"  title="Ir a galeria">
 						<img alt="" src="../../img/departamentos/trabajo-social/galeria.png" class="imgshadow" style="width: 330px; margin: 10px 0;">
					</a>					
    			</section>	     
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>