<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/startstop-slider.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/startstop-slider.js"></script>
		<script src="../../js/depto-informatica.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-sigho-modelo"></section>
    	<section id="modal-sigho-ubicando"></section>		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>   
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-ensenanza">
				<section class="informatica-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Ingenier&iacute;a en Sistemas de Informaci&oacute;n</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Ing. Ismael Ram&iacute;rez Cota &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Jefe del Departamento </p>
				 	</div>				
				</section>
				<section class="ensenanza-der">
					<img src="../../img/departamentos/informatica/header.png" width="340" height="96" alt="Informatica">
				</section>	 	
			</header>
			<div id="content">
				<div>
					<section class="informatica-izq">
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">SIGHO - Antecedentes</p><br/>	
						</div><br>				
						<p class="texto sangria">
							Con la implementaci&oacute;n del SIGHO en el hospital, se esta dando un avance importante en el tratamiento de la informaci&oacute;n, de esta manera estar&iacute;a a la mano del doctor en cuanto ocupara ver el expediente cl&iacute;nico del paciente sin tener que ir al archivo, as&iacute; como realizar los tramites correspondientes.
						</p><br>			
						<p class="texto sangria">
							El SIGHO permite a todas las &aacute;reas m&eacute;dicas estar conectadas con la informaci&oacute;n del paciente desde su ingreso.
						</p><br>			
						<p class="texto sangria">
							Los tiempos cambian y el departamento de inform&aacute;tica sigue desarrollando su infraestructura para mantenerse al margen y a la vanguardia de la m&eacute;dicina moderna, una de las tareas a seguir es preparar al personal de atenci&oacute;n medica en el uso de la computadoras, desarrollar nuevos sistemas para el mejor flujo de informaci&oacute;n en el hospital, de igual forma estar capacitandose para hacer de este departamento un &aacute;rea de gran valor al hospital.
						</p><br>			

						<p class="texto sangria">
							Con la implementaci&oacute;n del SIGHO y otros sistemas de computo para el &aacute;rea de administraci&oacute;n hospitalaria y con el personal para el soporte t&eacute;cnico, podremos decir que se sigue un buen camino para atender las exigencias hospitalarias.
						</p><br>			
					</section>									
					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Modelo SIGHO</p><br/>	
						</div><br> 
						<img alt="" src="../../img/departamentos/informatica/sigho-modelo-thumb.gif" width="340">
						<br/><br/>
						<a id="btn-sigho-modelo" class="btn">Zoom</a> <br/>
						<br/>					
	    			</section>
				</div>		
				<div>																
					<section class="informatica-izq">					
		    		 	<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">&iquest;Qu&eacute; es el SIGHO?</p><br/>	
						</div><br>	
						<p class="texto sangria">
							Es el Sistema de Informaci&oacute;n para la Gerencia Hospitalaria, que la Secretaria de Salud a trav&eacute;s de la Direcci&oacute;n General de Informaci&oacute;n en Salud (DGIS) ha liberado para su implementaci&oacute;n en apoyo a la gerencia de todos los Hospitales del sector salud en M&eacute;xico. 
						</p><br>
						<p class="texto sangria">
							Es un software basado en la NOM-168-SSA1-1998 referente al resguardo y uso del expediente cl&iacute;nico electr&oacute;nico para facilitar las actividades de gerencia dentro del hospital.
						</p><br>
						<p class="texto sangria">
							Se compone de 17 m&oacute;dulos (2 administrativos, 2 Soporte y 13 relacionados con la atenci&oacute;n al paciente).
						</p><br>
						<p class="texto sangria">
							Permite realizar registros individuales alrededor de la historia cl&iacute;nica electr&oacute;nica.
						</p><br>																													
					</section>
					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Ubicando al SIGHO</p><br/>	
						</div><br> 						
						<img alt="" src="../../img/departamentos/informatica/sigho-ubicando.gif" width="340" class="centrar-imagen" >
						<br>
						<a id="btn-sigho-ubicando" class="btn">Zoom</a><br/><br>
	    			</section>										
				</div>				
				<div>																
					<section class="informatica-izq">					
		    			<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Beneficios</p><br/>						
						</div><br>			
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Incorpora Visi&oacute;n Sist&eacute;mica de la automatizaci&oacute;n de los Servicios de Salud.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Es un desarrollo en apego al modelo de atenci&oacute;n (MIDAS) y a los lineamientos establecidos por las &aacute;reas normativas de la SSA.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Permite m&uacute;ltiples salidas, apegadas a las necesidades del usuario (datos e informaci&oacute;n) en tiempo real.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Mejorar la atenci&oacute;n a los usuarios de los servicios m&eacute;dicos.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Identificar de manera &uacute;nica a cada usuario de los servicios de la instituci&oacute;n, permiti&eacute;ndonos conocerlo y atenderlo eficientemente.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>
							Establecer un banco de informaci&oacute;n completo para an&aacute;lisis cl&iacute;nico estad&iacute;stico.
						</p><br>
					</section>
					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01"></p><br/>	
						</div><br> 						 
						<img alt="" src="../../img/departamentos/informatica/sigho-beneficios.gif" width="340">					
	    			</section>										
				</div>	
				
				<div>																
					<section class="informatica-izq">					
		    		 	<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">&iquest;Como se conforma el SIGHO?</p><br/>	
						</div><br>
						<img alt="" src="../../img/departamentos/informatica/sigho-conformacion.gif" width="600">	
					</section>

					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01"></p><br/>	
						</div><br> 

	    			</section>										
				</div>	
    						
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>