<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/startstop-slider.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/startstop-slider.js"></script>
		<script src="../../js/depto-informatica.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-sigho-modelo"></section>
    	<section id="modal-sigho-ubicando"></section>		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>   
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-ensenanza">
				<section class="informatica-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Ingenier&iacute;a en Sistemas de Informaci&oacute;n</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Ing. Ismael Ram&iacute;rez Cota &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Jefe del Departamento </p>
				 	</div>				
				</section>
				<section class="ensenanza-der">
					<img src="../../img/departamentos/informatica/header.png" width="340" height="96" alt="Informatica">
				</section>	 	
			</header>
			<div id="content">
				<div>
					<section class="informatica-izq">
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">NORMATIVIDAD DE TIC'S</p><br/>	
						</div><br>				
			
					</section>									
					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01"></p><br/>	
						</div><br> 
					
	    			</section>
				</div>						
				<div>				
					<p class="titulo-descarga">Programas</p>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/ProNaSa_2007-2012.pdf" target="_blank">
						Programa Nacional de Salud 2007-2012</a>
						<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/PAE2007-2012_SNIS.pdf" target="_blank">
						Programa de Acci&oacute;n Espec&iacute;fico 2007-2012 del Sistema Nacional de Informaci&oacute;n en Salud</a>
					<br><br>
				
					<p class="titulo-descarga">Leyes</p>	
					
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/LGS_14072008.pdf" target="_blank">
						Ley General de Salud</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lsnieg.pdf" target="_blank">	
						Ley del Sistema Nacional de Informaci&oacute;n Estad&iacute;stica y Geogr&aacute;fica</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/LFTAIPG.pdf" target="_blank">
						Ley Federal de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica Gubernamental</a>
					<br><br>
					<p style="padding-left: 20px;">Ley de salud para el estado de baja california sur decreto no. 1483</p>
					<br><br>
						
					<p class="titulo-descarga">Reglamentos</p>
							
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/ReglamentoInteriorSS.pdf" target="_blank">
						Reglamento Interior de la Secretar&iacute;a de Salud</a>
					<br><br>		
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/RLGS-MPSS.pdf" target="_blank">
						Reglamento de la Ley General de Salud en Materia de Protecci&oacute;n Social en Salud</a>
					<br><br>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/LIEG.pdf" target="_blank">
						Reglamento de la Ley de Informaci&oacute;n Estad&iacute;stica y Geogr&aacute;fica</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lsnieg.pdf" target="_blank">
						Reglamento de la Ley Federal de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica Gubernamental</a>
					<br><br>
					<p style="padding-left: 20px;">Reglamento Interior del Instituto de Servicios de Salud de Baja California Sur</p>
					<br><br>
								
					<p class="titulo-descarga">Lineamientos</p>		
					
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lineamientos_protdatper1.pdf" target="_blank">	
						LFTAIPG - Lineamientos de Protecci&oacute;n de Datos Personales</a>
					<br><br>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lineamientos_protdatper2.pdf" target="_blank">
						LFTAIPG - Lineamientos que deber&aacute;n observar las dependencias y entidades de la
						Administraci&oacute;n P&uacute;blica Federal en la recepci&oacute;n, procesamiento y tr&aacute;mite de las
						solicitudes de acceso a la informaci&oacute;n gubernamental que formulen los
						particulares, as&iacute; como en su resoluci&oacute;n y notificaci&oacute;n, y la entrega de la
						informaci&oacute;n en su caso, con exclusi&oacute;n de las solicitudes de acceso a datos
						personales y su correcci&oacute;n</a>
					<br><br>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lineamientos_protdatper3.pdf" target="_blank">
						LFTAIPG - Lineamientos que deber&aacute;n observar las dependencias y entidades de la
						Administraci&oacute;n P&uacute;blica Federal en la recepci&oacute;n, procesamiento, tr&aacute;mite,
						resoluci&oacute;n y notificaci&oacute;n de las solicitudes de acceso a datos personales que
						formulen los particulares, con exclusi&oacute;n de las solicitudes de correcci&oacute;n de dichos
						datos</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lineamientos_protdatper4.pdf" target="_blank">
						LFTAIPG	- Lineamientos que deber&aacute;n observar las dependencias y entidades de la
						Administraci&oacute;n P&uacute;blica Federal para notificar al Instituto el listado de sus
						sistemas de datos personales</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/PoBaLines_SS_15072008.pdf" target="_blank">
						Pol&iacute;ticas, Bases y Lineamientos que deber&aacute;n observar los servidores p&uacute;blicos de las
						unidades administrativas y &oacute;rganos desconcentrados en esta Secretar&iacute;a de Salud
						en los procedimientos de contrataci&oacute;n para la adjudicaci&oacute;n y arrendamiento de
						bienes muebles y la prestaci&oacute;n de servicios de cualquier naturaleza, con
						excepci&oacute;n de los servicios relacionados con la obra p&uacute;blica</a>
					<br><br>
			
			
					<p class="titulo-descarga">Normas oficiales mexicanas</p>
					
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-024-SSA3-2010_SistemasECE.pdf" target="_blank">
						<strong>NNOM-024-SSA3-2010.</strong>
						Que establece los objetivos funcionales y funcionalidades que deber&aacute;n observar
						los productos de Sistemas de Expediente Cl&iacute;nico Electr&oacute;nico para garantizar la
						interoperabilidad, procesamiento, interpretaci&oacute;n, confidencialidad, seguridad y
						uso de est&aacute;ndares y cat&aacute;logos de la informaci&oacute;n de los registros electr&oacute;nicos
						en salud</a>		
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-040-SSA2-2004_DOF28-09-2005.pdf" target="_blank">
						<strong>NOM-040-SSA2-2004.</strong>
						En Materia de Informaci&oacute;n en Salud</a>	
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-017-SSA2-1994.pdf" target="_blank">
						<strong>NNOM-017-SSA2-1994.</strong>
						Para la Vigilancia Epidemiol&oacute;gica</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-168-SSA1-1998.pdf" target="_blank">
						<strong>NNOM-168-SSA1-1998.</strong>
						Del Expediente Cl&iacute;nico</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/ResolucionNOM-168-SSA1-1998.pdf" target="_blank">
						RESOLUCI&oacute;N por la que se modifica la Norma Oficial Mexicana NOM-168-SSA1-1998, Del expediente cl&iacute;nico</a>	
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-178-SSA1-1998.pdf" target="_blank">
						<strong>NNOM-178-SSA1-1998.</strong>
						Que establece los requisitos m&iacute;nimos de infraestructura y equipamiento de
						establecimientos para la atenci&oacute;n m&eacute;dica de pacientes ambulatorios</a>	
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-197-SSA1-2000.pdf" target="_blank">
						<strong>NNOM-197-SSA1-2000.</strong>
						Que establece los requisitos m&iacute;nimos de infraestructura y equipamiento de
						hospitales y consultorios de atenci&oacute;n m&eacute;dica especializada</a>	
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/NOM-206-SSA1-2002.pdf" target="_blank">
						<strong>NNOM-206-SSA1-2002.</strong>
						Regulaci&oacute;n de los servicios de salud. Que establece los criterios de
						funcionamiento y atenci&oacute;n en los servicios de urgencias de los establecimientos
						de atenci&oacute;n m&eacute;dica</a>	
					<br><br>
					
				
					<p class="titulo-descarga">Decretros y acuerdos</p>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/DecretoCertificadosDefyMFetal.pdf" target="_blank">
						DECRETO
						por el que se da a conocer la forma oficial de los certificados de defunci&oacute;n y
						de muerte fetal</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/DOF_16Enero2009_DecretoAbrogaCDyMF.pdf" target="_blank">
						DECRETO
						que abroga el diverso por el que se da a conocer la forma oficial de los
						certificados de defunci&oacute;n y de muerte fetal, publicado el 21 de noviembre de
						1986</a>
					<br><br>	
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/DOF_30ENE2009_AcuerdoCDyMF.pdf" target="_blank">
						ACUERDO
						por el que la Secretar&iacute;a de Salud da a conocer los formatos de certificados de
						defunci&oacute;n y de muerte fetal</a>
					<br><br>
				
				
					<p class="titulo-descarga">Est&aacute;ndares</p>	
				
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/lista_mexicana.pdf" target="_blank">
						Lista mexicana para la selecci&oacute;n de primeras causas de enfermedad</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/criterios_cie10_princcausas.pdf" target="_blank">
						Criterio de agrupaci&oacute;n de las principales causas de enfermedad conforme a la CIE-10</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/xls/CIE_CAT-02102009_DGIS.xls" target="_blank">
						Cat&aacute;logo de la Clasificaci&oacute;n Estad&iacute;stica Internacional de Enfermedades y Problemas
						Relacionados con la Salud, D&eacute;cima Revisi&oacute;n (CIE-10) actualizado al mes de
						octubre de 2009 (6MB)</a>
					<br><br>
					<a class="link-descarga" href="http://dgis.salud.gob.mx/descargas/pdf/MASPA_1995.pdf" target="_blank">
						Modelo de Atenci&oacute;n a la Salud para la Poblaci&oacute;n Abierta (MASPA), 1995</a>	
					<br><br>
				
			
					<p class="titulo-descarga">Atribuciones</p>	
					<div style="padding-left: 20px;">
						<p>Reglamento Interior del Instituto de Servicios de Salud de Baja California Sur</p><br>
						<p>CAPITULO IX</p><br>	
						<p>De las atribuciones y obligaciones de los titulares de las direcciones de &aacute;rea y unidades administrativas del Instituto</p><br>	
						<p>CAPITULO X</p><br>	
						<p>De las funciones especificas de las Direcciones de &aacute;rea</p><br>
					</div>
				</div>		 						
			</div>  
			<?php 
				$objCabecera->pie();
			?>			
		</div>               	 
    </body>
</html>