<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/tooltip.js"></script>  		
	    <script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/depto-informatica.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>				
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>              
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-informatica">
				<section class="informatica-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Ingenier&iacute;a en Sistemas de Informaci&oacute;n</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Ing. Ismael Ram&iacute;rez Cota &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Jefe del Departamento </p>
				 	</div>				
				</section>
				<section class="ensenanza-der">
					<img src="../../img/departamentos/informatica/header.png" width="340" height="96" alt="Informatica">
				</section>	 	
			</header>
			<div id="content">
			
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Objetivo General</h1>							
								<p>Contar con un equipo humano comprometido para la creaci&oacute;n, administraci&oacute;n y alimentaci&oacute;n de Sistemas de gesti&oacute;n que permitan el registro de las actividades de atenci&oacute;n  de nuestros pacientes, generando  informaci&oacute;n de salud y estad&iacute;stica de calidad homog&eacute;nea, continua, oportuna, relevante y confiable de los servicios prestados por el Hospital para la toma de decisiones.</p>						
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/informatica/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>"Planear, desarrollar e impulsar con excelencia y calidad, tecnolog&iacute;as de la informaci&oacute;n y la comunicaci&oacute;n en salud (TIC's), fomentando una cultura de sistematizaci&oacute;n de procesos, esto como una herramienta fundamental,  sencilla, pr&aacute;ctica e  innovadora  para la generaci&oacute;n, difusi&oacute;n, operaci&oacute;n  y evaluaci&oacute;n del conocimiento en salud de acuerdo a los avances tecnol&oacute;gicos del &aacute;rea m&eacute;dica y de las necesidades del Benem&eacute;rito Hospital General con especialidades "Juan Mar&iacute;a de Salvatierra", contribuyendo a la mejora de la calidad de vida de la poblaci&oacute;n en nuestro Estado, haciendo efectivo su derecho a la protecci&oacute;n de la salud."</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/informatica/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>	
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>"Contar con la confianza y satisfacci&oacute;n de nuestros Usuarios a trav&eacute;s de las TIC's de vanguardia, que proporcione informaci&oacute;n y servicios oportunos, seguros, de calidad, brindando a sus necesidades respuestas integrales, r&aacute;pidas y efectivas a trav&eacute;s de un equipo multidisciplinario con calidad humana, t&eacute;cnica y profesional, para el buen funcionamiento del Benem&eacute;rito Hospital General con especialidades "Juan Mar&iacute;a de Salvatierra", como el hospital de concentraci&oacute;n estatal e innovador y ejemplo de los dem&aacute;s hospitales de nuestro Estado."</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/informatica/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>															
				</div>							
				<div>
					<section class="informatica-izq">
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Antecedentes</p><br/>	
						</div>
						<p class="texto sangria">		
							La inform&aacute;tica m&eacute;dica se sit&uacute;a en la intersecci&oacute;n entre la inform&aacute;tica y las diferentes disciplinas en la medicina y los cuidados de salud. Esta nueva ciencia resulta imprescindible para la adquisici&oacute;n no s&oacute;lo de conocimientos, sino de herramientas que le posibilitan al profesional de salud a acceder a informaci&oacute;n, como tambi&eacute;n a la utilizaci&oacute;n y creaci&oacute;n de software propios del medio en que se desarrollan.<br><br>
	                    	En este sentido, nuestro hospital ha ido evolucionando con esta herramienta de trabajo, ahora no solo son los procesos administrativos que tienen su operatividad con apoyo de la inform&aacute;tica, con el uso del expediente cl&iacute;nico electr&oacute;nico ECE y la tecnolog&iacute;a biom&eacute;dica de punta para el manejo de la informaci&oacute;n de salud.	
						</p>
					</section>					
					<section class="informatica-der">
						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Organigrama</p><br/>	
						</div>
	    				<img alt="Organigrama" src="../../img/departamentos/informatica/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin-top: 10px;">
	    				<br/><br/>
	    				<a id="btn-organigrama" class="btn">Zoom</a> <br/>
						<br>
	    			</section>									
				</div>
				
				<div>
					<section class="informatica-izq">

						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Sistemas de Informaci&oacute;n</p><br/>	
						</div><br>		
						
						<strong>Objetivo General</strong><br><br> 
						<p class="texto sangria">
							Proveer al Hospital de soluciones inform&aacute;ticas que faciliten el registro, distribuci&oacute;n y an&aacute;lisis de informaci&oacute;n para la atenci&oacute;n m&eacute;dica del paciente, comunicaci&oacute;n al p&uacute;blico, y administraci&oacute;n de recursos que ayuden en toma de decisiones.
						</p>
										
					</section>																	
					<section class="informatica-der">

						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Conoce el SIGHO</p><br/>	
						</div><br>
						
						<p class="texto sangria">
							Es el Sistema de Informaci&oacute;n para la Gerencia Hospitalaria, que la Secretaria de Salud a trav&eacute;s de la Direcci&oacute;n General de Informaci&oacute;n en Salud (DGIS) ha liberado para su implementaci&oacute;n en apoyo a la gerencia de todos los Hospitales del sector salud en M&eacute;xico. 
						</p>						
						<a href="sigho.php" class="link-descarga" style="padding-left: 10px; float: right;" >Leer mas...</a><br/>						
						<br>
	    			</section>	  				
				
				</div>
				
				<div>
					<section class="informatica-izq">

						
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Taller de Soporte T&eacute;cnico</p><br/>	
						</div><br>		
						<strong>Objetivo General</strong><br><br> 									
						<p class="texto sangria">
							Mantener en operaci&oacute;n y en las mejores condiciones posibles la infraestructura de c&oacute;mputo y sistemas especiales  de acuerdo a las necesidades del Hospital.
						</p>
						<br/>
						
						<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Redes y comunicaciones</p><br/>	
						</div><br>		
						<strong>Objetivo General</strong><br><br> 
						<p class="texto sangria">
							Desarrollar, mantener y promover la infraestructura de redes y comunicaciones as&iacute; como el uso adecuado de las mismas privilegiando la seguridad, para satisfacer los requerimientos de la comunidad Hospitalaria.
						</p>
						<br/>												
					</section>																	
					<section class="informatica-der">

						<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
							<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
							<p class="titulo-barra-01">Normatividad TIC'S</p><br/>	
						</div>
						<br>
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Programas
						</p><br/>			
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Leyes
						</p><br/>		
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Reglamentos
						</p><br/>
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Lineamientos
						</p><br/>	
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Normas oficiales mexicanas
						</p><br/>
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Decretros y acuerdos
						</p><br/>																																					
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Est&aacute;ndares
						</p><br/>
						<p><span class="ui-icon ui-icon-pin-w" style="float: left; margin-right: .3em;"></span>
							Atribuciones
						</p><br/>												
						<a href="normatividad-tic.php" class="link-descarga" style="padding-left: 10px; float: right;" >Leer mas...</a><br/>			
						
	    			</section>	  				
				
				</div>
								
			</div>
			<?php 
				$objCabecera->pie();
			?>			
		</div>               	 
    </body>
</html>