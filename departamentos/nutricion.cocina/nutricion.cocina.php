<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css"/>				
		<link rel="stylesheet" href="../../css/jshowoff.css"/>
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/depto-nutricion-cocina.js"></script>																
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");			
		</script>	
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>      
    	<div id="contador"></div>
    	<section id="modal-organigrama"></section>
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-nutricion-cocina">
				<section class="urgencias-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Nutrici&oacute;n, Diet&eacute;tica y Cocina</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">LN y ED Luis Enrique Garc&iacute;a Ram&iacute;rez &nbsp; &nbsp; &nbsp;Jefe de Nutrici&oacute;n, Diet&eacute;tica y Cocina</p>
				 	</div>			
				</section>
				<section class="nutricion-cocina-der">
					<img src="../../img/departamentos/nutricion.cocina/header-nutricion-cocina.png" width="340" height="96" alt="Nutricion y Cocina">
				</section>	 	
			</header>
			<div id="content">
			
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Brindar y entregar con excelencia Dietas Normales de acuerdo a est&aacute;ndares de poblaci&oacute;n sana y Dietas Terap&eacute;uticas a trav&eacute;s de una alimentaci&oacute;n oportuna,  adecuada, cumpliendo con las recomendaciones y necesidades alimentarias de los pacientes y usuarios, con la garant&iacute;a de calidad higi&eacute;nica concatenada con una adecuada atenci&oacute;n nutricial personalizada, fomentando as&iacute; la Medicina Preventiva y ofreciendo apoyo al cuerpo m&eacute;dico en el diagn&oacute;stico y manejo nutricional de los beneficiarios.</p>						
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/nutricion.cocina/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p> Ofrecer un servicio de atenci&oacute;n nutrimental  de alta calidad,  que satisfaga las necesidades y expectativas de nuestros usuarios y pacientes hospitalizados, alcanzando un sistema integral de alimentaci&oacute;n y nutrici&oacute;n traducido en acciones concretas que nos permita brindar con eficacia y eficiencia la asistencia pertinente a los beneficiarios a trav&eacute;s del mejoramiento continuo de procesos y conocimientos.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/nutricion.cocina/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>	
					<div>						
						<div class="features-izq">
							<div>
								<h1>Valores</h1>	
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Respeto a la Dignidad Humana.
								</p>								
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Compromiso real con los beneficiarios de nuestro servicio.
								</p>								
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Disposici&oacute;n de servicios sujetos a nuestra atenci&oacute;n.
								</p>								
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Lealtad.
								</p>								
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Nobleza.
								</p>								
								<p class="bullet">
									<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Tolerancia y Perseverancia.
								</p>								
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/nutricion.cocina/valores.png" alt="Vision"/>
							</div>
						</div>
					</div>															
				</div>					
				<section class="nutricion-cocina-izq">			
						
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Nutrici&oacute;n, Diet&eacute;tica y Cocina</p><br/>	
					</div>										
					<section style="padding-left: 10px; padding-top: 10px; float: left;" >
						<p class="cargo">Nutrici&oacute;n, Diet&eacute;tica</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaboraci&oacute;n de Men&uacute;s para personal y paciente en base a dietas normales y terap&eacute;uticas.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Pase de Visita a pacientes hospitalizados para apoyo en las indicaciones de Dietas.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>C&aacute;lculo de dietas espec&iacute;ficas para pacientes hospitalizados.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Atenci&oacute;n a Interconsultas de los diferentes servicios hospitalarios.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Valoraci&oacute;n, Asesor&iacute;a y Tratamiento a pacientes Hospitalizados y familiares.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Atenci&oacute;n de Pacientes en Consulta Externa.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Apoyo en el &aacute;rea de cocina para control de dietas.
						</p><br><br>
						<p class="cargo">Cocina</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaboraci&oacute;n, servicio de Alimentos y atenci&oacute;n para personal del Hospital.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Elaboraci&oacute;n y servicio de Alimentos para pacientes en base a dietas terap&eacute;uticas.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Distribuci&oacute;n de dietas terap&eacute;uticas a diferentes &aacute;reas del nosocomio.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Preparaci&oacute;n de F&oacute;rmulas Enterales.
						</p><br>
						<p class="bullet">
							<span class="ui-icon ui-icon-radio-on" style="float: left; margin-right: .3em;"></span>Atenci&oacute;n y preparaci&oacute;n de dietas espec&iacute;ficas.
						</p><br>
					</section>
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Horarios</p><br/>	
					</div>	
					<section style="padding-left: 10px; padding-top: 10px; float: left;">					
						<p class="titulo-horario">Nutrici&oacute;n</p>
						<p class="personal">Lunes a Viernes de 8am a 10pm</p><br>
						<p class="titulo-horario">Cocina del Paciente</p>
						<p class="personal">Diariamente de 6:30am a 7:30pm</p><br>
						<p class="titulo-horario">Cocina del Personal</p>
						<p class="personal">Diariamente de 7:30am a 12am</p>
					</section>										
				</section>																		
				<section class="nutricion-cocina-der">			
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>  
    				<img alt="" src="../../img/departamentos/nutricion.cocina/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin: 10px 0;">    				
    				<a id="btn-zoom" class="btn">Zoom</a><br/><br/>
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); float: left;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Personal</p><br/>	
					</div>	

			 		<section style=" padding-top: 10px; float: left; width: 340px;">						
						<div id="accordion" style="float: left;">
							<h3><a href="#">Turno Matutino</a></h3>
							<div>
								<p class="cargo">T&eacute;cnico en Nutrici&oacute;n</p>
								<p class="personal">Lili&aacute;n Margarita N&uacute;&ntilde;ez</p>
								<p class="cargo">Coordinadora de Cocina</p>
								<p class="personal">Lourdes Petitt Espinoza</p>
								<p class="cargo">Almacenista</p>
								<p class="personal">Patricia Rosas Romero</p>
								<p class="cargo">Cocinera de Personal</p>
								<p class="personal">Gloria Teodosa Olachea</p>
								<p class="cargo">Cocinero de Paciente</p>
								<p class="personal">Francisco Heraclio Angulo</p>
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Leonarda Reyes Reyes</p>
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Nereida Espinal Olachea</p>
								<p class="cargo">Asistente de Cocina</p>
								<p class="personal">Paz Inocencia Rosas Aguilar</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Mar&iacute;a del Carmen Cota Amador</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Rosa Candelaria Cota Angulo</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Alma Delia Zazueta Amador</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Ana Isabel Bustamante Beltr&aacute;n</p>
							</div>		
							<h3><a href="#">Turno Vespertino</a></h3>
							<div>
								<p class="cargo">Nutricionista</p>
								<p class="personal">Xochiquetzal Badillo Cota</p>
								<p class="cargo">Cocinera de Paciente</p>
								<p class="personal">Rosa Mar&iacute;a Alameda &Aacute;lvarez</p>
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Martina Guadalupe Ca&ntilde;edo</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Mar&iacute;a del Rosario S&aacute;nchez de la Pe&ntilde;a</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Luz Estela &Aacute;lvarez Marr&oacute;n</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Mireya Boj&oacute;rquez Cruz</p>
							</div>	
							<h3><a href="#">Turno Nocturno</a></h3>
							<div>
								<p class="cargo">Cocinera de Personal</p>
								<p class="personal">Elizabeth Romero Monteverde</p>	
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Juana M&aacute;rquez Ruiz</p>	
							</div>	
							<h3><a href="#">Turno Especial Diurno</a></h3>
							<div>
								<p class="cargo">T&eacute;cnico en Nutrici&oacute;n</p>
								<p class="personal">Guadalupe del Carmen Ni&ntilde;o de Rivera Flores</p>	
								<p class="cargo">Almacenista</p>
								<p class="personal">Mar&iacute;a del Rosario Alvarado Geraldo</p>	
								<p class="cargo">Cocinera de Personal</p>
								<p class="personal">Alina Elizabeth Salgado</p>
								<p class="cargo">Cocinera de Paciente</p>
								<p class="personal">Ma. Alejandrina M&aacute;rquez Ruiz</p>	
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Mar&iacute;a Guadalupe Rosas Cota</p>	
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Lourdes Agustina Flores Ortega</p>
								<p class="cargo">Asistente de Cocina</p>
								<p class="personal">Lourdes Bernab&eacute; Ni&ntilde;o de Rivera Flores</p>	
								<p class="cargo">Galopina</p>
								<p class="personal">Maricela Monteverde Ruiz</p>	
								<p class="cargo">Galopina</p>
								<p class="personal">Mar&iacute;a del Rosario M&aacute;rquez Monteverde</p>
								<p class="cargo">Galopina</p>
								<p class="personal">Adela Loa Vera</p>	
							</div>	
							<h3><a href="#">Especial Nocturno</a></h3>
							<div>
								<p class="cargo">Cocinera de Personal</p>
								<p class="personal">Mar&iacute;a de Jes&uacute;s S&aacute;nchez Avil&eacute;s</p>	
								<p class="cargo">Auxiliar de Cocina</p>
								<p class="personal">Martha Sagrario Osuna Duarte</p>	
							</div>	
						</div>	
					</section>					
    			</section>	     			 		 
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>