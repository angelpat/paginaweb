<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css" />
		<link rel="stylesheet" href="../../css/jshowoff.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/jquery.jshowoff.js"></script>
		<script src="../../js/depto-ensenanza.js"></script>
		<script type="text/javascript">
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>         
    	<div id="contador"></div>     
    	<section id="modal-ensenanza"></section>	
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");		
				$objCabecera->menu("../../");			
			?>	
			<header class="header-ensenanza">
				<section class="ensenanza-izq" >
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Subdirecci&oacute;n de Ense&ntilde;anza e Investigaci&oacute;n</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dr. Gustavo Jorge Farias Noyola &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Subdirector de Ense&ntilde;anza</p>
				 	</div>				
				</section>
				<section class="ensenanza-der">
					<img src="../../img/departamentos/ensenanza/header-ensenanza.png" width="340" height="96" alt="Enseniansa">
				</section>	 	
			</header>
			<div id="content">
			
				<div id="features">					
					<div>						
						<div class="features-izq">
							<div>
								<h1>Misi&oacute;n</h1>							
								<p>Utilizando m&eacute;todos modernos y tradicionales, mantener actualizado al personal en conocimientos y desarrollo de destrezas con especial &eacute;nfasis en la formaci&oacute;n de recursos humanos para la atenci&oacute;n de la salud dentro de un marco de valores &eacute;ticos tanto para su superaci&oacute;n personal como la satisfacci&oacute;n de los usuarios</p>							
							</div>
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/ensenanza/mision.png" alt="Mision"/>
							</div>
						</div>
					</div>		
					<div>						
						<div class="features-izq">
							<div>
								<h1>Visi&oacute;n</h1>							
								<p>Esta direcci&oacute;n estar&aacute; siempre a la vanguardia en la utilizaci&oacute;n de herramientas para la motivaci&oacute;n, capacitaci&oacute;n y actualizaci&oacute;n del personal, promoviendo actitudes positivas para el mejor desarrollo y para proporcionar servicios satisfactorios a los usuarios.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/ensenanza/vision.png" alt="Vision"/>
							</div>
						</div>
					</div>	
					<div>						
						<div class="features-izq">
							<div>
								<h1>Valores</h1>							
								<p>Trabajando responsable en equipo, proporcionar el servicio a los usuarios con calidad, respeto y eficiencia.</p>
							</div>							
						</div>
						<div class="features-der">
							<div>
								<img src="../../img/departamentos/ensenanza/valores.png" alt="Valores"/>
							</div>
						</div>
					</div>											
				</div>									
				<section class="ensenanza-izq">
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Ense&ntilde;anza e Investigacion</p><br/>	
					</div><br>				
					<p class="texto sangria">
						<a href="capacitacion.php">Capacitaci&oacute;n</a>
					</p>
					<br/>
					<p class="texto sangria">						
					</p>
				</section>																	
				<section class="ensenanza-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Organigrama</p><br/>	
					</div>    				
    				<img alt="" src="../../img/departamentos/ensenanza/organigrama-thumb.png" class="imgshadow" style="width: 330px; margin: 10px 0;">
    				
    				<a id="btn-zoom" class="btn">Zoom</a> 
					
    			</section>	  		    				
			</div>
			<?php 
				$objCabecera->pie();
			?>			
		</div>               	 
    </body>
</html>