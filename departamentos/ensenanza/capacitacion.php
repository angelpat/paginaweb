<?php 
include_once '../../cls/clsCabecera.php';
$objCabecera = new Cabecera();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="../../css/jquery-ui-1.8.16.custom.css" />
		<link rel="stylesheet" href="../../css/master.css" />
		<link rel="stylesheet" href="../../css/menu.css" />
		<link rel="stylesheet" href="../../css/startstop-slider.css" />
		<script src="../../js/jquery-1.6.2.min.js"></script>
		<script src="../../js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/menu.js"></script>    
		<script src="../../js/depto-ensenanza.js"></script>
		<script>
			document.createElement("nav");
			document.createElement("header");
			document.createElement("footer");
			document.createElement("section");
			document.createElement("article");
			document.createElement("aside");
			document.createElement("hgroup");	
			$(function() {
				$( "#tabs" ).tabs();
				$( "#accordion" ).accordion();
			});			
		</script>			
		<title>HOSPITAL SALVATIERRA</title>
		<link rel='shortcut icon' href='../../img/icon/shgjms.ico' type='image/x-icon' />
    </head>
    <body>         
    	<div id="contador"></div>     
    	<section id="modal-ensenanza"></section>		
		<div id="modal-contacto" title="P&aacute;gina de Contacto">
			<br><br><br>			
			<p style="font-size: 14px; color: #053B64;">Pr&oacute;ximamente en l&iacute;nea</p>
		</div>    	
		<div id="wrapper">
			<?php 
				$objCabecera->cabecera("");
				$objCabecera->menu("../../");
			?>	
			<header class="header-ensenanza">
				<section class="ensenanza-izq" >				
					<div style="background-image: url('../../img/content/orla.png'); background-repeat: no-repeat; ">
				 		<br>
				 		<p class="titulo-header">Subdirecci&oacute;n de Ense&ntilde;anza e Investigaci&oacute;n</p>				 	
				 		<br><br><br>
				 		<p class="jefe-servicio">Dr. Gustavo Jorge Farias Noyola &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Subdirector de Ense&ntilde;anza</p>
				 	</div>				
				</section>
				<section class="ensenanza-der">
					<img src="../../img/departamentos/ensenanza/header-ensenanza.png" width="340" height="96" alt="Ensenianza y Calidad">
				</section>	 	
			</header>
			<div id="content">
								
				<section class="ensenanza-izq">		
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Comisi&oacute;n Auxiliar Mixta de Capacitaci&oacute;n</p>	
					</div><br>				
					<p class="texto sangria">
					Las comisiones mixtas de capacitaci&oacute;n tendr&aacute;n por objeto promover, evaluar y vigilar el cumplimiento de los programas de capacitaci&oacute;n para los trabajadores de base de la Secretar&iacute;a.
					Se establecen tres instancias de coordinaci&oacute;n del esfuerzo capacitador:</p><br>
				
						<p class="texto"><strong>a. </strong>Comisi&oacute;n Nacional la que tendr&aacute; competencia en todo el territorio nacional y residir&aacute; en el Distrito Federal;</p><br>
						<p class="texto"><strong>b. </strong>Comisi&oacute;n Central con competencia en cada uno de los entes donde se instale y residir&aacute; en la misma sede del Titular correspondiente; y</p><br>
						<p class="texto"><strong>c. </strong>La Comisi&oacute;n Auxiliar con competencia en el &aacute;mbito exclusivo de cada jurisdicci&oacute;n sanitaria o establecimiento de atenci&oacute;n m&eacute;dica donde  se instale, la que residir&aacute; en el domicilio del centro de trabajo correspondiente.</p> 
								
					<br/>
											
	    			<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Cursos de Mayo - Junio</p><br/>	
					</div><br>	

					<div class="ui-state-highlight ui-corner-all" style="margin-top: 0px; padding: 0.7em;  width:580px;" > 
						<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
						<strong>Nota!</strong></p><br/>
						* Informes e inscripciones Depto. de Ense&ntilde;anza e Investigaci&oacute;n<br> 
						* Es importante ver a quien esta dirigido el curso.
					</div>					
					<br>									
					<table class="hor-zebra-azul" style="font-size: 10px;">
						<thead>
							<tr >
								<th style="font-size: 10px;">Evento</th>
								<th style="font-size: 10px;">Fecha</th>
								<th style="font-size: 10px;">Horario</th>
								<th style="font-size: 10px;">Dirigido a</th>
								<th style="font-size: 10px;">Coordinaci&oacute;n</th>								
								<th style="font-size: 10px;">Sede</th>
							</tr>
						</thead>
						<tr>
							<td>Indicadores de atenci&oacute;n de enfermer&iacute;a</td>
							<td>29 de Mayo</td>
							<td>8:30AM - 14:30PM</td>
							<td>M&eacute;dico y Enfermer&iacute;a</td>
							<td>MSP. B.Cristina Osuna Leon</td>
							<td>Aula de Ense&ntilde;anza No.3</td>
						</tr>	
						<tr>
							<td>Soporte Vital Cardiovascular Avanzado</td>
							<td>02-03 de Junio</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>	
						<tr>
							<td>T&oacute;picos de Sexualidad</td>
							<td>08-09 de Junio</td>
							<td>9:00AM - 14:00PM</td>
							<td></td>
							<td>
								Dr. Juan Luis Alvarez-Gayou Jurguenson<br>							
								Dr. Eduardo Baz&aacute;n Rodr&iacute;guez
							</td>
							<td></td>
						</tr>						
																
					</table>					
					<br>

				</section>																	
				<section class="ensenanza-der">
					<div class="barra-02" style="background-image: url('../../img/content/barra-02-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-02-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Integrantes CAMC</p><br/>	
					</div><br> 					
					<div id="accordion">

						<h3><a href="#">Secretaria T&eacute;cnica</a></h3>
						<div>
							<p class="titulo-dr">MSP. Blanca Cristina Osuna Le&oacute;n</p> 
							<p class="titulo-dr">Jefa de Capacitaci&oacute;n</p> <br> 
						</div>
						<h3><a href="#">Representantes por la Autoridad</a></h3>
						<div>
							<p class="titulo-dr">Propietario: Dr. Gustavo J. Farias Noyola</p>
							<p class="titulo-dr">Subdirector de Ense&ntilde;anza e Investigaci&oacute;n</p> <br> 
							<p class="titulo-dr">Propietaria: Lic. Claudia Ram&iacute;rez del Castillo</p>
							<p class="titulo-dr">Jefa de Recursos Humanos</p> <br> 
						</div>
						<h3><a href="#">Representantes Secci&oacute;n 61 SNTSA</a></h3>
						<div>
							<p class="titulo-dr">Propietaria: LE. Olivia Murillo Guti&eacute;rrez</p>
							<p class="titulo-dr">Secretaria Particular de la Secretaria General del SNTSA</p>  
							<p class="titulo-dr">EG. Ary de Jes&uacute;s Jim&eacute;nez Rodr&iacute;guez</p> 
							<p class="titulo-dr">Delegado Sindical Secci&oacute;n No.61</p><br/>
						</div>		
					</div>	<br>
					
					<div class="barra-01" style="background-image: url('../../img/content/barra-01-l.png'); margin: auto;">													
						<img alt="" src="../../img/content/barra-01-r.png" style="float: right; z-index: 0" >
						<p class="titulo-barra-01">Cursos y descargas</p><br/>	
					</div><br>					
					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">Descargas</a></li>
							<li><a href="#tabs-2">Cursos</a></li>	
						</ul>
						<div id="tabs-1">
							<a target="_blank" href="../../resources/departamentos/ensenanza/ReglamentoCapacitacion09.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Reglamento de Capacitaci&oacute;n</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/SOLICITUD-EVENTOS-DE-CAPACITACION.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Solicitud de Eventos de Capacitaci&oacute;n</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/cartelCarreraTransplante.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Carrera por la donaci&oacute;n de &oacute;rganos y tejidos</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/jornadas_medicas_2015.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> X Jornadas M&eacutedicas, actualidades en oncolog&iacutea</a><br><br>
							<p style="font-weight: bold;">Becas</p><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/REQUISITOS-BECA-2012.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Requisitos de Beca</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/SOLICITUD-DE-BECA-CAMC.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Solicitud de Beca</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/CARTA-COMPROMISO-CAMC.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Carta Compromiso</a><br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/CARTA-DE-JUSTIFICACION-CAMC.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Carta de Justificaci&oacute;n</a><br>		
							<a target="_blank" href="../../resources/departamentos/ensenanza/CARTA-DE-NO-INCONVENIENTE-CAMC.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Carta de no Inconveniente</a><br>							
						</div>
						<div id="tabs-2">
							<a target="_blank" href="../../resources/departamentos/ensenanza/Cursos/Cursos-Ensenanza-2012-06-01.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Junio 2012</a>
							<br>						
							<a target="_blank" href="../../resources/departamentos/ensenanza/Cursos/Cursos-Ensenanza-2012-05-01.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Mayo 2012</a>
							<br>						
							<a target="_blank" href="../../resources/departamentos/ensenanza/Cursos/Cursos-Ensenanza-2012-04-01.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Abril 2012</a>
							<br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/Cursos/Cursos-Ensenanza-2012-01-01.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> Enero 2012</a>
							<br>
							<a target="_blank" href="../../resources/departamentos/ensenanza/Cursos/cursoDonacion.pdf" class="organigrama"><img src="../../img/icon/pdf.png" alt="pdf" title="pdf" width="16" height="16"> 3er Curso de donaci&oacute;n y transplante de &oacute;rganos</a>
							<br><br><br><br><br><br>
						</div>
					</div>
    			</section>	      				    				
			</div>
			<?php 
				$objCabecera->pie();
			?>		
		</div>               	 
    </body>
</html>